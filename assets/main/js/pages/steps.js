$(".tab-wizard").steps({
    headerTag: "h6"
    , bodyTag: "section"
    , transitionEffect: "none"
    , titleTemplate: '<span class="step">#index#</span> #title#'
    , labels: {
        finish: "Submit"
    }
    , onFinished: function (event, currentIndex) {
       swal("Pengajuan Klaim Berhasil Disimpan !");
            
    }
});


var form = $(".validation-wizard").show();
let next = false;

$(".validation-wizard").steps({
    headerTag: "h6"
    , bodyTag: "section"
    , transitionEffect: "none"
    , titleTemplate: '<span class="step">#index#</span> #title#'
    , labels: {
        finish: "Submit"
    }
    , onStepChanging: function (event, currentIndex, newIndex) {
        console.log(currentIndex);
        console.log(newIndex);
         var status = $('#status_step').val();
        var noklaim = $("#noklaim").val();
        var url = $("#url").val();

        if(next){
            next = false;
            return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
        }else{
        $.ajax({
            type      : 'POST',
            url       : url + 'pengajuanklaim/getstatus', //Your form processing file URL
            data      : {
                          noklaim : noklaim                          
                        }, 
            // dataType  : 'json',
            success   : function(data) {
                console.log(data);
                status = data;
                if(currentIndex < newIndex){
                    if (status < 3) {
                    if (newIndex >= status) {
                        swal('Menunggu persetujuan Validator');
                            next = false;
                            return false;
                    }else{
                            next = true;
                            $(".validation-wizard").steps("next");
                    }
                } else{
                    next = true;
                    $(".validation-wizard").steps("next");
                }
                }else{
                    next = true;
                    $(".validation-wizard").steps("previous");
                }
                
                //  if ( == 1) {
                    
//                     } else{
//                         console.log('test1');
//                         next = true;
//                         $(".validation-wizard").steps("next");
                        
//                         }
//                     } else if (newIndex == 2) {
//                         if (status == 2) {
//                             swal('Menunggu persetujuan Validator');
//                             next = false;
//                             return false;
//                         } else{
//                             console.log('test2');
//                             next = true;
//                             $(".validation-wizard").steps("next");
//                             // return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
//                         }
//                     } else if (newIndex == 3) {
//                         if (status == 3) {
//                             swal('Menunggu persetujuan Validator');
//                             next = false;
//                             return false;
//                         } else{
//                             console.log('test3');
//                             next = true;
//                             $(".validation-wizard").steps("next");
//                             // return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
//                         }
//                     }
//                     else {
//                         console.log('test4');
//                         next = true;
//                         // $(".validation-wizard").steps("next");
//                         return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
//                     }

                // return status;
                // window.alert('Step 1 Submitted Sucessfully!');
                // window.location.href='<?=base_url('pengajuanklaim/klaimna/')?>' + noklaim;

             }
        });
        }
        
        
    },
    onContentLoaded: function (event, currentIndex) {
       next = false;
   }
    , onFinishing: function (event, currentIndex) {
        return form.validate().settings.ignore = ":disabled", form.valid()
    }
    , onFinished: function (event, currentIndex) {
         swal("Pengajuan Klaim Berhasil Disimpan !");
    }
}), $(".validation-wizard").validate({
    ignore: "input[type=hidden]"
    , errorClass: "text-danger"
    , successClass: "text-success"
    , highlight: function (element, errorClass) {
        $(element).removeClass(errorClass)
    }
    , unhighlight: function (element, errorClass) {
        $(element).removeClass(errorClass)
    }
    , errorPlacement: function (error, element) {
        error.insertAfter(element)
    }
    , rules: {
        email: {
            email: !0
        }
    }
})
 <div class="container-full">
		<!-- Main content -->
                <section class="content">
            <div class="row">
                <div class="col-12">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h4 class="box-title">Rekap Restitusi Asuradur</h4>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div class="row">
                         <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Cabang :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                          $sub = substr($as,4);
                                        $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BRK') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BRK') === false) {
                                            $sql = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and nama_cabang LIKE '%KC%'")->result_array();
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang">
                                            <option value=""></option>
                                         <?php
                                          foreach ($sql as $key => $value) {
                                         ?>
                                           <option value="<?=$value['id_cabang']?>"><?=$value['nama_cabang']?></option> 
                                         <?php
                                          } ?>
                                          </select>
                                          <?php
                                        }
                                        elseif(!empty($cabangs)){
                                            foreach ($cabangs as $key => $values) {
                                        ?>
                                        <select class="custom-select form-control" id="cabang" name="cabang">
                                            <option value=""></option>
                                           <option value="<?=$values['id_cabang']?>"><?=$values['nama_cabang']?></option> 
                                           </select>
                                        <?php
                                        }
                                         }else{
                                            $cab = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub'")->result_array();
                                            $sub1 = $cab[0]['id_induk'];
                                            $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub1'")->result_array();
                                          foreach ($sqlss as $key => $vass) {
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang" disabled="">
                                            <option value="<?=$vass['id_cabang']?>"><?=$vass['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Periode :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="location3" name="periodebulan">
                                        <option value=""></option>
                                        <option value="Amsterdam">Januari</option>
                                        <option value="Berlin">Februari</option>
                                        <option value="Frankfurt">Maret</option>
                                        <option value="Frankfurt">April</option>
                                        <option value="Frankfurt">Mei</option>
                                        <option value="Frankfurt">Juni</option>
                                        <option value="Frankfurt">Juli</option>
                                        <option value="Frankfurt">Agustus</option>
                                        <option value="Frankfurt">September</option>
                                        <option value="Frankfurt">Oktober</option>
                                        <option value="Frankfurt">November</option>
                                        <option value="Frankfurt">Desember</option>
                                    </select>
                                    <div>
                                        <select class="custom-select form-control" id="location3" name="periodetahun">
                                        <option value=""></option>
                                        <option value="Amsterdam">2021</option>
                                        <option value="Berlin">2022</option>
                                        <option value="Frankfurt">2023</option>
                                        <option value="Frankfurt">2024</option>
                                        <option value="Frankfurt">2025</option>
                                        <option value="Frankfurt">2026</option>
                                        <option value="Frankfurt">2027</option>
                                        <option value="Frankfurt">2028</option>
                                        <option value="Frankfurt">2029</option>
                                        <option value="Frankfurt">2030</option>
                                    </select>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Capem / Kedai :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                        $sub = substr($as,4);
                                        $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BRK') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BRK') === false) {
                                         ?>
                                         <select class="custom-select form-control" id="capem" name="capem">
                                        </select>
                                         <?php
                                        } elseif(!empty($cabangs)){
                                        ?>
                                            <select class="custom-select form-control" id="capem" name="capem">
                                            </select>
                                        <?php
                                        } else{
                                            $sub = substr($as,4);
                                            $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub'")->result_array();
                                          foreach ($sqlss as $key => $va) {
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang" disabled="">
                                            <option value="<?=$va['id_cabang']?>"><?=$va['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Status Data :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="capem" name="capem">
                                        <option value=""></option>
                                        <option value="">Semua Data</option>
                                        <option value="">Sudah Rekonsiliasi</option>
                                        <option value="">Belum Rekonsiliasi</option>                                        
                                    </select>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="button" class="waves-effect waves-light btn btn-rounded btn-primary-light mb-5"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
                                </div>
                            </div>
                        </div>
                  </div>
              </div>
              <!-- /.box -->
            </div>
            </div>      
        </section>




		<section class="content">
			<div class="row">
			<div class="col-12">
			  <div class="box box-default">
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table">
						<thead class="bg-dark">
							<tr>
								<th class="text-center">No</th>
                                <th class="text-center">Action</th>
                                <th class="text-center">Cabang</th>
                                <th class="text-center">No PK</th>
                                <th class="text-center">No Rekening</th>
                                <th class="text-center">Nama</th>
                                <th class="text-center">Tanggal Lahir</th>
                                <th class="text-center">Tanggal Pembukaan</th>
                                <th class="text-center">Jatuh Tempo</th>
                                <th class="text-center">Plan Kredit</th>
                                <th class="text-center">Amount</th>
                                <th class="text-center">No KTP</th>
                                <th class="text-center">Rate</th>
                                <th class="text-center">Jenis Kelamin</th>
                                <th class="text-center">Rate Asuransi</th>
                                <th class="text-center">Asuransi</th>
                                <th class="text-center">No NPWP</th>
                                <th class="text-center">No PK Lama</th>
							</tr>
						</thead>
						<tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
							<!-- <?php $no=1; foreach ($datana as $key => $value) { ?>
                                <tr id="<?php echo $value['id']; ?>">
                                <td class="text-center"><?=$no++?></td>
                                <td class="text-center">
                                    <div style="width: max-content;">
                                    <a title="View"
                                        href="javascript:;"
                                        data-No_Journal="<?php echo $value['id_data'];?>"
                                        data-toggle="modal" 
                                        data-target="#view<?php echo $value['id_data']?>"
                                        class="btn-sm btn-warning"
                                        >
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                    </a> &nbsp;
                                    <a title="Change"
                                       href="javascript:;"
                                       data-No_Journal="<?php echo $value['id_data'];?>"
                                       data-toggle="modal"
                                       data-target="#edit<?php echo $value['id_data']?>"
                                       class="btn-sm btn-info"
                                    >
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </a> &nbsp;
                                    <a title="Delete" href="<?php echo base_url('rekonsiliasi/delete/'.$value['id_data'])?>" onclick="return confirm('Are your sure ?');" class="btn-sm btn-danger remove">
                                        <i class="glyphicon glyphicon-remove-sign"></i>
                                    </a>
                                    </div>
                                </td>
                                <td class="text-center"><?=$value['cab']?></td>
                                <td class="text-center"><?=$value['pk']?></td>
                                <td class="text-center"><?=$value['norek']?></td>
                                <td class="text-center"><?=$value['nama']?></td>
                                <td class="text-center"><?=date("d/m/Y", strtotime($value['lahir']));?></td>
                                <td class="text-center"><?=date("d/m/Y", strtotime($value['buka']));?></td>
                                <td class="text-center"><?=$value['tempo']?></td>
                                <td class="text-center"><?=$value['plankredit']?></td>
                                <td class="text-center"><?=number_format($value['amount'], 0);?></td>
                                <td class="text-center"><?=$value['ktp']?></td>
                                <td class="text-center"><?=$value['rate'];?></td>
                                <td class="text-center"><?php
                                    if ($value['sex'] == 'P'){
                                        ?>
                                    <span class="badge badge-pill badge-success">
                                        <?php
                                        echo $value['sex'] = 'Perempuan';
                                        } else if ($value['sex'] == 'L') {
                                        ?>
                                        <span class="badge badge-pill badge-danger">
                                        <?php
                                        echo $value['sex'] = 'Laki-Laki';
                                        } else{
                                            echo '';
                                        }
                                        ?></td>
                                        <td class="text-center"><?=$value['rate_asuransi'];?></td>
                                <td class="text-center"><?=$value['asuransi']?></td>
                                <td class="text-center"><?=$value['npwp']?></td>
                                <td class="text-center"><?=$value['old_pk']?></td>
                            </tr>  
                            <?php } ?> -->
						</tbody>
						<tfoot>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			</div>		
		</section>
		<!-- /.content -->
	  </div>
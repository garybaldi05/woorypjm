 <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h4 class="box-title">Penutupan Asuransi</h4>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <form class="form-horizontal form-bordered" method="post" action="<?=base_url('dokumen/search')?>">
                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Cabang :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                          $sub = substr($as,4);
                                        $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BRK') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BRK') === false) {
                                            $sql = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and nama_cabang LIKE '%KC%'")->result_array();
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang">
                                            <option value=""></option>
                                         <?php
                                          foreach ($sql as $key => $value) {
                                         ?>
                                           <option value="<?=$value['id_cabang']?>"><?=$value['nama_cabang']?></option> 
                                         <?php
                                          } ?>
                                          </select>
                                          <?php
                                        }
                                        elseif(!empty($cabangs)){
                                            foreach ($cabangs as $key => $values) {
                                        ?>
                                        <select class="custom-select form-control" id="cabang" name="cabang">
                                            <option value=""></option>
                                           <option value="<?=$values['id_cabang']?>"><?=$values['nama_cabang']?></option> 
                                           </select>
                                        <?php
                                        }
                                         }else{
                                            $cab = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub'")->result_array();
                                            $sub1 = $cab[0]['id_induk'];
                                            $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub1'")->result_array();
                                          foreach ($sqlss as $key => $vass) {
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang" disabled="">
                                            <option value="<?=$vass['id_cabang']?>"><?=$vass['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Periode :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="periodebulan" name="periodebulan">
                                        <option value=""></option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                    <div>
                                        <select class="custom-select form-control" id="periodetahun" name="periodetahun">
                                        <option value=""></option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                        <option value="2026">2026</option>
                                        <option value="2027">2027</option>
                                        <option value="2028">2028</option>
                                        <option value="2029">2029</option>
                                        <option value="2030">2030</option>
                                    </select>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Capem / Kedai :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                        $sub = substr($as,4);
                                        $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BRK') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BRK') === false) {
                                         ?>
                                         <select class="custom-select form-control" id="capem" name="capem">
                                        </select>
                                         <?php
                                        } elseif(!empty($cabangs)){
                                        ?>
                                            <select class="custom-select form-control" id="capem" name="capem">
                                            </select>
                                        <?php
                                        } else{
                                            $sub = substr($as,4);
                                            $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub'")->result_array();
                                          foreach ($sqlss as $key => $va) {
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang" disabled="">
                                            <option value="<?=$va['id_cabang']?>"><?=$va['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status Klaim :</label>
                                <div class="demo-checkbox">
                                    <input type="checkbox" id="basic_checkbox_3" name="klaim" class="filled-in" value="2" />
                                    <label for="basic_checkbox_3">Yes</label>
                                    <input type="checkbox" id="basic_checkbox_4" name="klaim" class="filled-in" value="0" />
                                    <label for="basic_checkbox_4">No</label>
                                    <input type="checkbox" id="basic_checkbox_5" name="klaim" class="filled-in" value="1" />
                                    <label for="basic_checkbox_5">Ditolak</label>
                                </div>
                            </div>
                        </div>
                            
                        </div>

                        <div class="row">

                        <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status Medical :</label>
                                <div class="demo-checkbox">
                                    <input type="checkbox" id="basic_checkbox_6" name="medical" class="filled-in" value="CAC" />
                                    <label for="basic_checkbox_6">CAC</label>
                                    <input type="checkbox" id="basic_checkbox_7" name="medical" class="filled-in" value="CBC" />
                                    <label for="basic_checkbox_7">CBC</label>
                                </div>
                            </div>
                        </div> 
                            
                         <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status Restitusi :</label>
                                <div class="demo-checkbox">
                                    <input type="checkbox" id="basic_checkbox_8" name="restitusi" class="filled-in" value="2" />
                                    <label for="basic_checkbox_8">Yes</label>
                                    <input type="checkbox" id="basic_checkbox_9" name="restitusi" class="filled-in" value="0" />
                                    <label for="basic_checkbox_9">No</label>
                                    <input type="checkbox" id="basic_checkbox_10" name="restitusi" class="filled-in" value="1" />
                                    <label for="basic_checkbox_10">Ditolak</label>
                                </div>
                            </div>
                        </div> 
                            
                        </div>

                        <div class="row">                   
                        </div>
                        <!-- <div class="row"> -->
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="waves-effect waves-light btn btn-rounded btn-primary-light mb-5"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
                                </div>
                            </div>
                        <!-- </div> -->
                    </form>
                  </div>
              </div>
              <!-- /.box -->
            </div>
            </div>      
        </section>

        <?php
        if($this->session->flashdata('success')){
            ?>
            <div class="alert alert-success text-center">
                <i class="glyphicon glyphicon-ok-sign"></i> <span><?=$this->session->flashdata('success')?></span>
            </div>
            <div></div>
            <?php
        }
        ?>
        <?php
        if($this->session->flashdata('error')){
            ?>
            <div class="alert alert-danger text-center">
                <i class="glyphicon glyphicon-remove-sign"></i> <span><?=$this->session->flashdata('error')?></span>
            </div>
            <div>
            </div>
        <?php   } ?>
        <!-- /.content -->
                <section class="content">
            <div class="row">
                <div class="col-12">
              <div class="box box-default">
                <div class="box-body">
                    <div class="table-responsive">
                      <table class="table">
                        <thead class="bg-dark">
                            <tr>
                                <!-- <?php
                                $user = strtoupper($this->session->userdata('KodeUser'));
                                if (strpos($user, 'BWS') === false) {
                                ?>
                                <th class="text-center">Action</th>
                                <?php
                                }
                                ?> -->
                                <th class="text-center">No</th>
                                <th class="text-center">Cabang</th>
                                <th class="text-center"><div style="width: max-content;">No PK</div></th>
                                <th class="text-center"><div style="width: max-content;">No CIF</div></th>
                                <th class="text-center">Nama</th>
                                <th class="text-center">Tanggal Lahir</th>
                                <th class="text-center">Tanggal Pembukaan</th>
                                <th class="text-center">Jatuh Tempo</th>
                                <th class="text-center">Amount</th>
                                <th class="text-center">No KTP</th>
                                <th class="text-center">Premi</th>
                                <th class="text-center">Asuransi</th>
                                <!-- <th class="text-center">Status Medical</th>
                                <th class="text-center">Nama Medical</th> -->
                                <th class="text-center">Status Klaim</th>
                                <th class="text-center">Status Restitusi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($datana as $key => $value) { 
                                ?>
                                <tr id="<?php echo $value['id_data']; ?>">
                                <!-- <?php
                                $id_data = $value['id_data'];
                                $dok = $this->db->query("select id_data, buktiaktifbatal from PJM_SAUDARA.dbo.DataPenutupan
                                where id_data = '$id_data' order by date_created ASC ")->result_array();
                                $user = strtoupper($this->session->userdata('KodeUser'));
                                if (strpos($user, 'BRK') === false) {
                                ?>
                                <td class="text-center">
                                    <div style="width: max-content;">
                                <a title="Change"
                                    href="javascript:;"
                                    data-No_Acc="<?php echo $value['id_data'];?>"
                                    data-toggle="modal" 
                                    data-target="#edit<?php echo $value['id_data']?>"
                                    >
                                    <button class="btn btn-sm btn-info">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </button> 
                                </a>
                                <a title="Delete"
                                    href="javascript:;"
                                    data-No_Acc="<?php echo $value['id_data'];?>"
                                    data-toggle="modal" 
                                    data-target="#batal<?php echo $value['id_data']?>"
                                    >
                                    <button class="btn btn-sm btn-danger">
                                        <i class="glyphicon glyphicon-remove"></i>
                                    </button> 
                                </a>&nbsp;
                                <?php
                                // var_dump($dok);
                                foreach ($dok as $key => $dok1) {
                                    if (!empty($dok1['buktiaktifbatal'])) {
                                    ?>
                                        <a href="<?php
                                        echo base_url('dokumen/download/'.$dok1['id_data'])?>" class="btn-sm btn-warning">
                                            <i class="glyphicon glyphicon-download-alt"></i>
                                        </a>
                                    <?php
                                    }
                                }
                                ?>
                                </div>
                                </td>
                                <?php
                                }
                                ?> -->
                                <td class="text-center"><?=$no++?></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['namacab']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['pk']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['norek']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['nama']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?php
                                $tanggal = new DateTime($value['lahir']);
                                $today = new DateTime('today');
                                $y = $today->diff($tanggal)->y;
                                $m = $today->diff($tanggal)->m;
                                $d = $today->diff($tanggal)->d;
                                if ($m >= 6 && $d > 0) {
                                    $year = $y + 1;
                                } else{
                                    $year = $y;
                                }
                                echo date("d/m/Y", strtotime($value['lahir'])). ' ('.$year.'th)';
                                ?></div>
                                </td>
                                <td class="text-center"><?php if (!empty($value['buka'])) {
                                        echo date("d/m/Y", strtotime($value['buka']));
                                        } else{
                                            echo '';
                                        }?></td>
                                <td class="text-center"><?=$value['tempo']?></td>
                                <td class="text-center"><?=$value['amount'];?></td>
                                <td class="text-center"><?=$value['ktp']?></td>
                                <td class="text-center"><?=$value['premi']?></td>
                                <td class="text-center"><div style="width: max-content;">
                                   <?=$value['asuransi']?>
                                   </div>
                                </td>
                                <td class="text-center"><?php
                                if ($value['status_klaim'] == '0'){
                                        ?>
                                    <span class="badge badge-pill badge-warning">
                                        <?php
                                        echo $value['status_klaim'] = 'No';
                                        } else if ($value['status_klaim'] == '2'){
                                        ?>
                                        <span class="badge badge-pill badge-success">
                                        <?php
                                            echo $value['status_klaim'] = 'Yes';
                                        }
                                        ?></td>
                                <td class="text-center"><?php
                                if ($value['status_restitusi'] == '0'){
                                        ?>
                                    <span class="badge badge-pill badge-warning">
                                        <?php
                                        echo $value['status_restitusi'] = 'No';
                                        } else if ($value['status_restitusi'] == '1') {
                                        ?>
                                        <span class="badge badge-pill badge-danger">
                                        <?php
                                        echo $value['status_restitusi'] = 'Ditolak';
                                        } else if($value['status_restitusi'] == '2'){
                                        ?>
                                        <span class="badge badge-pill badge-success">
                                        <?php
                                            echo $value['status_restitusi'] = 'Yes';
                                        }
                                        ?></td>
                            </tr>  
                            <?php } ?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                      <nav aria-label="Page navigation">
                    <?php echo $this->pagination->create_links(); ?>
                    </nav>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            </div>      
        </section>
      </div>

      <?php foreach ($datana as $key => $val) { ?>

            <div id="edit<?php echo $val['id_data']?>" class="modal fade " tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <!-- <h3 class="modal-title">Edit Account</h3> -->
                        </div>
                        <div class="col-12 col-lg-12">
                        <div class="box">
                        <div class="box-body">
                        <form action="<?=base_url('dokumen/update/'.$val['id_data'])?>" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" >
                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Cabang :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="cab" name="cab">
                                        <?php
                                        $a = $val['cab'];
                                        $sqll = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$a'")->result_array();
                                        $b = $sqll[0]['nama_cabang'];
                                        ?>
                                        <option value="<?php echo $val['cab'];?>"><?php echo $b?></option>
                                        <?php 
                                          $sqls = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang")->result_array();
                                          foreach ($sqls as $key => $values) {
                                         ?>
                                           <option value="<?=$values['id_cabang']?>"><?=$values['nama_cabang']?></option> 
                                         <?php
                                          }
                                         ?>
                                    </select>
                                </div>
                                </div>
                            </div>
                        </div>
                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. PK :</label>
                                    <input type="text" class="form-control" id="pk" name="pk" value="<?= $val['pk'] ?>"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. Rekening :</label>
                                    <input type="text" class="form-control" id="norek" name="norek" readonly="" value="<?= $val['norek'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama :</label>
                                    <input type="text" class="form-control" id="nama" name="nama" value="<?= $val['nama'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tanggal Lahir :</label>
                                    <?php
                                    if (!empty($val['lahir'])) {
                                    ?>
                                    <input class="form-control" type="date" value="<?=date("Y-m-d", strtotime($val['lahir']));?>" id="lahir" name="lahir">
                                    <?php
                                    } else{
                                    ?>
                                    <input class="form-control" type="date" id="lahir" name="lahir">
                                    <?php
                                    }
                                    ?>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tanggal Pembukaan :</label>
                                    <?php
                                    if (!empty($val['buka'])) {
                                    ?>
                                    <input class="form-control" type="date" value="<?=date("Y-m-d", strtotime($val['buka']));?>" id="buka" name="buka">
                                    <?php
                                    } else{
                                    ?>
                                    <input class="form-control" type="date" id="buka" name="buka">
                                    <?php
                                    }
                                    ?>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Jatuh Tempo (dalam bulan) :</label>
                                    <input type="text" class="form-control" id="tempo" name="tempo" value="<?= $val['tempo'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Plan Kredit :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="plankredit" name="plankredit">
                                        <?php
                                        $a = $val['plankredit'];
                                        $sqll = $this->db->query("SELECT kode_faskre, nama_faskre FROM PJM_SAUDARA.dbo.FasilitasKredit where nama_faskre = '$a'")->result_array();
                                        $b = $sqll[0]['nama_faskre'];
                                        ?>
                                        <option value="<?php echo $b;?>"><?php echo $b?></option>
                                        <?php 
                                          $sqls = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.FasilitasKredit")->result_array();
                                          foreach ($sqls as $key => $values) {
                                         ?>
                                           <option value="<?=$values['nama_faskre']?>"><?=$values['nama_faskre']?></option> 
                                         <?php
                                          }
                                         ?>
                                    </select>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Amount :</label>
                                    <input type="text" class="form-control" id="amount" name="amount" value="<?= number_format($val['amount'], 0); ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. KTP :</label>
                                    <input type="text" class="form-control" id="ktp" name="ktp" value="<?= $val['ktp'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Rate (dari Bank):</label>
                                    <input type="text" class="form-control" id="rate" name="rate" value="<?= $val['rate'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Jenis Kelamin :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="sex" name="sex">
                                        <?php
                                        if ($val['sex'] == 'P'){
                                        ?>
                                        <?php
                                        echo $ab = $val['sex'] = 'Perempuan';
                                        } else if ($val['sex'] == 'L') {
                                        ?>
                                        <?php
                                        echo $ab = $val['sex'] = 'Laki-Laki';
                                        } else{
                                            echo $ab = '';
                                        }
                                        ?>
                                        <option value="<?php echo $val['sex'];?>"><?php echo $ab;?></option>
                                        <?php
                                            if ($val['sex'] == 'Perempuan') {
                                        ?>
                                           <option value="Laki-Laki">Laki-Laki</option>
                                       <?php
                                        } elseif ($val['sex'] == 'Laki-Laki') {
                                       ?>
                                       <option value="Perempuan">Perempuan</option>
                                       <?php
                                        } else{
                                       ?>
                                        <option value="Laki-Laki">Laki-Laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Rate Asuransi (Per Mil):</label>
                                    <input type="text" class="form-control" id="rate_asuransi" name="rate_asuransi" value="<?= $val['rate_asuransi'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Asuransi :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="asuransi" name="asuransi">
                                        <?php
                                        $asu = $val['asuransi'];
                                        $sqll = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Asuransi where kode_asuransi = '$a'")->result_array();
                                        $b = $sqll[0]['nama_asuransi'];
                                        ?>
                                        <option value="<?php echo $val['asuransi'];?>"><?php echo $b?></option>
                                        <?php 
                                          $sqls = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Asuransi")->result_array();
                                          foreach ($sqls as $key => $values) {
                                         ?>
                                           <option value="<?=$values['nama_asuransi']?>"><?=$values['nama_asuransi']?></option> 
                                         <?php
                                          }
                                         ?>
                                    </select>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. NPWP :</label>
                                    <input type="text" class="form-control" id="npwp" name="npwp" value="<?= $val['npwp'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. PK Lama :</label>
                                    <input type="text" class="form-control" id="old_pk" name="old_pk" value="<?= $val['old_pk'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tanggal Pengajuan Asuransi :</label>
                                    <?php
                                    if (!empty($val['tglpengajuanasuransi'])) {
                                    ?>
                                    <input class="form-control" type="date" value="<?=date("Y-m-d", strtotime($val['tglpengajuanasuransi']));?>" id="tglpengajuanasuransi" name="tglpengajuanasuransi">
                                    <?php
                                    } else{
                                    ?>
                                    <input class="form-control" type="date" id="tglpengajuanasuransi" name="tglpengajuanasuransi" value="">
                                    <?php
                                    }
                                    ?>
                            </div>
                        </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-sm btn-primary">Save changes</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php } ?>

      <?php foreach ($datana as $key => $val) { ?>

            <div id="batal<?php echo $val['id_data']?>" class="modal fade " tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <!-- <h3 class="modal-title">Edit Account</h3> -->
                        </div>
                        <div class="col-12 col-lg-12">
                        <div class="box">
                        <div class="box-body">
                        <form action="<?=base_url('dokumen/upload_avatar/')?>" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" >
                            <input type="text" class="form-control" id="id_data" name="id_data" value="<?= $val['id_data'] ?>" readonly="" style="display: none;">
                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. PK :</label>
                                    <input type="text" class="form-control" id="pk" name="pk" value="<?= $val['pk'] ?>" readonly=""></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. Rekening :</label>
                                    <input type="text" class="form-control" id="norek" name="norek" readonly="" value="<?= $val['norek'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama :</label>
                                    <input type="text" class="form-control" id="nama" name="nama" value="<?= $val['nama'] ?>" readonly=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Upload File</label>
                                    <div class="custom-file">
                                    <input type="file" class="form-control" name="buktipembatalan" id="buktipembatalan" accept="all">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Status</label>
                                    <div class="demo-checkbox">
                                        <input type="checkbox" id="aktif" name="aktif" class="filled-in" value="0" checked readonly="" />
                                        <label for="basic_checkbox_4">Pembatalan</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-sm btn-primary">Save changes</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php } ?>
<script src="<?=base_url()?>assets/main/js/jquery-1.10.0.min.js"></script>
                    <script>
                        $(document).ready(function(){
                            var base_url = '<?php echo base_url();?>';
                        $('#cabang').change(function(){
                            var id = $('#cabang').val();
                            $.ajax({
                            type      : "POST",
                            url       : base_url + 'dokumen/get_capem',
                            data      : {id : id},
                            // async     : false,
                            dataType  : 'json',
                            success   : function(data) {
                                // alert(data);
                                var html = '';
                                var i;
                                for(i=0; i<data.length; i++){
                                    html += '<option value='+data[i].id_cabang+'>'+data[i].nama_cabang+'</option>';
                                }
                                $("#capem").html(html);
                        }
                        });
                        });
                    });
                </script>
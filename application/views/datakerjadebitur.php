 <div class="container-full">
		<!-- Main content -->
		<section class="content">
			<div class="row">
							<div class="col-12">
			  <div class="box box-default">
				<div class="box-header with-border">
				  <h4 class="box-title"><?php echo $title ?></h4>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table">
						<thead class="bg-dark">
							<tr>
								 <th class="text-center">No</th>
                                <th class="text-center">Kode Jenis Debitur</th>
                                <th class="text-center">Jenis Debitur</th>
                                <th class="text-center">Nama Jenis Debitur</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach ($datana as $key => $value) { ?>
                            <tr id="<?php echo $value['kode_jenisdeb']; ?>">
                                <td class="text-center"><?=$no++?></td>
                                <td class="text-center"><?=$value['kode_jenisdeb']?></td>
                                <td class="text-center"><?=$value['jenis_deb']?></td>
                                <td class="text-center"><?=$value['nama_jenisdeb']?></td>
                            </tr>  
                            <?php } ?>
						</tbody>
						<tfoot>
						</tfoot>
					  </table>
					</div>
				</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			</div>		
		</section>
		<!-- /.content -->
	  </div>
 <?php
 $kodekla = '';
  $kodek = $this->db->query("select kodeklaim FROM PJM_SAUDARA.dbo.Klaim order by kodeklaim DESC");
  if ($kodek->num_rows() <> 0) {
            $datas = $kodek->row();
            $kode = intval($datas->kodeklaim) + 1;

        } else {
            $kode = 1;
        }

        $kodemax = str_pad($kode++, 4, "0", STR_PAD_LEFT);

  if ($kodek->num_rows() <> 0) {
      $jour = substr($datas->kodeklaim,0,6);
      $head = $this->db->query("select top 1 kodeklaim from PJM_SAUDARA.dbo.Klaim ORDER BY kodeklaim desc");
      $da = $head->row();
      $kodes = intval($da->kodeklaim) + 1;
      $kodemaxs = str_pad($kodes++, 4, "0", STR_PAD_LEFT);
      $kodekla = $kodemaxs;
  } else{
      $kodekla = date('Ym').$kodemax;
  }
?>
 <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
            <div class="col-12">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h4 class="box-title">Pengajuan Klaim</h4>
                  <input type="text" class="form-control" id="url" name="url" value="<?=base_url()?>" style="display: none;">
                </div>
              <form method="POST" action="<?= site_url('pengajuanklaim/excel') ?>" enctype="multipart/form-data">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                        <label class="col-form-label text-md-left">Upload File</label> 
                                            <input type="file" class="form-control" name="file" accept=".xls, .xlsx" required>
                                            <div class="mt-1">
                                                <span class="text-secondary">File yang harus diupload : .xls, xlsx</span>
                                            </div>
                                            <?= form_error('file','<div class="text-danger">','</div>') ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-center">
                            <div class="form-group mb-0">
                              <button type="submit" name="import" id="import" class="waves-effect waves-light btn mb-5 bg-gradient-danger">Upload</button>
                            </div>
                        </div>
                        </div>
                    </form>

                <!-- /.box-header -->
                <div class="box-body">
          <?php
        if($this->session->flashdata('success')){
            ?>
            <div class="alert alert-success text-center">
                <i class="glyphicon glyphicon-ok-sign"></i> <span><?=$this->session->flashdata('success')?></span>
            </div>
            <div></div>
            <?php
        }
        ?>
        <?php
        if($this->session->flashdata('error')){
            ?>
            <div class="alert alert-danger text-center">
                <i class="glyphicon glyphicon-remove-sign"></i> <span><?=$this->session->flashdata('error')?></span>
            </div>
            <div>
            </div>
        <?php   } ?>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs customtab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home2" role="tab"><span class="hidden-sm-up"><i class="ion-home"></i></span> <span class="hidden-xs-down">Data</span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile2" role="tab"><span class="hidden-sm-up"><i class="ion-person"></i></span> <span class="hidden-xs-down">Tambah</span></a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="home2" role="tabpanel">
                        <div class="box">
                <div class="box-header with-border">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
          <form class="form-horizontal form-bordered" method="post" action="<?=base_url('pengajuanklaim/search')?>">
                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Cabang :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                          $sub = substr($as,4);
                                        $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BWS') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BWS') === false) {
                                            $sql = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and nama_cabang LIKE '%KC%'")->result_array();
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang">
                                            <option value=""></option>
                                         <?php
                                          foreach ($sql as $key => $value) {
                                         ?>
                                           <option value="<?=$value['id_cabang']?>"><?=$value['nama_cabang']?></option> 
                                         <?php
                                          } ?>
                                          </select>
                                          <?php
                                        }
                                        elseif(!empty($cabangs)){
                                            foreach ($cabangs as $key => $values) {
                                        ?>
                                        <select class="custom-select form-control" id="cabang" name="cabang">
                                            <option value=""></option>
                                           <option value="<?=$values['id_cabang']?>"><?=$values['nama_cabang']?></option> 
                                           </select>
                                        <?php
                                        }
                                         }else{
                                            $cab = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub'")->result_array();
                                            $sub1 = $cab[0]['id_induk'];
                                            $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub1'")->result_array();
                                          foreach ($sqlss as $key => $vass) {
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang" disabled="">
                                            <option value="<?=$vass['id_cabang']?>"><?=$vass['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Periode :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="location3" name="periodebulan">
                                        <option value=""></option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                    <div>
                                        <select class="custom-select form-control" id="location3" name="periodetahun">
                                        <option value=""></option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                        <option value="2026">2026</option>
                                        <option value="2027">2027</option>
                                        <option value="2028">2028</option>
                                        <option value="2029">2029</option>
                                        <option value="2030">2030</option>
                                    </select>
                                    </div>
                                </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Capem / Kedai :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                        $sub = substr($as,4);
                                        $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BWS') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BWS') === false) {
                                         ?>
                                         <select class="custom-select form-control" id="capem" name="capem">
                                        </select>
                                         <?php
                                        } elseif(!empty($cabangs)){
                                        ?>
                                            <select class="custom-select form-control" id="capem" name="capem">
                                            </select>
                                        <?php
                                        } else{
                                            $sub = substr($as,4);
                                            $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub'")->result_array();
                                          foreach ($sqlss as $key => $va) {
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang" disabled="">
                                            <option value="<?=$va['id_cabang']?>"><?=$va['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Masa Asuransi :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="asuransis" name="asuransis">
                                        <option value=""></option>
                                        <option value="PRA">PRA BROKER</option>
                                        <option value="PASCA">PASCA BROKER</option>
                                    </select>
                                </div>
                                </div>
                            </div>
                           </div>

                           <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Status Klaim :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="statusklaims" name="statusklaims">
                                        <option value=""></option>
                                        <option value="PROSES CABANG">PROSES CABANG</option>
                                        <option value="PROSES INSURANCE PARTNER">PROSES INSURANCE PARTNER</option>
                                        <option value="PROSES DITERIMA (PROSES BAYAR)">PROSES DITERIMA (PROSES BAYAR)</option>
                                        <option value="CLAIM PAID">CLAIM PAID</option>
                                        <option value="DITOLAK">DITOLAK</option>
                                        <option value="DISPUTE">DISPUTE</option>
                                    </select>
                                </div>
                                </div>
                            </div>
                           </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="waves-effect waves-light btn btn-rounded btn-primary-light mb-5"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
                                </div>
                            </div>
                        </div>
                        </form>
                        <br>

                    <div class="table-responsive">
                      <table class="table">
                        <thead class="bg-dark">
                            <tr>
                                <th class="text-center">Action</th>
                                <th class="text-center"><div style="width: max-content;">CIF No.</div></th>
                                <th class="text-center"><div style="width: max-content;">Kode Cabang</div></th>
                                <th class="text-center"><div style="width: max-content;">Wilayah</div></th>
                                <th class="text-center"><div style="width: max-content;">Nama Cabang</div></th>
                                <th class="text-center"><div style="width: max-content;">Nama Debitur</div></th>
                                <th class="text-center"><div style="width: max-content;">Tanggal Lahir</div></th>
                                <th class="text-center"><div style="width: max-content;">No. Polis Asuransi</div></th>
                                <th class="text-center"><div style="width: max-content;">Nama Mitra Asuransi</div></th>
                                <th class="text-center"><div style="width: max-content;">Tanggal Submit Tagihan Klaim</div></th>
                                <th class="text-center"><div style="width: max-content;">Outstanding Total Klaim</div></th>
                                <th class="text-center"><div style="width: max-content;">Status Klaim</div></th>
                                <th class="text-center"><div style="width: max-content;">Remark Mitra Asuransi</div></th>
                            </tr>
                        </thead>
                        <?php $no=1; foreach ($datana as $key => $value) { 
                                ?>
                                <tr id="<?php echo $value['kodeklaim']; ?>">
                                <?php
                                $user = strtoupper($this->session->userdata('KodeUser'));
                                if (strpos($user, 'BWS') === false) {
                                ?>
                                <td class="text-center">
                                    <div style="width: max-content;">
                                        <a href="<?=base_url('editklaim/index/'.$value['id_kla'])?>" class="waves-effect waves-light btn btn-rounded btn-primary-light mb-5"><i class="glyphicon glyphicon-pencil"></i></a>
                                </div>
                                </td>
                                <?php
                                }
                                ?>
                                <td class="text-center"><div style="width: max-content;"><?=$value['cif']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['kodecabang']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['wilayah']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['namacabang']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['nama']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?php
                                $test = preg_replace( "/\r|\n/", " ", $value['tgllahir']);
                                $tanggal = new DateTime($test);
                                $today = new DateTime('today');
                                $y = $today->diff($tanggal)->y;
                                $m = $today->diff($tanggal)->m;
                                $d = $today->diff($tanggal)->d;
                                if ($m >= 6 && $d > 0) {
                                    $year = $y + 1;
                                } else{
                                    $year = $y;
                                }
                                echo date("d/m/Y", strtotime($value['tgllahir'])). ' ('.$year.'th)';
                                ?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['polis']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['asuransi']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['tgllapor']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['outstandingtotalklaim']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['statusklaim']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['remarkasuransi']?></div></td>
                                
                                
                            </tr>  
                            <?php } ?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                      <nav aria-label="Page navigation">
                    <?php echo $this->pagination->create_links(); ?>
                    </nav>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>
                        </div>
                        <div class="tab-pane" id="profile2" role="tabpanel">
                        <div class="box box-default">
            <div class="box-header with-border">
            </div>
            <!-- /.box-header -->
            <div class="box-body wizard-content">

          <form class="validation-wizard wizard-circle" method="post" action="<?=base_url('pengajuanklaim/save')?>" id="adding">
                    <!-- Step 1 -->
                    <h6>Pengajuan Klaim</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">No. Klaim<span class="text-danger"> *</span> :</label>
                  <input type="text" class="form-control" id="status_step" name="status_step" value="<?=$status?>" style="display: none;">
                        <input type="text" class="form-control" id="noklaim" name="noklaim" required="" value="<?=$kodekla?>" readonly=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Tanggal Pengajuan dari Bank<span class="text-danger"> *</span> :</label>
                                    <input type="date" class="form-control" id="tglsuratbank" name="tglsuratbank" required=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">No. Loan Kredit<span class="text-danger"> *</span> :</label>
                                    <input type="text" class="form-control" id="noloan" name="noloan" required=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">No. CIF<span class="text-danger"> *</span> :</label>
                                    <input type="text" class="form-control" id="nocif" name="nocif" required=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">Nama Debitur<span class="text-danger"> *</span> :</label>
                                    <input type="text" class="form-control" id="namadebitur" name="namadebitur" required=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Tanggal Lahir<span class="text-danger"> *</span> :</label>
                                    <input type="date" class="form-control" id="tgllahir" name="tgllahir" required=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">No. Sertifikat Polis<span class="text-danger"> *</span> :</label>
                                    <input type="text" class="form-control" id="nosertifikatpolis" name="nosertifikatpolis" required=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">Kantor Cabang (KC)<span class="text-danger"> *</span> :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                          $sub = substr($as,4);
                                        $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BWS') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BWS') === false) {
                                            $sql = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and nama_cabang LIKE '%KC%'")->result_array();
                                         ?>
                                         <select class="custom-select form-control" id="cabangs" name="cabangs">
                                            <option value="" required=""></option>
                                         <?php
                                          foreach ($sql as $key => $value) {
                                         ?>
                                           <option value="<?=$value['id_cabang']?>" required=""><?=$value['nama_cabang']?></option> 
                                         <?php
                                          } ?>
                                          </select>
                                          <?php
                                        }
                                        elseif(!empty($cabangs)){
                                            foreach ($cabangs as $key => $values) {
                                        ?>
                                        <select class="custom-select form-control" id="cabangs" name="cabangs">
                                            <option value=""></option>
                                           <option value="<?=$values['id_cabang']?>" required=""><?=$values['nama_cabang']?></option> 
                                           </select>
                                        <?php
                                        }
                                         }else{
                                            $cab = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub'")->result_array();
                                            $sub1 = $cab[0]['id_induk'];
                                            $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub1'")->result_array();
                                          foreach ($sqlss as $key => $vass) {
                                         ?>
                                         <select class="custom-select form-control" id="cabangs" name="cabangs" disabled="">
                                            <option value="<?=$vass['id_cabang']?>" required=""><?=$vass['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Kantor Cabang Pembantu (KCP)<span class="text-danger"> *</span> :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                        $sub = substr($as,4);
                                        $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BWS') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BWS') === false) {
                                         ?>
                                         <select class="custom-select form-control" id="capems" name="capems">
                                        </select>
                                         <?php
                                        } elseif(!empty($cabangs)){
                                        ?>
                                            <select class="custom-select form-control" id="capems" name="capems">
                                            </select>
                                        <?php
                                        } else{
                                            $sub = substr($as,4);
                                            $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub'")->result_array();
                                          foreach ($sqlss as $key => $va) {
                                         ?>
                                         <select class="custom-select form-control" id="cabangs" name="cabangs" disabled="">
                                            <option value="<?=$va['id_cabang']?>" required=""><?=$va['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">Nama Perusahaan Asuransi<span class="text-danger"> *</span> :</label>
                                    <select class="custom-select form-control" id="asuransi" name="asuransi" required="">
                   <option value=""></option>
                    <?php
                    $sqlss = $this->db->query("SELECT kode_asuransi, nama_asuransi FROM PJM_SAUDARA.dbo.Asuransi")->result_array();
                    foreach ($sqlss as $key => $vass) {
                     ?>
                        <option value="<?=$vass['kode_asuransi']?>" required=""><?=$vass['nama_asuransi']?></option>
                     <?php
                        }
                        ?>
                      </select>
                            </div>
                        </div>
                    </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">Fasilitas Kredit<span class="text-danger"> *</span> :</label>
                  <select class="custom-select form-control" id="tipemanfaat" name="tipemanfaat" required="">
                   <option value=""></option>
                    <?php
                    $sqlss = $this->db->query("SELECT kode_faskre, nama_faskre FROM PJM_SAUDARA.dbo.FasilitasKredit")->result_array();
                    foreach ($sqlss as $key => $vass) {
                     ?>
                        <option value="<?=$vass['nama_faskre']?>" required=""><?=$vass['nama_faskre']?></option>
                     <?php
                        }
                        ?>
                      </select></div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-12">
                                <div class="form-group">
                                    <label for="location3">Jenis Klaim<span class="text-danger"> *</span> :</label>
                  <select class="custom-select form-control" id="jenisklaim" name="jenisklaim">
                  </select>
                                </div>
                            </div>
                        </div>
                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Tanggal Kematian<span class="text-danger"> *</span> :</label>
                                    <input type="date" class="form-control" id="tglkejadian" name="tglkejadian" required=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">Nilai Plafond Kredit (yang dipertanggungkan)<span class="text-danger"> *</span> :</label>
                                    <input type="text" class="form-control" id="nilaiplafon" name="nilaiplafon" required=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">Nilai Pengajuan Klaim<span class="text-danger"> *</span> :</label>
                                    <input type="text" class="form-control" id="nilaipengajuanklaim" name="nilaipengajuanklaim" placeholder="0" required=""></div>
                            </div>
                        </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">Nilai Bunga Pengajuan Klaim :</label>
                  <input type="text" class="form-control" id="nilaibungapengajuanklaim" name="nilaibungapengajuanklaim" placeholder="0" disabled="disabled"></div>
              </div>
            </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="button" id="searchForm" onclick="SubmitForm();" class="waves-effect waves-light btn btn-info mb-7">Save</button>
                        </div>
                    </div>
                </div>
            </section>

                    <!-- Step 2 -->
                    <h6>Info Ke Asuransi</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">Jasa Ekspedisi :</label>
                                    <input type="text" class="form-control" id="noklaim" name="noklaim" style="display: none;">
                                    <select class="custom-select form-control" id="ekspedisi" name="ekspedisi">
                                        <option value=""></option>
                                        <option value="JNE">JNE</option>
                                        <option value="DAKOTA">DAKOTA</option>
                                        <option value="J&T">J&T</option>
                                        <option value="POS">POS</option>
                                        <option value="SICEPAT">SICEPAT</option>
                                        <option value="TIKI">TIKI</option>
                                    </select></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">No. Resi :</label>
                                    <input type="text" class="form-control" id="noresi" name="noresi" required=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Tanggal kirim via Ekspedisi :</label>
                                    <input type="date" class="form-control" name="tglekspedisi" id="tglekspedisi" required=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="button" id="searchForm2" onclick="SubmitForm2();" class="waves-effect waves-light btn btn-info mb-7">Save</button>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- Step 3 -->
                    <h6>Kelengkapan & Persetujuan Klaim</h6>
                    <section>
                        <input type="text" class="form-control" id="noklaim" name="noklaim" style="display: none;">
                    <?php

                    if (!empty($noklaim)) {
                
                $ceks = $this->db->query("select * from PJM_SAUDARA.dbo.Klaim where kodeklaim = $noklaim");

                if ($ceks->num_rows() <> 0) {
                    $cek->row();

                if ($cek->asuransi == 'ASEI' and $cek->kode_jenisklaim == 'MD001' ) { ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peryataan Kesehatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sp_kesehatan" name="sp_kesehatan">
                      <label class="custom-file-label" for="sp_kesehatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencarian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Terusan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skterusan" name="fc_skterusan">
                      <label class="custom-file-label" for="fc_skterusan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Keterangan Polisi</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_polisi" name="sk_polisi">
                      <label class="custom-file-label" for="sk_polisi">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Keterangan Dokter</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="skematian_dokter" name="skematian_dokter">
                      <label class="custom-file-label" for="skematian_dokter">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>
                          
                      
              <?php } else if ($cek->asuransi == 'ASEI' and $cek->kode_jenisklaim == 'PHK001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peryataan Kesehatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sp_kesehatan" name="sp_kesehatan">
                      <label class="custom-file-label" for="sp_kesehatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencarian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Terusan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skterusan" name="fc_skterusan">
                      <label class="custom-file-label" for="fc_skterusan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Tuntutan Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_tuntutanklaim" name="s_tuntutanklaim">
                      <label class="custom-file-label" for="s_tuntutanklaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK PHK</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_phk" name="sk_phk">
                      <label class="custom-file-label" for="sk_phk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peringatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_peringatan" name="s_peringatan">
                      <label class="custom-file-label" for="s_peringatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'ASEI' and $cek->kode_jenisklaim == 'KM001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peryataan Kesehatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sp_kesehatan" name="sp_kesehatan">
                      <label class="custom-file-label" for="sp_kesehatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencarian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Terusan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skterusan" name="fc_skterusan">
                      <label class="custom-file-label" for="fc_skterusan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peringatan dari Bank ke Debitur</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sp_bank" name="sp_bank">
                      <label class="custom-file-label" for="sp_bank">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Laporan Kunjungan ke Debitur</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="lap_kunjungandebitur" name="lap_kunjungandebitur">
                      <label class="custom-file-label" for="lap_kunjungandebitur">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Kronologi Macet</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="kronologi_macet" name="kronologi_macet">
                      <label class="custom-file-label" for="kronologi_macet">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Slik OJK</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="slik_ojk" name="slik_ojk">
                      <label class="custom-file-label" for="slik_ojk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <?php } else if ($cek->asuransi == 'ASEI' and $cek->kode_jenisklaim == 'PHK001' ) {  ?>

                  <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peryataan Kesehatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sp_kesehatan" name="sp_kesehatan">
                      <label class="custom-file-label" for="sp_kesehatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencarian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Terusan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skterusan" name="fc_skterusan">
                      <label class="custom-file-label" for="fc_skterusan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Tuntutan Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_tuntutanklaim" name="s_tuntutanklaim">
                      <label class="custom-file-label" for="s_tuntutanklaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK PHK</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_phk" name="sk_phk">
                      <label class="custom-file-label" for="sk_phk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peringatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_peringatan" name="s_peringatan">
                      <label class="custom-file-label" for="s_peringatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'ASEI' and $cek->kode_jenisklaim == 'PAW001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peryataan Kesehatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sp_kesehatan" name="sp_kesehatan">
                      <label class="custom-file-label" for="sp_kesehatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencarian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Terusan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skterusan" name="fc_skterusan">
                      <label class="custom-file-label" for="fc_skterusan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Tuntutan Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_tuntutanklaim" name="s_tuntutanklaim">
                      <label class="custom-file-label" for="s_tuntutanklaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK PAW</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_paw" name="sk_paw">
                      <label class="custom-file-label" for="sk_paw">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peringatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_peringatan" name="s_peringatan">
                      <label class="custom-file-label" for="s_peringatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'ASKRINDO' and $cek->kode_jenisklaim == 'MD001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Polis</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="polis" name="polis">
                      <label class="custom-file-label" for="polis">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencairan Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Kuasa Debitur ke Bendahara</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_debiturbendahara" name="sk_debiturbendahara">
                      <label class="custom-file-label" for="sk_debiturbendahara">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Kematian Dokter</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="skematian_dokter" name="skematian_dokter">
                      <label class="custom-file-label" for="skematian_dokter">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'ASKRINDO' and $cek->kode_jenisklaim == 'PHK001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Polis</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="polis" name="polis">
                      <label class="custom-file-label" for="polis">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencairan Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Kuasa Debitur ke Bendahara</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_debiturbendahara" name="sk_debiturbendahara">
                      <label class="custom-file-label" for="sk_debiturbendahara">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK PHK</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_phk" name="sk_phk">
                      <label class="custom-file-label" for="sk_phk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'ASKRINDO' and $cek->kode_jenisklaim == 'KM001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Polis</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="polis" name="polis">
                      <label class="custom-file-label" for="polis">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencairan Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Kuasa Debitur ke Bendahara</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_debiturbendahara" name="sk_debiturbendahara">
                      <label class="custom-file-label" for="sk_debiturbendahara">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Berita Acara Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="ba_klaim" name="ba_klaim">
                      <label class="custom-file-label" for="ba_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Tagihan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_tagihan" name="s_tagihan">
                      <label class="custom-file-label" for="s_tagihan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Slik OJK</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="slik_ojk" name="slik_ojk">
                      <label class="custom-file-label" for="slik_ojk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'BUMIPUTERA' and $cek->kode_jenisklaim == 'MD001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Keterangan Meninggal Dunia</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="keterangan_meninggal" name="keterangan_meninggal">
                      <label class="custom-file-label" for="keterangan_meninggal">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Keluarga</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kk" name="fc_kk">
                      <label class="custom-file-label" for="fc_kk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Jadwal Angguran Pokok</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="jadwal_angsuranpokok" name="jadwal_angsuranpokok">
                      <label class="custom-file-label" for="jadwal_angsuranpokok">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Pembayaran Premi</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_pembayaranpremi" name="fc_pembayaranpremi">
                      <label class="custom-file-label" for="fc_pembayaranpremi">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Sertifikat/polis asuransi</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="polis" name="polis">
                      <label class="custom-file-label" for="polis">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <?php } else if ($cek->asuransi == 'JASINDO' and $cek->kode_jenisklaim == 'MD001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Polis + Lampiran</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="polis" name="polis">
                      <label class="custom-file-label" for="polis">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Klaim + nilai nya</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_klaim" name="s_klaim">
                      <label class="custom-file-label" for="s_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Keterangan Meninggal Dunia</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="keterangan_meninggal" name="keterangan_meninggal">
                      <label class="custom-file-label" for="keterangan_meninggal">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Laporan Kerugian Diisi Bank</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="lap_kerugian" name="lap_kerugian">
                      <label class="custom-file-label" for="lap_kerugian">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Jadwal Angguran Pokok</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="jadwal_angsuranpokok" name="jadwal_angsuranpokok">
                      <label class="custom-file-label" for="jadwal_angsuranpokok">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'JIWASRAYA' and $cek->kode_jenisklaim == 'MD001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Keterangan Meninggal Dunia</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="keterangan_meninggal" name="keterangan_meninggal">
                      <label class="custom-file-label" for="keterangan_meninggal">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Jadwal Angguran Pokok</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="jadwal_angsuranpokok" name="jadwal_angsuranpokok">
                      <label class="custom-file-label" for="jadwal_angsuranpokok">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Keputusan Pensiunan (Untuk Pensiun)</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_pensiun" name="sk_pensiun">
                      <label class="custom-file-label" for="sk_pensiun">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'SLU' and $cek->kode_jenisklaim == 'MD001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form kematian</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_kematian" name="form_kematian">
                      <label class="custom-file-label" for="form_kematian">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Keterangan Ahli Waris</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_ahliwaris" name="sk_ahliwaris">
                      <label class="custom-file-label" for="sk_ahliwaris">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Jadwal Angguran Pokok</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="jadwal_angsuranpokok" name="jadwal_angsuranpokok">
                      <label class="custom-file-label" for="jadwal_angsuranpokok">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'SLU' and $cek->kode_jenisklaim == 'PHK001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK PHK</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_phk" name="sk_phk">
                      <label class="custom-file-label" for="sk_phk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Jadwal Angguran Pokok</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="jadwal_angsuranpokok" name="jadwal_angsuranpokok">
                      <label class="custom-file-label" for="jadwal_angsuranpokok">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim PHK</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaimphk" name="form_klaimphk">
                      <label class="custom-file-label" for="form_klaimphk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'TASPEN LIFE' and $cek->kode_jenisklaim == 'MD001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Keterangan Meninggal Dunia</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="keterangan_meninggal" name="keterangan_meninggal">
                      <label class="custom-file-label" for="keterangan_meninggal">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Keluarga</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kk" name="fc_kk">
                      <label class="custom-file-label" for="fc_kk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Jadwal Angguran Pokok</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="jadwal_angsuranpokok" name="jadwal_angsuranpokok">
                      <label class="custom-file-label" for="jadwal_angsuranpokok">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } }?>
                   
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                 <button type="button" id="searchForm3" onclick="SubmitForm3();" class="waves-effect waves-light btn btn-info mb-7">Save</button>
                                </div>
                            </div>
                        </div>
                         <?php } ?>
                    </section>
                    <!-- Step 4 -->
                    <h6>Penitipan Klaim Ke Broker</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">Nilai Pengajuan Klaim :</label>
                  <input type="text" class="form-control" id="noklaim" name="noklaim" style="display: none;">
                                    <input type="text" class="form-control" id="nilaipengajuanklaim2" name="nilaipengajuanklaim2" readonly="" placeholder="0"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">Nilai Persetujuan Klaim :</label>
                                    <input type="text" class="form-control" id="nilaipersetujuanklaim" name="nilaipersetujuanklaim" readonly="" placeholder="0"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Nilai Klaim yang Dibayar :</label>
                                    <div class="input-group mb-3">
                                <input type="text" class="form-control" id="nilaiklaimdibayar" name="nilaiklaimdibayar" placeholder="0">
                                    <div class="clearfix">
                                        <button type="button" class="waves-effect waves-light btn mb-8 bg-gradient-danger">SAMAKAN</button>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Tanggal Pembayaran Klaim :</label>
                                    <input type="date" class="form-control" id="date1" name="tglpembayaran"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                  <button type="button" id="searchForm4" onclick="SubmitForm4();" class="waves-effect waves-light btn btn-info mb-7">Save</button>
                                </div>
                            </div>
                        </div>
                    </section>

                    <h6>Pembayaran Klaim</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">Nilai Pengajuan Klaim :</label>
                  <input type="text" class="form-control" id="noklaim" name="noklaim" style="display: none;">
                                    <input type="text" class="form-control" id="nilaipengajuanklaim3" name="nilaipengajuanklaim3" readonly="" placeholder="0"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">Nilai Persetujuan Klaim :</label>
                                    <input type="text" class="form-control" id="nilaipersetujuanklaim1" name="nilaipersetujuanklaim1" readonly="" placeholder="0"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">Nilai Klaim yang dibayar ke Broker :</label>
                                    <input type="text" class="form-control" id="nilaiklaimdibayarbroker" name="nilaiklaimdibayarbroker" readonly="" placeholder="0"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Nilai Klaim yang Dibayar ke Bank :</label>
                                    <div class="input-group mb-3">
                                <input type="text" class="form-control" id="nilaiklaimdibayarbank" name="nilaiklaimdibayarbank" placeholder="0">
                                    <div class="clearfix">
                                        <button type="button" class="waves-effect waves-light btn mb-8 bg-gradient-danger">SAMAKAN</button>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Tanggal Pembayaran Klaim :</label>
                                    <input type="date" class="form-control" id="date1" name="tglpembayaran1"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                  <button type="button" id="searchForm5" onclick="SubmitForm5();" class="waves-effect waves-light btn btn-info mb-7">Save</button>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            </div>      
        </section>
        <!-- /.content -->
      </div>

    <script>
      function SubmitForm() {
        let element = document.getElementById(noklaim);

        var noklaim = $("#noklaim").val();
        var tglsuratbank = $("#tglsuratbank").val();
        var noloan = $("#noloan").val();
        var nocif = $("#nocif").val();
        var namadebitur = $("#namadebitur").val();
        var tgllahir = $("#tgllahir").val();
        var nosertifikatpolis = $("#nosertifikatpolis").val();
        var capems = $("#capems").val();
        var asuransi = $("#asuransi").val();
        var produk = $("#produk").val();
        var tipemanfaat = $("#tipemanfaat").val();
        var jenisklaim = $("#jenisklaim").val();
        var tglkejadian = $("#tglkejadian").val();
        var nilaiplafon = $("#nilaiplafon").val();
        var nilaipengajuanklaim = $("#nilaipengajuanklaim").val();
        var nilaibungapengajuanklaim = $("#nilaibungapengajuanklaim").val();
        
        
        $.ajax({
            type      : 'POST',
            url       : '<?php echo base_url();?>' + 'pengajuanklaim/save', //Your form processing file URL
            data      : {
                          noklaim : noklaim,
                          tglsuratbank : tglsuratbank,
                          noloan : noloan,
                          nocif : nocif,
                          namadebitur : namadebitur,
                          tgllahir : tgllahir,
                          nosertifikatpolis : nosertifikatpolis,
                          capems : capems,
                          asuransi : asuransi,
                          produk : produk,
                          tipemanfaat : tipemanfaat,
                          jenisklaim : jenisklaim,
                          tglkejadian : tglkejadian,
                          nilaiplafon : nilaiplafon,
                          nilaipengajuanklaim : nilaipengajuanklaim,
                          nilaibungapengajuanklaim : nilaibungapengajuanklaim
                        }, 
            // dataType  : 'json',
            success   : function(data) {
                console.log(data);
                alert("Step 1 Submitted Sucessfully!");
                // window.alert('Step 1 Submitted Sucessfully!');
                window.location='<?=base_url('editklaim/index/')?>' + data;

             }
        });
      }

      function SubmitForm2() {
        var ekspedisi = $("#ekspedisi").val();
        var noresi = $("#noresi").val();
        var tglekspedisi = $("#tglekspedisi").val();
        var noklaim = $("#noklaim").val();

        $.ajax({
            type      : 'POST',
            url       : '<?php echo base_url();?>' + 'pengajuanklaim/updatestepdua', //Your form processing file URL
            data      : {
                          noklaim : noklaim,
                          ekspedisi : ekspedisi,
                          noresi : noresi,
                          tglekspedisi : tglekspedisi
                        }, 
            // dataType  : 'json',
            success   : function(data) {
              // alert(data);

                alert("Step 2 Submitted Sucessfully!");

                
            }
        });
      }

    </script>

<script src="<?=base_url()?>assets/main/js/jquery-1.10.0.min.js"></script>
<script>
  $(document).ready(function() {
      $('#asuransi').change(function(){
          if($(this).val() === 'AS001' || $(this).val() === 'AS002' || $(this).val() === 'BP001' || $(this).val() === 'JS002') {
              $('#nilaibungapengajuanklaim').prop('disabled', false);
          } else {
              $('#nilaibungapengajuanklaim').prop('disabled', true);
          }
      });
  });
</script>
<script>
    $(document).ready(function(){
        var base_url = '<?php echo base_url();?>';
    $('#asuransi').change(function(){
        var id = $('#asuransi').val();
        $.ajax({
        type      : "POST",
        url       : base_url + 'pengajuanklaim/get_jenisklaim',
        data      : {id : id},
        // async     : false,
        dataType  : 'json',
        success   : function(data) {
            // alert(data);
            var html = '';
            var i;
            for(i=0; i<data.length; i++){
                html += '<option value='+data[i].kode_jenisklaim+'>'+data[i].nama_jenisklaim+'</option>';
            }
            $("#jenisklaim").html(html);
    }
    });
    });
});
</script>

<script>
    $(document).ready(function(){
        var base_url = '<?php echo base_url();?>';
    $('#cabang').change(function(){
        var id = $('#cabang').val();
        $.ajax({
        type      : "POST",
        url       : base_url + 'pengajuanklaim/get_capem',
        data      : {id : id},
        // async     : false,
        dataType  : 'json',
        success   : function(data) {
            // alert(data);
            var html = '';
            var i;
            for(i=0; i<data.length; i++){
                html += '<option value='+data[i].id_cabang+'>'+data[i].nama_cabang+'</option>';
            }
            $("#capem").html(html);
    }
    });
    });
});
</script>

<script>
    $(document).ready(function(){
        var base_url = '<?php echo base_url();?>';
    $('#cabangs').change(function(){
        var id = $('#cabangs').val();
        $.ajax({
        type      : "POST",
        url       : base_url + 'pengajuanklaim/get_capem',
        data      : {id : id},
        // async     : false,
        dataType  : 'json',
        success   : function(data) {
            // alert(data);
            var html = '';
            var i;
            for(i=0; i<data.length; i++){
                html += '<option value='+data[i].id_cabang+'>'+data[i].nama_cabang+'</option>';
            }
            $("#capems").html(html);
    }
    });
    });
});
</script>
<script type="text/javascript">

 /* Tanpa Rupiah */
 var tanpa_rupiah = document.getElementById('nilaiplafon');
 tanpa_rupiah.addEventListener('keyup', function(e)
 {
  tanpa_rupiah.value = formatRupiah(this.value);
 });
 
 /* Dengan Rupiah */
 var dengan_rupiah = document.getElementById('nilaipengajuanklaim');
 dengan_rupiah.addEventListener('keyup', function(e)
 {
  dengan_rupiah.value = formatRupiah(this.value, 'Rp. ');
 });
 
 /* Fungsi */
 function formatRupiah(angka, prefix)
 {
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
   split = number_string.split(','),
   sisa  = split[0].length % 3,
   rupiah  = split[0].substr(0, sisa),
   ribuan  = split[0].substr(sisa).match(/\d{3}/gi);
   
  if (ribuan) {
   separator = sisa ? '.' : '';
   rupiah += separator + ribuan.join('.');
  }
  
  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
 }
</script>
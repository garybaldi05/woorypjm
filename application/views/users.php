 <div class="container-full">
		<!-- Main content -->
		<section class="content">
			<div class="row">
			<div class="col-12">
			  <div class="box box-default">
			  	<div class="box-header with-border">
                  <h4 class="box-title">Data User</h4>
                </div>
				<!-- /.box-header -->
				<div class="box-body">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs customtab" role="tablist">
						<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home2" role="tab"><span class="hidden-sm-up"><i class="ion-home"></i></span> <span class="hidden-xs-down">Data</span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile2" role="tab"><span class="hidden-sm-up"><i class="ion-person"></i></span> <span class="hidden-xs-down">Tambah</span></a> </li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="home2" role="tabpanel">
						<div class="box">
				<div class="box-header with-border">
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Kode User :</label>
                                    <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder="Kode User">
                                </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Nama User :</label>
                                    <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder="Nama User">
                                	</div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="button" class="waves-effect waves-light btn btn-rounded btn-primary-light mb-5"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
                                </div>
                            </div>
                        </div>
                        <br>

					<div class="table-responsive">
					  <table id="example1" class="table">
						<thead class="bg-dark">
							<tr>
								<th>Kode User</th>
								<th>Nama User</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php  foreach ($datana as $key => $value) { ?>
                            <tr id="<?php echo $value['KodeUser']; ?>">
                                <td class="text-center"><?=$value['KodeUser']?></td>
                                <td class="text-center"><?=$value['NamaUser']?></td>
                                <td class="text-center">
                                    <a title="Change"
                                       href="javascript:;"
                                       data-No_Journal="<?php echo $value['KodeUser'];?>"
                                       data-toggle="modal"
                                       data-target="#edit<?php echo $value['KodeUser']?>"
                                       class="btn-sm btn-info"
                                    >
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </a> &nbsp;
                                    <a href="<?php echo base_url('users/delete/'.$value['KodeUser'])?>" onclick="return confirm('Are your sure ?');" class="btn-sm btn-danger remove">
                                        <i class="glyphicon glyphicon-remove"></i>
                                    </a>
                                </td>
                            </tr>  
                            <?php } ?>
						</tbody>
						<tfoot>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
						</div>
						<div class="tab-pane" id="profile2" role="tabpanel">
						<div class="box box-default">
			<div class="box-header with-border">
			</div>
			<!-- /.box-header -->

			<form action="#" class="form-horizontal form-element">
				  <div class="box-body">
					<div class="form-group row">
					  <label for="inputEmail3" class="col-sm-2 control-label">Kode User</label>

					  <div class="col-sm-10">
						<input type="text" class="form-control" id="KodeUser" placeholder="Kode User" autocomplete="off">
					  </div>
					</div>
					<div class="form-group row">
					  <label for="inputEmail3" class="col-sm-2 control-label">Nama User</label>

					  <div class="col-sm-10">
						<input type="text" class="form-control" id="NamaUser" placeholder="Nama User" autocomplete="off">
					  </div>
					</div>
				  </div>
				  <!-- /.box-body -->
				  <div class="box-footer">
					<button type="submit" class="btn btn-rounded btn-danger">Cancel</button>
					<button type="submit" class="btn btn-rounded btn-info pull-right">Save</button>
				  </div>
				  <!-- /.box-footer -->
				</form>
		  </div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			</div>		
		</section>
		<!-- /.content -->
	  </div>
 <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h4 class="box-title">Edit Klaim</h4>
                   <input type="text" class="form-control" id="url" name="url" value="<?=base_url()?>" style="display: none;">
                </div>
                        <?php
        if($this->session->flashdata('success')){
            ?>
            <div class="alert alert-success text-center">
                <i class="glyphicon glyphicon-ok-sign"></i> <span><?=$this->session->flashdata('success')?></span>
            </div>
            <div></div>
            <?php
        }
        ?>
        <?php
        if($this->session->flashdata('error')){
            ?>
            <div class="alert alert-danger text-center">
                <i class="glyphicon glyphicon-remove-sign"></i> <span><?=$this->session->flashdata('error')?></span>
            </div>
            <div>
            </div>
        <?php   } ?>
        <!-- /.content -->
                <!-- /.box-header -->
                <?php $no=1; foreach ($datana as $key => $value) { ?>
                <div class="box-body wizard-content">
          <form class="validation-wizard wizard-circle" method="post" action="<?=base_url('pengajuanklaim/save')?>" id="adding">
                        
                          <input type="text" class="form-control" id="id_kla" name="id_kla" readonly="" value="<?= $value['id_kla'] ?>" style="display: none">
                          <input type="text" class="form-control" id="status_step" name="status_step" readonly="" value="<?= $value['status_step'] ?>" style="display: none">
<h6>Pengajuan Klaim</h6>
          <section>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">No. Klaim<span class="text-danger"> *</span> :</label>
                  <input type="text" class="form-control" id="noklaim" name="noklaim" readonly="" value="<?= $value['kodeklaim'] ?>"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="firstName5">Tanggal Pengajuan dari Bank<span class="text-danger"> *</span> :</label>
                  <?php
                        if (!empty($value['tgllapor'])) {
                        ?>
                        <input class="form-control" type="date" value="<?=$value['tgllapor'];?>" id="tgllapor" name="tgllapor">
                        <?php
                        } else{
                        ?>
                        <input class="form-control" type="date" id="tgllapor" name="tgllapor">
                  <?php
                  }
                  ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">No. Loan Kredit<span class="text-danger"> *</span> :</label>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" id="noloan" name="noloan" readonly="" value="<?= $value['loan'] ?>">
                </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">No. CIF<span class="text-danger"> *</span> :</label>
                  <input type="text" class="form-control" id="nocif" name="nocif" readonly="" value="<?= $value['cif'] ?>"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">Nama Debitur<span class="text-danger"> *</span> :</label>
                  <input type="text" class="form-control" id="namadebitur" name="namadebitur" required="" value="<?= $value['nama'] ?>"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="firstName5">Tanggal Lahir<span class="text-danger"> *</span> :</label>
                  <?php
                        if (!empty($value['tgllahir'])) {
                        ?>
                        <input class="form-control" type="date" value="<?=$value['tgllahir'];?>" id="tgllahir" name="tgllahir">
                        <?php
                        } else{
                        ?>
                        <input class="form-control" type="date" id="tgllahir" name="tgllahir">
                  <?php
                  }
                  ?></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">No. Sertifikat Polis<span class="text-danger"> *</span> :</label>
                  <input type="text" class="form-control" id="nosertifikatpolis" name="nosertifikatpolis" required="" value="<?= $value['polis'] ?>"></div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="firstName5">Kantor Cabang (KC) / Kantor Cabang Pembantu (KCP)<span class="text-danger"> *</span> :</label>
                        <div class="input-group mb-3">
                          <select class="custom-select form-control" id="cabangs" name="cabangs">
                          <?php
                            $po = $value['kodecabang'];
                            $sqll = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$po'")->result_array();
                            $b = $sqll[0]['nama_cabang'];
                            ?>
                            <option value="<?php echo $value['kodecabang'];?>"><?php echo $b?></option>
                            <?php 
                              $sqls = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang")->result_array();
                              foreach ($sqls as $key => $values) {
                             ?>
                               <option value="<?=$values['id_cabang']?>"><?=$values['nama_cabang']?></option> 
                             <?php
                              }
                             ?>
                           </select>
                    </div>
                    </div>
                </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">Nama Perusahaan Asuransi<span class="text-danger"> *</span> :</label>
                  <select class="custom-select form-control" id="asuransi" name="asuransi">
                  <?php
                    $as = $value['asuransi'];
                    $asu = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Asuransi where nama_asuransi = '$as'")->result_array();
                    $bb = $asu[0]['nama_asuransi'];
                    $c = $asu[0]['kode_asuransi'];
                    ?>
                    <option value="<?php echo $value['asuransi'];?>"><?php echo $bb?></option>
                    <?php 
                      $sqls = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Asuransi")->result_array();
                      foreach ($sqls as $key => $values) {
                     ?>
                       <option value="<?=$values['nama_asuransi']?>"><?=$values['nama_asuransi']?></option> 
                     <?php
                      }
                     ?>
                   </select>
              </div>
            </div>
          </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">Fasilitas Kredit<span class="text-danger"> *</span> :</label>
                  <select class="custom-select form-control" id="tipemanfaat" name="tipemanfaat">
                  <?php
                    $fa = $value['fasilitas'];
                    $sqll = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.FasilitasKredit where nama_faskre = '$fa'")->result_array();
                    $bbb = $sqll[0]['nama_faskre'];
                    ?>
                    <option value="<?php echo $value['fasilitas'];?>"><?php echo $bbb?></option>
                    <?php 
                      $sqls = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.FasilitasKredit")->result_array();
                      foreach ($sqls as $key => $values) {
                     ?>
                       <option value="<?=$values['nama_faskre']?>"><?=$values['nama_faskre']?></option> 
                     <?php
                      }
                     ?>
                   </select>
                 </div>
              </div>
            </div>
            <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                  <label for="location3">Jenis Klaim<span class="text-danger"> *</span> :</label>
                  <select class="custom-select form-control" id="jenisklaim" name="jenisklaim">
                  <?php
                    $jklaim = $value['kode_jenisklaim'];
                    $jk = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.JenisKlaim where kode_jenisklaim = '$jklaim' and kode_asuransi = '$c'")->result_array();
                    $bs = $jk[0]['nama_jenisklaim'];
                    ?>
                    <option value="<?php echo $value['kode_jenisklaim'];?>"><?php echo $bs?></option>
                    <?php 
                      $sqls = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.JenisKlaim where kode_asuransi = '$c' ")->result_array();
                      foreach ($sqls as $key => $values) {
                     ?>
                       <option value="<?=$values['kode_jenisklaim']?>"><?=$values['nama_jenisklaim']?></option> 
                     <?php
                      }
                     ?>
                   </select>
                </div>
              </div>
            </div>
              <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="firstName5">Tanggal Kematian<span class="text-danger"> *</span> :</label>
                  <?php
                        if (!empty($value['tglkematian'])) {
                        ?>
                        <input class="form-control" type="date" value="<?=$value['tglkematian'];?>" id="tglkematian" name="tglkematian">
                        <?php
                        } else{
                        ?>
                        <input class="form-control" type="date" id="tglkematian" name="tglkematian">
                  <?php
                  } ?></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">Nilai Plafond Kredit (yang dipertanggungkan)<span class="text-danger"> *</span> :</label>
                  <input type="text" class="form-control" id="nilaiplafon" name="nilaiplafon" required="" value="<?= $value['plafond'] ?>"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">Nilai Pengajuan Klaim<span class="text-danger"> *</span> :</label>
                  <input type="text" class="form-control" id="nilaipengajuanklaim" name="nilaipengajuanklaim" required="" value="<?= $value['outstandingpokok'] ?>"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">Nilai Bunga Pengajuan Klaim :</label>
                  <input type="text" class="form-control" id="nilaibungapengajuanklaim" name="nilaibungapengajuanklaim" required="" value="<?= $value['outstandingbunga'] ?>"></div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <button type="button" id="searchForm" onclick="SubmitForm();" class="waves-effect waves-light btn btn-info mb-7">Save</button>
                </div>
              </div>
            </div>
          </section>
          <!-- Step 2 -->
          <h6>Info Ke Asuransi</h6>
          <section>
            <input type="text" class="form-control" id="idk" name="idk" readonly="" value="<?= $value['id_kla'] ?>" style="display: none;">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">Jasa Ekspedisi :</label>
                    <select class="custom-select form-control" id="ekspedisi" name="ekspedisi">
                      <?php
                    $as = $value['jasaekspedisi'];
                    $idkla = $value['id_kla'];
                    $jasa = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Klaim where id_kla = '$idkla'")->result_array();
                    $eks = $jasa[0]['jasaekspedisi'];
                    ?>
                        <option value="<?php echo $as;?>"><?php echo $eks?></option>
                        <option value=""></option>
                        <option value="JNE">JNE</option>
                        <option value="DAKOTA">DAKOTA</option>
                        <option value="J&T">J&T</option>
                        <option value="POS">POS</option>
                        <option value="SICEPAT">SICEPAT</option>
                        <option value="TIKI">TIKI</option>
                    </select></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">No. Resi :</label>
                  <input type="text" class="form-control" id="noresi" name="noresi" required="" value="<?= $value['noresi'] ?>"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="firstName5">Tanggal kirim via Ekspedisi :</label>
                  <input type="date" class="form-control" name="tglekspedisi" id="tglekspedisi" required="" value="<?= $value['tglkirim'] ?>"></div>
              </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Bukti Jasa Ekspedisi</label>
                        <div class="custom-file">
                        <input type="file" class="form-control" name="buktijasa" id="buktijasa" accept="all" value="c:/<?php echo $value['buktijasa']?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <button type="button" id="searchForm2" onclick="SubmitForm2();" class="waves-effect waves-light btn btn-info mb-7">Save</button>
                </div>
              </div>
            </div>
          </section>
          <!-- Step 3 -->
          <h6>Kelengkapan & Persetujuan Klaim</h6>
          <section>
              <?php

                $idna = $this->uri->segment(3);
                
                $cek = $this->db->query("select * from PJM_SAUDARA.dbo.Klaim where id_kla = $idna")->row();

                if ($cek->asuransi == 'ASEI' and $cek->kode_jenisklaim == 'MD001' ) { ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peryataan Kesehatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sp_kesehatan" name="sp_kesehatan">
                      <label class="custom-file-label" for="sp_kesehatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencarian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Terusan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skterusan" name="fc_skterusan">
                      <label class="custom-file-label" for="fc_skterusan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Keterangan Polisi</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_polisi" name="sk_polisi">
                      <label class="custom-file-label" for="sk_polisi">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Keterangan Dokter</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="skematian_dokter" name="skematian_dokter">
                      <label class="custom-file-label" for="skematian_dokter">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>
                          
                      
              <?php } else if ($cek->asuransi == 'ASEI' and $cek->kode_jenisklaim == 'PHK001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peryataan Kesehatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sp_kesehatan" name="sp_kesehatan">
                      <label class="custom-file-label" for="sp_kesehatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencarian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Terusan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skterusan" name="fc_skterusan">
                      <label class="custom-file-label" for="fc_skterusan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Tuntutan Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_tuntutanklaim" name="s_tuntutanklaim">
                      <label class="custom-file-label" for="s_tuntutanklaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK PHK</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_phk" name="sk_phk">
                      <label class="custom-file-label" for="sk_phk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peringatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_peringatan" name="s_peringatan">
                      <label class="custom-file-label" for="s_peringatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'ASEI' and $cek->kode_jenisklaim == 'KM001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peryataan Kesehatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sp_kesehatan" name="sp_kesehatan">
                      <label class="custom-file-label" for="sp_kesehatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencarian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Terusan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skterusan" name="fc_skterusan">
                      <label class="custom-file-label" for="fc_skterusan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peringatan dari Bank ke Debitur</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sp_bank" name="sp_bank">
                      <label class="custom-file-label" for="sp_bank">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Laporan Kunjungan ke Debitur</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="lap_kunjungandebitur" name="lap_kunjungandebitur">
                      <label class="custom-file-label" for="lap_kunjungandebitur">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Kronologi Macet</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="kronologi_macet" name="kronologi_macet">
                      <label class="custom-file-label" for="kronologi_macet">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Slik OJK</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="slik_ojk" name="slik_ojk">
                      <label class="custom-file-label" for="slik_ojk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <?php } else if ($cek->asuransi == 'ASEI' and $cek->kode_jenisklaim == 'PHK001' ) {  ?>

                  <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peryataan Kesehatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sp_kesehatan" name="sp_kesehatan">
                      <label class="custom-file-label" for="sp_kesehatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencarian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Terusan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skterusan" name="fc_skterusan">
                      <label class="custom-file-label" for="fc_skterusan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Tuntutan Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_tuntutanklaim" name="s_tuntutanklaim">
                      <label class="custom-file-label" for="s_tuntutanklaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK PHK</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_phk" name="sk_phk">
                      <label class="custom-file-label" for="sk_phk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peringatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_peringatan" name="s_peringatan">
                      <label class="custom-file-label" for="s_peringatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'ASEI' and $cek->kode_jenisklaim == 'PAW001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peryataan Kesehatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sp_kesehatan" name="sp_kesehatan">
                      <label class="custom-file-label" for="sp_kesehatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencarian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC SK Terusan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skterusan" name="fc_skterusan">
                      <label class="custom-file-label" for="fc_skterusan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Tuntutan Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_tuntutanklaim" name="s_tuntutanklaim">
                      <label class="custom-file-label" for="s_tuntutanklaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK PAW</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_paw" name="sk_paw">
                      <label class="custom-file-label" for="sk_paw">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Peringatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_peringatan" name="s_peringatan">
                      <label class="custom-file-label" for="s_peringatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'ASKRINDO' and $cek->kode_jenisklaim == 'MD001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Polis</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="polis" name="polis">
                      <label class="custom-file-label" for="polis">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencairan Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Kuasa Debitur ke Bendahara</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_debiturbendahara" name="sk_debiturbendahara">
                      <label class="custom-file-label" for="sk_debiturbendahara">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Kematian Dokter</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="skematian_dokter" name="skematian_dokter">
                      <label class="custom-file-label" for="skematian_dokter">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'ASKRINDO' and $cek->kode_jenisklaim == 'PHK001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Polis</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="polis" name="polis">
                      <label class="custom-file-label" for="polis">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencairan Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Kuasa Debitur ke Bendahara</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_debiturbendahara" name="sk_debiturbendahara">
                      <label class="custom-file-label" for="sk_debiturbendahara">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK PHK</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_phk" name="sk_phk">
                      <label class="custom-file-label" for="sk_phk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'ASKRINDO' and $cek->kode_jenisklaim == 'KM001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Polis</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="polis" name="polis">
                      <label class="custom-file-label" for="polis">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Loan Quiry</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="loan_quiry" name="loan_quiry">
                      <label class="custom-file-label" for="loan_quiry">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Bukti Pencairan Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="bukti_pencariankredit" name="bukti_pencariankredit">
                      <label class="custom-file-label" for="bukti_pencariankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK Pengangkatan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_skpengangkatan" name="fc_skpengangkatan">
                      <label class="custom-file-label" for="fc_skpengangkatan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Tanda Pegawai</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kartutandapegawai" name="fc_kartutandapegawai">
                      <label class="custom-file-label" for="fc_kartutandapegawai">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Kuasa Debitur ke Bendahara</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_debiturbendahara" name="sk_debiturbendahara">
                      <label class="custom-file-label" for="sk_debiturbendahara">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Berita Acara Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="ba_klaim" name="ba_klaim">
                      <label class="custom-file-label" for="ba_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Tagihan</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_tagihan" name="s_tagihan">
                      <label class="custom-file-label" for="s_tagihan">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Slik OJK</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="slik_ojk" name="slik_ojk">
                      <label class="custom-file-label" for="slik_ojk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'BUMIPUTERA' and $cek->kode_jenisklaim == 'MD001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Keterangan Meninggal Dunia</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="keterangan_meninggal" name="keterangan_meninggal">
                      <label class="custom-file-label" for="keterangan_meninggal">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Keluarga</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kk" name="fc_kk">
                      <label class="custom-file-label" for="fc_kk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Jadwal Angguran Pokok</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="jadwal_angsuranpokok" name="jadwal_angsuranpokok">
                      <label class="custom-file-label" for="jadwal_angsuranpokok">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Pembayaran Premi</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_pembayaranpremi" name="fc_pembayaranpremi">
                      <label class="custom-file-label" for="fc_pembayaranpremi">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Sertifikat/polis asuransi</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="polis" name="polis">
                      <label class="custom-file-label" for="polis">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <?php } else if ($cek->asuransi == 'JASINDO' and $cek->kode_jenisklaim == 'MD001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Polis + Lampiran</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="polis" name="polis">
                      <label class="custom-file-label" for="polis">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Klaim + nilai nya</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="s_klaim" name="s_klaim">
                      <label class="custom-file-label" for="s_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Keterangan Meninggal Dunia</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="keterangan_meninggal" name="keterangan_meninggal">
                      <label class="custom-file-label" for="keterangan_meninggal">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Laporan Kerugian Diisi Bank</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="lap_kerugian" name="lap_kerugian">
                      <label class="custom-file-label" for="lap_kerugian">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Jadwal Angguran Pokok</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="jadwal_angsuranpokok" name="jadwal_angsuranpokok">
                      <label class="custom-file-label" for="jadwal_angsuranpokok">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'JIWASRAYA' and $cek->kode_jenisklaim == 'MD001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Keterangan Meninggal Dunia</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="keterangan_meninggal" name="keterangan_meninggal">
                      <label class="custom-file-label" for="keterangan_meninggal">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Jadwal Angguran Pokok</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="jadwal_angsuranpokok" name="jadwal_angsuranpokok">
                      <label class="custom-file-label" for="jadwal_angsuranpokok">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Keputusan Pensiunan (Untuk Pensiun)</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_pensiun" name="sk_pensiun">
                      <label class="custom-file-label" for="sk_pensiun">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'SLU' and $cek->kode_jenisklaim == 'MD001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form kematian</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_kematian" name="form_kematian">
                      <label class="custom-file-label" for="form_kematian">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Surat Keterangan Ahli Waris</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_ahliwaris" name="sk_ahliwaris">
                      <label class="custom-file-label" for="sk_ahliwaris">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Jadwal Angguran Pokok</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="jadwal_angsuranpokok" name="jadwal_angsuranpokok">
                      <label class="custom-file-label" for="jadwal_angsuranpokok">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'SLU' and $cek->kode_jenisklaim == 'PHK001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>SK PHK</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="sk_phk" name="sk_phk">
                      <label class="custom-file-label" for="sk_phk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Jadwal Angguran Pokok</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="jadwal_angsuranpokok" name="jadwal_angsuranpokok">
                      <label class="custom-file-label" for="jadwal_angsuranpokok">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim PHK</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaimphk" name="form_klaimphk">
                      <label class="custom-file-label" for="form_klaimphk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } else if ($cek->asuransi == 'TASPEN LIFE' and $cek->kode_jenisklaim == 'MD001' ) {  ?>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Form Klaim</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="form_klaim" name="form_klaim">
                      <label class="custom-file-label" for="form_klaim">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Keterangan Meninggal Dunia</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="keterangan_meninggal" name="keterangan_meninggal">
                      <label class="custom-file-label" for="keterangan_meninggal">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC KTP</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_ktp" name="fc_ktp">
                      <label class="custom-file-label" for="fc_ktp">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Kartu Keluarga</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_kk" name="fc_kk">
                      <label class="custom-file-label" for="fc_kk">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>FC Perjanjian Kredit</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="fc_perjanjiankredit" name="fc_perjanjiankredit">
                      <label class="custom-file-label" for="fc_perjanjiankredit">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label>Jadwal Angguran Pokok</label>
                          <div class="custom-file">
                      <input type="file" class="custom-file-input" id="jadwal_angsuranpokok" name="jadwal_angsuranpokok">
                      <label class="custom-file-label" for="jadwal_angsuranpokok">Choose file</label>
                  </div>
                  </div>
                  </div>
                </div>

              <?php } ?>


            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <button type="button" id="searchForm3" onclick="SubmitForm3();" class="waves-effect waves-light btn btn-info mb-7">Save</button>
                </div>
              </div>
            </div>
          </section>
          <!-- Step 4 -->
          <h6>Penitipan Klaim Ke Broker</h6>
          <section>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">Nilai Pengajuan Klaim :</label>
                  <input type="text" class="form-control" id="nilaipengajuanklaim2" name="nilaipengajuanklaim2" readonly="" placeholder="0"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">Nilai Persetujuan Klaim :</label>
                  <input type="text" class="form-control" id="nilaipersetujuanklaim" name="nilaipersetujuanklaim" readonly="" placeholder="0"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="firstName5">Nilai Klaim yang Dibayar :</label>
                  <div class="input-group mb-3">
                <input type="text" class="form-control" id="nilaiklaimdibayar" name="nilaiklaimdibayar" placeholder="0">
                  <div class="clearfix">
                    <button type="button" class="waves-effect waves-light btn mb-8 bg-gradient-danger">SAMAKAN</button>
                  </div>
                </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="firstName5">Tanggal Pembayaran Klaim :</label>
                  <input type="date" class="form-control" id="date1" name="tglpembayaran"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <button type="button" id="searchForm4" onclick="SubmitForm4();" class="waves-effect waves-light btn btn-info mb-7">Save</button>
                </div>
              </div>
            </div>
          </section>

          <h6>Pembayaran Klaim</h6>
          <section>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">Nilai Pengajuan Klaim :</label>
                  <input type="text" class="form-control" id="nilaipengajuanklaim3" name="nilaipengajuanklaim3" readonly="" placeholder="0"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">Nilai Persetujuan Klaim :</label>
                  <input type="text" class="form-control" id="nilaipersetujuanklaim1" name="nilaipersetujuanklaim1" readonly="" placeholder="0"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="firstName5">Nilai Klaim yang dibayar ke Broker :</label>
                  <input type="text" class="form-control" id="nilaiklaimdibayarbroker" name="nilaiklaimdibayarbroker" readonly="" placeholder="0"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="firstName5">Nilai Klaim yang Dibayar ke Bank :</label>
                  <div class="input-group mb-3">
                <input type="text" class="form-control" id="nilaiklaimdibayarbank" name="nilaiklaimdibayarbank" placeholder="0">
                  <div class="clearfix">
                    <button type="button" class="waves-effect waves-light btn mb-8 bg-gradient-danger">SAMAKAN</button>
                  </div>
                </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="firstName5">Tanggal Pembayaran Klaim :</label>
                  <input type="date" class="form-control" id="date1" name="tglpembayaran1"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <button type="button" id="searchForm5" onclick="SubmitForm5();" class="waves-effect waves-light btn btn-info mb-7">Save</button>
                </div>
              </div>
            </div>
          </section>
                        
                    </form>
                  </div>
              </div>
              <?php
                }
                ?>
              <!-- /.box -->
            </div>
            </div>      
        </section>
      </div>

<script>
      function SubmitForm() {
        let element = document.getElementById(noklaim);

        var noklaim = $("#noklaim").val();
        var tgllapor = $("#tgllapor").val();
        var noloan = $("#noloan").val();
        var nocif = $("#nocif").val();
        var namadebitur = $("#namadebitur").val();
        var tgllahir = $("#tgllahir").val();
        var nosertifikatpolis = $("#nosertifikatpolis").val();
        var cabangs = $("#cabangs").val();
        var asuransi = $("#asuransi").val();
        var produk = $("#produk").val();
        var tipemanfaat = $("#tipemanfaat").val();
        var jenisklaim = $("#jenisklaim").val();
        var tglkematian = $("#tglkematian").val();
        var nilaiplafon = $("#nilaiplafon").val();
        var nilaipengajuanklaim = $("#nilaipengajuanklaim").val();
        var nilaibungapengajuanklaim = $("#nilaibungapengajuanklaim").val();
        
        
        $.ajax({
            type      : 'POST',
            url       : '<?php echo base_url();?>' + 'editklaim/save', //Your form processing file URL
            data      : {
                          noklaim : noklaim,
                          tgllapor : tgllapor,
                          noloan : noloan,
                          nocif : nocif,
                          namadebitur : namadebitur,
                          tgllahir : tgllahir,
                          nosertifikatpolis : nosertifikatpolis,
                          cabangs : cabangs,
                          asuransi : asuransi,
                          produk : produk,
                          tipemanfaat : tipemanfaat,
                          jenisklaim : jenisklaim,
                          tglkematian : tglkematian,
                          nilaiplafon : nilaiplafon,
                          nilaipengajuanklaim : nilaipengajuanklaim,
                          nilaibungapengajuanklaim : nilaibungapengajuanklaim
                        }, 
            // dataType  : 'json',
            success   : function(data) {

                alert("Step 1 Submitted Sucessfully!");

             }
        });
      }

      function SubmitForm2() {

        var ekspedisi = $("#ekspedisi").val();
        var noresi = $("#noresi").val();
        var tglekspedisi = $("#tglekspedisi").val();
        var noklaim = $("#noklaim").val();
        var id_kla = $("#idk").val();
        var buktijasa = $("#buktijasa").val();
        var buktijasa_baru = buktijasa.replace("C:\\fakepath\\","");
        var formData = new FormData($("#adding")[0]);

        $.ajax({
            type      : 'POST',
            url       : '<?php echo base_url();?>' + 'editklaim/updatestepdua', //Your form processing file URL
            data:formData,
                  buktijasa_baru,
             processData:false,
             contentType:false,
             cache:false,
             async:false,
              success: function(data){
                  alert(buktijasa_baru);
                  console.log(buktijasa_baru);
                  alert("Step 2 Submitted Sucessfully!");
           }
            // data      : {
            //               noklaim : noklaim,
            //               ekspedisi : ekspedisi,
            //               noresi : noresi,
            //               tglekspedisi : tglekspedisi,
            //               buktijasa : buktijasa_baru,
            //             },
            // // dataType  : 'json',
            // success   : function(data) {
            //   alert(data);
            //   console.log(data);

            //     alert("Step 2 Submitted Sucessfully!");

                
            // }
        });
      }

    </script>

      <script>
    $(document).ready(function(){
        var base_url = '<?php echo base_url();?>';
    $('#asuransi').change(function(){
        var id = $('#asuransi').val();
        $.ajax({
        type      : "POST",
        url       : base_url + 'editklaim/get_jenisklaim',
        data      : {id : id},
        // async     : false,
        dataType  : 'json',
        success   : function(data) {
            // alert(data);
            var html = '';
            var i;
            for(i=0; i<data.length; i++){
                html += '<option value='+data[i].kode_jenisklaim+'>'+data[i].nama_jenisklaim+'</option>';
            }
            $("#jenisklaim").html(html);
    }
    });
    });
});

    function SubmitForm3()
      {
        $.ajax({
             url:'<?php echo base_url('editklaim/updatestep3/'.$idna);?>',
             type:"post",
             data:new FormData(this),
             processData:false,
             contentType:false,
             cache:false,
             async:false,
              success: function(data){
                  alert("Upload Image Berhasil.");
           }
         });
      }
</script>
<script src="<?=base_url()?>assets/main/js/jquery-1.10.0.min.js"></script>
<script>
  $(document).ready(function() {
      $('#asuransi').change(function(){
          if($(this).val() === 'AS001' || $(this).val() === 'AS002' || $(this).val() === 'BP001' || $(this).val() === 'JS002') {
              $('#nilaibungapengajuanklaim').prop('disabled', false);
          } else {
              $('#nilaibungapengajuanklaim').prop('disabled', true);
          }
      });
  });
</script>
 <div class="container-full">
		<!-- Main content -->
                <section class="content">
            <div class="row">
                <div class="col-12">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h4 class="box-title">Rekonsiliasi</h4>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <form class="form-horizontal form-bordered" method="post" action="<?=base_url('rekonsiliasi/search')?>">
                    <div class="row">
                         <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Cabang :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                          $sub = substr($as,4);
                                        $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BRK') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BRK') === false) {
                                            $sql = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and nama_cabang LIKE '%KC%'")->result_array();
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang">
                                            <option value=""></option>
                                         <?php
                                          foreach ($sql as $key => $value) {
                                         ?>
                                           <option value="<?=$value['id_cabang']?>"><?=$value['nama_cabang']?></option> 
                                         <?php
                                          } ?>
                                          </select>
                                          <?php
                                        }
                                        elseif(!empty($cabangs)){
                                            foreach ($cabangs as $key => $values) {
                                        ?>
                                        <select class="custom-select form-control" id="cabang" name="cabang">
                                            <option value=""></option>
                                           <option value="<?=$values['id_cabang']?>"><?=$values['nama_cabang']?></option> 
                                           </select>
                                        <?php
                                        }
                                         }else{
                                            $cab = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub'")->result_array();
                                            $sub1 = $cab[0]['id_induk'];
                                            $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub1'")->result_array();
                                          foreach ($sqlss as $key => $vass) {
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang" disabled="">
                                            <option value="<?=$vass['id_cabang']?>"><?=$vass['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Periode :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="location3" name="periodebulan">
                                        <option value=""></option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                    <div>
                                        <select class="custom-select form-control" id="location3" name="periodetahun">
                                        <option value=""></option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                        <option value="2026">2026</option>
                                        <option value="2027">2027</option>
                                        <option value="2028">2028</option>
                                        <option value="2029">2029</option>
                                        <option value="2030">2030</option>
                                    </select>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Capem / Kedai :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                        $sub = substr($as,4);
                                        $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BRK') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BRK') === false) {
                                         ?>
                                         <select class="custom-select form-control" id="capem" name="capem">
                                        </select>
                                         <?php
                                        } elseif(!empty($cabangs)){
                                        ?>
                                            <select class="custom-select form-control" id="capem" name="capem">
                                            </select>
                                        <?php
                                        } else{
                                            $sub = substr($as,4);
                                            $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub'")->result_array();
                                          foreach ($sqlss as $key => $va) {
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang" disabled="">
                                            <option value="<?=$va['id_cabang']?>"><?=$va['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Status Data :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="status" name="status">
                                        <option value=""></option>
                                        <option value="">Semua Data</option>
                                        <option value="">Sudah Rekonsiliasi</option>
                                        <option value="">Belum Rekonsiliasi</option>                                        
                                    </select>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="waves-effect waves-light btn btn-rounded btn-primary-light mb-5"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
                                </div>
                            </div>
                        </div>
                    </form>
                  </div>
              </div>
              <!-- /.box -->
            </div>
            </div>      
        </section>
                    <?php
                        if($this->session->flashdata('success')){
                            ?>
                            <div class="alert alert-success text-center">
                                <i class="glyphicon glyphicon-ok-sign"></i> <span><?=$this->session->flashdata('success')?></span>
                            </div>
                            <div></div>
                            <?php
                        }
                        ?>
                        <?php
                        if($this->session->flashdata('error')){
                            ?>
                            <div class="alert alert-danger text-center">
                                <i class="glyphicon glyphicon-remove-sign"></i> <span><?=$this->session->flashdata('error')?></span>
                            </div>
                            <div>
                            </div>
                        <?php   } ?>

		<section class="content">
			<div class="row">
			<div class="col-12">
			  <div class="box box-default">
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example1" class="table">
						<thead class="bg-dark">
							<tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Cabang</th>
                                <th class="text-center">No PK</th>
                                <th class="text-center">No Rekening</th>
                                <th class="text-center">Nama</th>
                                <th class="text-center">Amount</th>
                                <th class="text-center">Jatuh Tempo</th>
                                <th class="text-center">Rate (dari Bank)</th>
                                <th class="text-center">Rate Asuransi (Per Mil)</th>
                                <th class="text-center">Premi</th>
                                <th class="text-center">Fee Based Income (FBI)</th>
                                <th class="text-center">Jumlah Bayar</th>
                                <th class="text-center">Selisih Bayar</th>
                                <th class="text-center"><div style="width: max-content;">Tgl Bayar (Rekening Koran)</div></th>
                                <th class="text-center">Status Rekonsiliasi</th>
                                <th class="text-center">Action</th>
                            </tr>
						</thead>
						<tbody>
							<?php $no=1; foreach ($datana as $key => $value) { ?>
                                <tr id="<?php echo $value['id_data']; ?>">
                                <td class="text-center"><?=$no++?></td>
                                <td class="text-center">
                                    <div style="width: max-content;">
                                    <?php
                                $a = $value['cab'];
                                $cab = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang
                                where id_cabang = '$a'")->result_array();
                                foreach ($cab as $key => $cab1) {
                                    echo $cab1['nama_cabang'];
                                }
                                ?>
                                    </div>
                                </td>
                                <td class="text-center"><?=$value['pk']?></td>
                                <td class="text-center"><?=$value['norek']?></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['nama']?></div></td>
                                <td class="text-center"><?=number_format($value['amount'], 0);?></td>
                                <td class="text-center"><?=$value['tempo']?></td>
                                <td class="text-center"><?=$value['rate'];?></td>
                                        <td class="text-center"><?=$value['rate_asuransi'];?></td>
                                        <td class="text-center"><b><?php
                                        if ($value['amount'] != '' && $value['rate_asuransi'] != '') {
                                            $premi = ($value['amount'] * $value['rate_asuransi']) / 1000;
                                        }
                                            if (!empty($premi)) {
                                                echo $premi = number_format(floatval($premi), 0);
                                            } else{
                                                echo $premi = '0';
                                            }
                                            
                                        ?></b></td>
                                        <td class="text-center"><?php
                                            $fbi = str_replace(',', '', $premi) * 0.1;
                                                echo $fbis = number_format($fbi);
                                            
                                        ?></td>
                                        <td class="text-center"><b><?php 
                                        if($value['jumlah_bayar'] > 0){
                                        $bayar = str_replace(',', '', $value['jumlah_bayar']);
                                        echo number_format($bayar);
                                        } else{
                                            echo '-';
                                        }
                                        ?></b></td>
                                        <td class="text-center"><?php 
                                        $bayar = str_replace(',', '', $value['jumlah_bayar']);
                                        if($bayar > 0){
                                            $sel = str_replace(',', '', $premi) - $bayar;
                                        echo number_format($sel);
                                        } else{
                                            echo '-';
                                        }
                                        ?></td>
                                        <td class="text-center"><?php
                                        if (!empty($value['tglbayarrk'])) {
                                            echo date("d/m/Y", strtotime($value['tglbayarrk']));
                                        } else{
                                            echo '';
                                        }
                                        ?></td>
                                        <td class="text-center"><?php
                                    if ($value['status_rekon'] == '0'){
                                        ?>
                                    <span class="badge badge-pill badge-danger">
                                        <?php
                                        echo $value['status_rekon'] = 'No';
                                        } else if ($value['status_rekon'] == '1') {
                                        ?>
                                        <span class="badge badge-pill badge-success">
                                        <?php
                                        echo $value['status_rekon'] = 'Yes';
                                        }
                                        ?>
                                        </td>
                                        <td class="text-center">
                                <a title="Rekon"
                                    href="javascript:;"
                                    data-dokumentidaklengkap="<?php echo $value['id_data'];?>"
                                    data-toggle="modal" 
                                    data-target="#edit<?php echo $value['id_data']?>"
                                    >
                                    <?php
                                    if ($value['status_rekon'] == 'No') {
                                    ?>
                                    <button class="btn btn-sm btn-info">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </button> 
                                    <?php
                                    }
                                    ?>
                                </a>
                                    <?php
                                    if ($value['status_rekon'] == 'Yes') {
                                    ?>
                                        <form action="<?=base_url('historirekon/index/')?>" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" >
                                        <input type="text" class="form-control" id="id_data" name="id_data" readonly="" value="<?= $value['id_data'] ?>" style="display: none;">
                                        <button type="submit" class="waves-effect waves-light btn btn-rounded btn-primary-light mb-5"><i class="glyphicon glyphicon-plus"></i></button>
                                    </form>
                                    <?php
                                    }
                                    ?>
                            </td>
                            </tr>  
                            <?php } ?>
						</tbody>
						<tfoot>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->

      <?php foreach ($datana as $key => $val) { ?>

            <div id="edit<?php echo $val['id_data']?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <!-- <h3 class="modal-title">Edit Account</h3> -->
                        </div>
                        <div class="col-12 col-lg-12">
                        <div class="box">
                        <div class="box-body">
                        <form action="<?=base_url('rekonsiliasi/update/'.$val['id_data'])?>" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" >
                            <input type="text" class="form-control" id="id_data" name="id_data" readonly="" value="<?= $val['id_data'] ?>" style="display: none">
                            <input type="text" class="form-control" id="amount" name="amount" readonly="" value="<?= $val['amount'] ?>" style="display: none">
                            <input type="text" class="form-control" id="rate_asuransi" name="rate_asuransi" readonly="" value="<?= $val['rate_asuransi'] ?>" style="display: none">
                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. PK :</label>
                                    <input type="text" class="form-control" id="pk" name="pk" readonly="" value="<?= $val['pk'] ?>"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. Rekening :</label>
                                    <input type="text" class="form-control" id="norek" name="norek" readonly="" value="<?= $val['norek'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama :</label>
                                    <input type="text" class="form-control" id="nama" name="nama" readonly="" value="<?= $val['nama'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Total yang harus dibayar :</label>
                                    <input type="text" class="form-control" id="tempo" readonly="" name="tempo" value="<?php
                                    if ($val['amount'] != '' && $val['rate_asuransi'] != '') {
                                            $premi = ($val['amount'] * $val['rate_asuransi']) / 1000;
                                        }
                                            if (!empty($premi)) {
                                                echo $premi = number_format(floatval($premi), 0);
                                            } else{
                                                echo $premi = '0';
                                            }
                                     ?>"></div>
                            </div>
                        </div>
                        <p id="demo"></p>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">Jumlah Bayar :</label>
                                    <div class="input-group mb-3">
                                <input type="text" class="form-control" id="jumlahbayar" name="jumlahbayar" autocomplete="off" required="">
                                    <!-- <div class="clearfix">
                                        <button type="button" onclick="samakan()" class="waves-effect waves-light btn mb-8 bg-gradient-danger">SAMAKAN</button>
                                    </div> -->
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tanggal Bayar RK :</label>
                                    <input type="date" class="form-control" id="date1" name="tglbayarrk" required=""></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-sm btn-primary">Save changes</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            <?php } ?>
            </div>
            </div>      
        </section>
        <!-- /.content -->
      </div>

      <script src="<?=base_url()?>assets/main/js/jquery-1.10.0.min.js"></script>
                    <script>
                        $(document).ready(function(){
                            var base_url = '<?php echo base_url();?>';
                        $('#cabang').change(function(){
                            var id = $('#cabang').val();
                            $.ajax({
                            type      : "POST",
                            url       : base_url + 'rekonsiliasi/get_capem',
                            data      : {id : id},
                            // async     : false,
                            dataType  : 'json',
                            success   : function(data) {
                                // alert(data);
                                var html = '';
                                var i;
                                for(i=0; i<data.length; i++){
                                    html += '<option value='+data[i].id_cabang+'>'+data[i].nama_cabang+'</option>';
                                }
                                $("#capem").html(html);
                        }
                        });
                        });
                    });
                </script>
 <div class="container-full">
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-12">
					<div class="box bg-gradient-primary overflow-hidden pull-up">
						<div class="box-body pr-0 pl-lg-50 pl-15 py-0">							
							<div class="row align-items-center">
								<div class="col-12 col-lg-8">
									<h1 class="font-size-40 text-white">Welcome <?php echo $this->session->userdata('NamaUser');?> ..</h1>
									<p class="text-white mb-0 font-size-20">
										Work hard in silence, let your success be your noise..
									</p>
								</div>
								<div class="col-12 col-lg-4"><img src="https://eduadmin-template.multipurposethemes.com/bs4/images/svg-icon/color-svg/custom-15.svg" alt=""></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xl-3 col-md-6 col-12">
					<div class="box pull-up">
						<div class="box-body">	
							<div class="bg-primary rounded">
								<h5 class="text-white text-center p-10">Penutupan</h5>
							</div>
							<h1 class="countnm font-size-50 m-0 text-center">
								<?php
								$as = strtoupper($this->session->userdata('KodeUser'));
	                              $sub = substr($as,4);
	                            $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
	                                     where KodeUser = '$as' ")->result_array();
	                            if (strpos($as, 'BRK') !== false) {
	                            $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
	                            }
	                            if (strpos($as, 'BRK') === false) {
	                                $sql = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.DataPenutupan where 
										status = 1");
	                                echo $sql = count($sql->result());
	                            } elseif(!empty($cabangs)){
	                            	$caba = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.DataPenutupan a
										join PJM_SAUDARA.dbo.Cabang b on b.id_cabang = a.cab
										where 
										b.id_induk = '$sub'
										and a.status = 1");
	                            	echo $sql = count($caba->result());
	                            } else{
                                    $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.DataPenutupan where 
										status = 1
										and cab = '$sub'");
                                    echo $sql = count($sqlss->result());
	                            }
								?>
							</h1>
							<div class="text-center pull-up">
								<a href="<?php echo base_url('dokumen')?>" class="btn btn-block btn-primary-light">View all</a>
							</div>			
						</div>					
					</div>
				</div>
				<div class="col-xl-3 col-md-6 col-12">
					<div class="box pull-up">
						<div class="box-body">	
							<div class="bg-warning rounded">
								<h5 class="text-white text-center p-10">Pembatalan</h5>
							</div>
							<h1 class="countnm font-size-50 m-0 text-center">
								<?php
								$as = strtoupper($this->session->userdata('KodeUser'));
	                              $sub = substr($as,4);
	                            $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
	                                     where KodeUser = '$as' ")->result_array();
	                            if (strpos($as, 'BRK') !== false) {
	                            $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
	                            }
	                            if (strpos($as, 'BRK') === false) {
	                                $sql = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.DataPenutupan where 
										status = 0");
	                                echo $sql = count($sql->result());
	                            } elseif(!empty($cabangs)){
	                            	$caba = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.DataPenutupan a
										join PJM_SAUDARA.dbo.Cabang b on b.id_cabang = a.cab
										where 
										b.id_induk = '$sub'
										and a.status = 0");
	                            	echo $sql = count($caba->result());
	                            } else{
                                    $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.DataPenutupan where 
										status = 0
										and cab = '$sub'");
                                    echo $sql = count($sqlss->result());
	                            }
								?>
							</h1>
							<div class="text-center pull-up">
								<a href="<?php echo base_url('pembatalan')?>" class="btn btn-block btn-primary-light">View all</a>
							</div>										
						</div>					
					</div>
				</div>
				<div class="col-xl-3 col-md-6 col-12">
					<div class="box pull-up">
						<div class="box-body">	
							<div class="bg-danger rounded">
								<h5 class="text-white text-center p-10">Klaim</h5>
							</div>
							<h1 class="countnm font-size-50 m-0 text-center">
							<?php
								$as = strtoupper($this->session->userdata('KodeUser'));
	                              $sub = substr($as,4);
	                            $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
	                                     where KodeUser = '$as' ")->result_array();
	                            if (strpos($as, 'BRK') !== false) {
	                            $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
	                            }
	                            if (strpos($as, 'BRK') === false) {
	                                $sql = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Klaim");
	                                echo $sql = count($sql->result());
	                            } elseif(!empty($cabangs)){
	                            	$caba = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Klaim a
										join PJM_SAUDARA.dbo.Cabang b on b.id_cabang = a.cab
										where 
										b.id_induk = '$sub'");
	                            	echo $sql = count($caba->result());
	                            } else{
                                    $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Klaim where cab = '$sub'");
                                    echo $sql = count($sqlss->result());
	                            }
								?>
							</h1>
							<div class="text-center pull-up">
								<a href="<?php echo base_url('pengajuanklaim')?>" class="btn btn-block btn-primary-light">View all</a>
							</div>										
						</div>					
					</div>
				</div>
				<div class="col-xl-3 col-md-6 col-12">
					<div class="box pull-up">
						<div class="box-body">	
							<div class="bg-info rounded">
								<h5 class="text-white text-center p-10">Restitusi</h5>
							</div>
							<h1 class="countnm font-size-50 m-0 text-center">
								0
							</h1>
							<div class="text-center pull-up">
								<a href="<?php echo base_url('pengajuanrestitusi')?>" class="btn btn-block btn-primary-light">View all</a>
							</div>								
						</div>					
					</div>
				</div>
			</div>

		  <div class="row">
			<div class="col-12">
				<div class="box">
					<div class="box-header">
					<h4 class="box-title">Insurance Klaim Update</h4>
				</div>
				<div class="box-body">
					<div class="table-responsive">
					  <table class="table">
						<thead class="bg-dark">
							<tr>
								<th class="text-center">Asuransi</th>
                                <th class="text-center">Total Klaim</th>
                                <th class="text-center">Total Berkas</th>
                                <th class="text-center">Claim Paid</th>
                                <th class="text-center">Dispute</th>
                                <th class="text-center">Ditolak</th>
                                <th class="text-center">Proses Cabang</th>
                                <th class="text-center">Proses Diterima</th>
                                <th class="text-center">Proses Insurance Partner</th>
							</tr>
						</thead>
						<tbody>
							 <?php $no=1; 
							 $totalklaim = 0;
							 $totalberkas = 0;
							 $totalpaid = 0;
							 $totalproses = 0;
							 $totaldispute = 0;
							 $totalditolak = 0;
							 $totalditerima = 0;
							 $totalinsurance = 0;
							 foreach ($datana as $key => $value) { 
							 	
							 	$totalklaim += $value['total_outstandingklaim'];

							 	
							 	$totalberkas += $value['totalberkas'];
							 ?>
                            <tr>
                                <td class="text-center font-weight-bold"><?=$value['asuransi']?></td>
                                <td class="text-center"><?=number_format($value['total_outstandingklaim'],0)?></td>
                                <td class="text-center"><?=number_format($value['totalberkas'],0)?></td>
                                <td class="text-center">
                                	<table class="table table-borderless">
                                	<?php
                                	$claimpad = $status->status($value['asuransi'], 'CLAIM PAID');
                                	$totalpaid += !isset($claimpad->total_outstandingklaim) ? 0 : $claimpad->total_outstandingklaim;
                                	// var_dump($claimpad);
                                	echo '<tr><td>Nilai Klaim: ';
                                	echo !isset($claimpad->total_outstandingklaim) ? 0 : $claimpad->total_outstandingklaim;
                                	echo '<td/></tr>';
                                	echo '<tr><td>Jumlah NOC: ';
                                	echo !isset($claimpad->totalberkas) ? 0 : $claimpad->totalberkas;
                                	echo '<td/></tr>';
                                	echo '<tr><td>Persentase: ';
                                	if(!isset($claimpad->total_outstandingklaim)){
                                		echo '0%';
                                	}else{
                                	$total = ($claimpad->total_outstandingklaim / $value['total_outstandingklaim']) * 100;
                                	echo number_format($total, 2).'%';
                                	}
                                	echo '</td></tr>';
                                	?>
                               		 </table>
                                </td>

                                <td class="text-center">
                                	<table class="table table-borderless">
                                	<?php
                                	$claimpad = $status->status($value['asuransi'], 'DISPUTE');
                                	// var_dump($claimpad);
                                	echo '<tr><td>Nilai Klaim: ';
                                	echo !isset($claimpad->total_outstandingklaim) ? 0 : $claimpad->total_outstandingklaim;
                                	echo '<td/></tr>';
                                	echo '<tr><td>Jumlah NOC: ';
                                	echo !isset($claimpad->totalberkas) ? 0 : $claimpad->totalberkas;
                                	echo '<td/></tr>';
                                	echo '<tr><td>Persentase: ';
                                	if(!isset($claimpad->total_outstandingklaim)){
                                		echo '0%';
                                	}else{
                                	$total = ($claimpad->total_outstandingklaim / $value['total_outstandingklaim']) * 100;
                                	echo number_format($total, 2).'%';
                                	}
                                	echo '</td></tr>';
                                	?>
                               		 </table>
                                </td>

                                <td class="text-center">
                                	<table class="table table-borderless">
                                	<?php
                                	$claimpad = $status->status($value['asuransi'], 'DITOLAK');
                                	// var_dump($claimpad);
                                	echo '<tr><td>Nilai Klaim: ';
                                	echo !isset($claimpad->total_outstandingklaim) ? 0 : $claimpad->total_outstandingklaim;
                                	echo '<td/></tr>';
                                	echo '<tr><td>Jumlah NOC: ';
                                	echo !isset($claimpad->totalberkas) ? 0 : $claimpad->totalberkas;
                                	echo '<td/></tr>';
                                	echo '<tr><td>Persentase: ';
                                	if(!isset($claimpad->total_outstandingklaim)){
                                		echo '0%';
                                	}else{
                                	$total = ($claimpad->total_outstandingklaim / $value['total_outstandingklaim']) * 100;
                                	echo number_format($total, 2).'%';
                                	}
                                	echo '</td></tr>';
                                	?>
                               		 </table>
                                </td>

                                <td class="text-center">
                                	<table class="table table-borderless">
                                	<?php
                                	$claimpad = $status->status($value['asuransi'], 'PROSES CABANG');
                                	// var_dump($claimpad);
                                	echo '<tr><td>Nilai Klaim: ';
                                	echo !isset($claimpad->total_outstandingklaim) ? 0 : $claimpad->total_outstandingklaim;
                                	echo '<td/></tr>';
                                	echo '<tr><td>Jumlah NOC: ';
                                	echo !isset($claimpad->totalberkas) ? 0 : $claimpad->totalberkas;
                                	echo '<td/></tr>';
                                	echo '<tr><td>Persentase: ';
                                	if(!isset($claimpad->total_outstandingklaim)){
                                		echo '0%';
                                	}else{
                                	$total = ($claimpad->total_outstandingklaim / $value['total_outstandingklaim']) * 100;
                                	echo number_format($total, 2).'%';
                                	}
                                	echo '</td></tr>';
                                	?>
                               		 </table>
                                </td>

                                <td class="text-center">
                                	<table class="table table-borderless">
                                	<?php
                                	$claimpad = $status->status($value['asuransi'], 'PROSES DITERIMA');
                                	// var_dump($claimpad);
                                	echo '<tr><td>Nilai Klaim: ';
                                	echo !isset($claimpad->total_outstandingklaim) ? 0 : $claimpad->total_outstandingklaim;
                                	echo '<td/></tr>';
                                	echo '<tr><td>Jumlah NOC: ';
                                	echo !isset($claimpad->totalberkas) ? 0 : $claimpad->totalberkas;
                                	echo '<td/></tr>';
                                	echo '<tr><td>Persentase: ';
                                	if(!isset($claimpad->total_outstandingklaim)){
                                		echo '0%';
                                	}else{
                                	$total = ($claimpad->total_outstandingklaim / $value['total_outstandingklaim']) * 100;
                                	echo number_format($total, 2).'%';
                                	}
                                	echo '</td></tr>';
                                	?>
                               		 </table>
                                </td>

                                <td class="text-center">
                                	<table class="table table-borderless">
                                	<?php
                                	$claimpad = $status->status($value['asuransi'], 'PROSES INSURANCE PARTNER');
                                	// var_dump($claimpad);
                                	echo '<tr><td>Nilai Klaim: ';
                                	echo !isset($claimpad->total_outstandingklaim) ? 0 : $claimpad->total_outstandingklaim;
                                	echo '<td/></tr>';
                                	echo '<tr><td>Jumlah NOC: ';
                                	echo !isset($claimpad->totalberkas) ? 0 : $claimpad->totalberkas;
                                	echo '<td/></tr>';
                                	echo '<tr><td>Persentase: ';
                                	if(!isset($claimpad->total_outstandingklaim)){
                                		echo '0%';
                                	}else{
                                	$total = ($claimpad->total_outstandingklaim / $value['total_outstandingklaim']) * 100;
                                	echo number_format($total, 2).'%';
                                	}
                                	echo '</td></tr>';
                                	?>
                               		 </table>
                                </td>
                            </tr>  
                            <?php } ?>
						</tbody>
						<tfoot>
							<tr class="bg-dark">
								<td class='text-center font-weight-bold'>Total</td>
								<td class='text-center font-weight-bold'><?=number_format($totalklaim,0)?></td>
								<td class='text-center font-weight-bold'><?=number_format($totalberkas,0)?></td>
								<td class='text-center font-weight-bold'><?=number_format($totalpaid,0)?></td>
							</tr>
						</tfoot>
					  </table>
					</div>
				</div>
				</div>
			</div>
			<!-- /.col -->

		  </div>
		
		</section>
		<!-- /.content -->
	  </div>
 <div class="container-full">
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-12">
			  <div class="box box-default">
				<div class="box-header with-border">
				  <h4 class="box-title">Simulasi Biaya Nasabah</h4>
				</div>
				<!-- /.box-header -->

                <div class="box-body">
                    <form class="form-horizontal form-bordered" method="post" action="<?=base_url('simulasikalkulator/simulasi')?>">
                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Plafon :</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" id="plafon" name="plafon" autocomplete="off" required="">
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Plan Kredit :</label>
                                    <div class="input-group mb-3">
                                        <select class="custom-select form-control" id="plankredit" name="plankredit" required="">
                                            <option></option> 
                                        <?php
                                          $sqls = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.MasterPlan")->result_array();
                                          foreach ($sqls as $key => $values) {
                                         ?>
                                           <option value="<?=$values['kode_plan']?>"><?=$values['nama_plan']?></option> 
                                         <?php
                                          }
                                         ?>
                                    </select>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Pekerjaan :</label>
                                    <div class="input-group mb-3">
                                        <select class="custom-select form-control" id="pekerjaan" name="pekerjaan" required="">
                                            <option></option>
                                        <?php 
                                          $sqls = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.MasterDebitur")->result_array();
                                          foreach ($sqls as $key => $values) {
                                         ?>
                                           <option value="<?=$values['id_pekerjaan']?>"><?=$values['nama_pekerjaan']?></option> 
                                         <?php
                                          }
                                         ?>
                                    </select>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tipe Manfaat :</label>
                                    <div class="input-group mb-3">
                                        <select class="custom-select form-control" id="tipemanfaat" name="tipemanfaat" required="">
                                        </select>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tanggal Lahir :</label>
                                    <div class="input-group mb-3">
                                    <input type="date" class="form-control" id="tgllahir" name="tgllahir" required=""></div>
                                </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tanggal Akad :</label>
                                    <div class="input-group mb-3">
                                    <input type="date" class="form-control" id="tglakad" name="tglakad" required=""></div>
                                </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Jangka Waktu (Bulan) :</label>
                                    <div class="input-group mb-3">
                                    <input type="text" class="form-control" id="jangkawaktu" name="jangkawaktu" autocomplete="off"></div>
                                </div>
                            </div>

                        </div>
                        <!-- <div class="row"> -->
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" name="submit" class="waves-effect waves-light btn mb-5 bg-gradient-info mb-5">Submit</button>
                                </div>
                            </div>

                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Plafon :</label>
                                    <div class="input-group mb-3">
                                        <?php
                                        if(isset($_POST['submit'])) {
                                                $premis = $datas['premi'];
                                                $plafons = $datas['plafon'];
                                                $rates = $datas['rate'];
                                        }
                                        ?>
                                        <input type="text" class="form-control" id="plafonresult" name="plafonresult" readonly="" value="<?php
                                        if(isset($_POST['submit'])) {
                                            if ($datas != null)
                                                echo $plafons;
                                            } else{
                                            }
                                        ?>">
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Rate :</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" id="rate" name="rate" readonly="" value="<?php
                                        if(isset($_POST['submit'])) {
                                            if ($datas != null)
                                                echo $rates;
                                            } else{
                                            }
                                        ?>">
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName5">Premi :</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" id="premi" name="premi" readonly="" value="<?php
                                        if(isset($_POST['submit'])) {
                                            if ($datas != null)
                                                echo "Rp. ".$premis;
                                            } else{
                                            }
                                        ?>">
                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- </div> -->
                    </form>
                  </div>

			  </div>
			  <!-- /.box -->
			</div>
			</div>		
		</section>
		<!-- /.content -->
</div>

<script src="<?=base_url()?>assets/main/js/jquery-1.10.0.min.js"></script>
                    <script>
                        $(document).ready(function(){
                            var base_url = '<?php echo base_url();?>';
                        $('#pekerjaan').change(function(){
                            var id = $('#pekerjaan').val();
                            $.ajax({
                            type      : "POST",
                            url       : base_url + 'simulasikalkulator/get_tipe',
                            data      : {id : id},
                            // async     : false,
                            dataType  : 'json',
                            success   : function(data) {
                                // alert(data);
                                var html = '';
                                var i;
                                for(i=0; i<data.length; i++){
                                    html += '<option value='+data[i].id_type+'>'+data[i].nama_type+'</option>';
                                }
                                $("#tipemanfaat").html(html);
                        }
                        });
                        });
                    });
                </script>
                <script type="text/javascript">
                    var plafon = document.getElementById('plafon');
                    plafon.addEventListener('keyup', function(e)
                    {
                        plafon.value = formatRupiah(this.value, 'Rp. ');
                    });
                    
                    /* Fungsi */
                    function formatRupiah(angka, prefix)
                    {
                        var number_string = angka.replace(/[^,\d]/g, '').toString(),
                            split    = number_string.split(','),
                            sisa     = split[0].length % 3,
                            rupiah     = split[0].substr(0, sisa),
                            ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
                            
                        if (ribuan) {
                            separator = sisa ? '.' : '';
                            rupiah += separator + ribuan.join('.');
                        }
                        
                        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
                    }
             </script>
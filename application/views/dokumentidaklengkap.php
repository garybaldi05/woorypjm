 <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h4 class="box-title">Dokumen Tidak Lengkap</h4>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <form class="form-horizontal form-bordered" method="post" action="<?=base_url('dokumentidaklengkap/search')?>">
                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Cabang :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                          $sub = substr($as,4);
                                        $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BRK') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BRK') === false) {
                                            $sql = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and nama_cabang LIKE '%KC%'")->result_array();
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang">
                                            <option value=""></option>
                                         <?php
                                          foreach ($sql as $key => $value) {
                                         ?>
                                           <option value="<?=$value['id_cabang']?>"><?=$value['nama_cabang']?></option> 
                                         <?php
                                          } ?>
                                          </select>
                                          <?php
                                        }
                                        elseif(!empty($cabangs)){
                                            foreach ($cabangs as $key => $values) {
                                        ?>
                                        <select class="custom-select form-control" id="cabang" name="cabang">
                                            <option value=""></option>
                                           <option value="<?=$values['id_cabang']?>"><?=$values['nama_cabang']?></option> 
                                           </select>
                                        <?php
                                        }
                                         }else{
                                            $cab = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub'")->result_array();
                                            $sub1 = $cab[0]['id_induk'];
                                            $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub1'")->result_array();
                                          foreach ($sqlss as $key => $vass) {
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang" disabled="">
                                            <option value="<?=$vass['id_cabang']?>"><?=$vass['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Periode :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="location3" name="periodebulan">
                                        <option value=""></option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                    <div>
                                        <select class="custom-select form-control" id="location3" name="periodetahun">
                                        <option value=""></option>
                                       <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                        <option value="2026">2026</option>
                                        <option value="2027">2027</option>
                                        <option value="2028">2028</option>
                                        <option value="2029">2029</option>
                                        <option value="2030">2030</option>
                                    </select>
                                    </div>
                                </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Capem / Kedai :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                        $sub = substr($as,4);
                                        $user = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BRK') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BRK') === false) {
                                         ?>
                                         <select class="custom-select form-control" id="capem" name="capem">
                                        </select>
                                         <?php
                                        } elseif(!empty($cabangs)){
                                        ?>
                                            <select class="custom-select form-control" id="capem" name="capem">
                                            </select>
                                        <?php
                                        } else{
                                            $sub = substr($as,4);
                                            $sqlss = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$sub'")->result_array();
                                          foreach ($sqlss as $key => $va) {
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang" disabled="">
                                            <option value="<?=$va['id_cabang']?>"><?=$va['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="waves-effect waves-light btn btn-rounded btn-primary-light mb-5"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
                                </div>
                            </div>
                        </div>
                    </form>
                  </div>
              </div>
              <!-- /.box -->
            </div>
            </div>      
        </section>
        <!-- /.content -->
                        <?php
                        if($this->session->flashdata('success')){
                            ?>
                            <div class="alert alert-success text-center">
                                <i class="glyphicon glyphicon-ok-sign"></i> <span><?=$this->session->flashdata('success')?></span>
                            </div>
                            <div></div>
                            <?php
                        }
                        ?>
                        <?php
                        if($this->session->flashdata('error')){
                            ?>
                            <div class="alert alert-danger text-center">
                                <i class="glyphicon glyphicon-remove-sign"></i> <span><?=$this->session->flashdata('error')?></span>
                            </div>
                            <div>
                            </div>
                        <?php   } ?>

                <section class="content">
            <div class="row">
                <div class="col-12">
              <div class="box box-default">
                <div class="box-body">
                    <div class="table-responsive">
                      <table id="example1" class="table">
                        <thead class="bg-dark">
                            <tr>
                                <th class="text-center">Action</th>
                                <th class="text-center">No</th>
                                <th class="text-center">Cabang</th>
                                <th class="text-center">No PK</th>
                                <th class="text-center">No Rekening</th>
                                <th class="text-center">Nama</th>
                                <th class="text-center">Tanggal Lahir</th>
                                <th class="text-center">Tanggal Pembukaan</th>
                                <th class="text-center">Jatuh Tempo</th>
                                <th class="text-center">Plan Kredit</th>
                                <th class="text-center">Pekerjaan</th>
                                <th class="text-center">Amount</th>
                                <th class="text-center">No KTP</th>
                                <th class="text-center">Rate (dari Bank)</th>
                                <th class="text-center">Jenis Kelamin</th>
                                <th class="text-center">Rate Asuransi (Per Mil)</th>
                                <th class="text-center">Premi</th>
                                <th class="text-center">Fee Based Income (FBI)</th>
                                <th class="text-center">Asuransi</th>
                                <th class="text-center">Tipe Manfaat</th>
                                <th class="text-center">No NPWP</th>
                                <th class="text-center">No PK Lama</th>
                                <th class="text-center">Status Rekonsiliasi</th>
                                <th class="text-center">Status Medical</th>
                                <th class="text-center">Status Klaim</th>
                                <th class="text-center">Status Restitusi</th>
                                <th class="text-center">Tanggal Produksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($datana as $key => $value) { ?>
                                <tr id="<?php echo $value['id_data']; ?>">
                                     <td class="text-center">
                                <a title="Change"
                                    href="javascript:;"
                                    data-dokumentidaklengkap="<?php echo $value['id_data'];?>"
                                    data-toggle="modal" 
                                    data-target="#edit<?php echo $value['id_data']?>"
                                    >
                                    <button class="btn btn-sm btn-info">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </button> 
                                </a>
                            </td>
                                <td class="text-center"><?=$no++?></td>
                                <td class="text-center">
                                    <div style="width: max-content;">
                                    <?php
                                $a = $value['cab'];
                                $cab = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang
                                where id_cabang = '$a'")->result_array();
                                foreach ($cab as $key => $cab1) {
                                    echo $cab1['nama_cabang'];
                                }
                                ?>
                                    </div>
                                </td>
                                <td class="text-center"><?=$value['pk']?></td>
                                <td class="text-center"><?=$value['norek']?></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['nama']?></div></td>
                                <td class="text-center"><?=date("d/m/Y", strtotime($value['lahir']));?></td>
                                <td class="text-center"><?=date("d/m/Y", strtotime($value['buka']));?></td>
                                <td class="text-center"><?=$value['tempo']?></td>
                                <td class="text-center">
                                    <div style="width: max-content;">
                                    <?php
                                    $b = $value['plankredit'];
                                    $plan = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.MasterPlan
                                    where kode_plan = '$b'")->result_array();
                                    foreach ($plan as $key => $plan1) {
                                        echo $plan1['nama_plan'];
                                    }
                                ?>
                                </div>
                                </td>
                                <td class="text-center">
                                    <div style="width: max-content;">
                                    <?php
                                    $id = $value['id'];
                                    $idkerja = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.MasterDebitur
                                    where id_pekerjaan = '$id'")->result_array();
                                    foreach ($idkerja as $key => $idkerja1) {
                                        echo $idkerja1['nama_pekerjaan'];
                                    }
                                ?>
                                </div>
                                </td>
                                <td class="text-center"><?=number_format($value['amount'], 0);?></td>
                                <td class="text-center"><?=$value['ktp']?></td>
                                <td class="text-center"><?=$value['rate'];?></td>
                                <td class="text-center"><?php
                                    if ($value['sex'] == 'P'){
                                        ?>
                                    <span class="badge badge-pill badge-success">
                                        <?php
                                        echo $value['sex'] = 'Perempuan';
                                        } else if ($value['sex'] == 'L') {
                                        ?>
                                        <span class="badge badge-pill badge-danger">
                                        <?php
                                        echo $value['sex'] = 'Laki-Laki';
                                        } else{
                                            echo '';
                                        }
                                        ?></td>
                                        <td class="text-center"><?=$value['rate_asuransi'];?></td>
                                        <td class="text-center"><?php
                                        if ($value['amount'] != '' && $value['rate_asuransi'] != '') {
                                            $premi = ($value['amount'] * $value['rate_asuransi']) / 1000;
                                        }
                                            if (!empty($premi)) {
                                                echo $premi = number_format(floatval($premi), 0);
                                            } else{
                                                echo $premi = '0';
                                            }
                                            
                                        ?></td>
                                        <td class="text-center"><?php
                                            $fbi = str_replace(',', '', $premi) * 0.1;
                                                echo $fbis = number_format($fbi);
                                            
                                        ?></td>
                                <td class="text-center"><div style="width: max-content;">
                                    <?php
                                $ass = $value['asuransi'];
                                $asuransi = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Asuransi
                                where id_asuransi = '$ass'")->result_array();
                                foreach ($asuransi as $key => $asu) {
                                    echo $asu['nama_asuransi'];
                                }
                                ?></div>
                                </td>
                                <td class="text-center"><?php
                                $typ = $value['id_type'];
                                $typemanfaat = $this->db->query("SELECT nama_type FROM PJM_SAUDARA.dbo.TypeManfaat
                                where id_type = '$typ'")->result_array();                                
                                foreach ($typemanfaat as $key => $type1) {
                                    echo $type1['nama_type'];
                                }
                                ?></td>
                                <td class="text-center"><?=$value['npwp']?></td>
                                <td class="text-center"><?=$value['old_pk'];?></td>
                                <td class="text-center"><?php
                                if ($value['status_rekon'] == '0'){
                                        ?>
                                    <span class="badge badge-pill badge-danger">
                                        <?php
                                        echo $value['status_rekon'] = 'No';
                                        } else if ($value['status_rekon'] == '1') {
                                        ?>
                                        <span class="badge badge-pill badge-success">
                                        <?php
                                        echo $value['status_rekon'] = 'Yes';
                                        }
                                        ?>
                                        </td>
                                <td class="text-center"><?=$value['status_medical']?></td>
                                <td class="text-center"><?php
                                if ($value['status_klaim'] == '0'){
                                        ?>
                                    <span class="badge badge-pill badge-warning">
                                        <?php
                                        echo $value['status_klaim'] = 'No';
                                        } else if ($value['status_klaim'] == '1') {
                                        ?>
                                        <span class="badge badge-pill badge-danger">
                                        <?php
                                        echo $value['status_klaim'] = 'Ditolak';
                                        } else if ($value['status_klaim'] == '2'){
                                        ?>
                                        <span class="badge badge-pill badge-success">
                                        <?php
                                            echo $value['status_klaim'] = 'Yes';
                                        }
                                        ?></td>
                                <td class="text-center"><?php
                                if ($value['status_restitusi'] == '0'){
                                        ?>
                                    <span class="badge badge-pill badge-warning">
                                        <?php
                                        echo $value['status_restitusi'] = 'No';
                                        } else if ($value['status_restitusi'] == '1') {
                                        ?>
                                        <span class="badge badge-pill badge-danger">
                                        <?php
                                        echo $value['status_restitusi'] = 'Ditolak';
                                        } else if($value['status_restitusi'] == '2'){
                                        ?>
                                        <span class="badge badge-pill badge-success">
                                        <?php
                                            echo $value['status_restitusi'] = 'Yes';
                                        }
                                        ?></td>
                                        <td class="text-center"><div style="width: max-content;"><?=date("d/m/Y H:i:s", strtotime($value['date_created']));?></div></td>
                            </tr>  
                            <?php } ?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->

             <?php foreach ($datana as $key => $val) { ?>

            <div id="edit<?php echo $val['id_data']?>" class="modal fade " tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <!-- <h3 class="modal-title">Edit Account</h3> -->
                        </div>
                        <div class="col-12 col-lg-12">
                        <div class="box">
                        <div class="box-body">
                        <form action="<?=base_url('dokumentidaklengkap/update/'.$val['id_data'])?>" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" >
                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Cabang :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="cab" name="cab">
                                        <?php
                                        $a = $val['cab'];
                                        $sqll = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang where id_cabang = '$a'")->result_array();
                                        $b = $sqll[0]['nama_cabang'];
                                        ?>
                                        <option value="<?php echo $val['cab'];?>"><?php echo $b?></option>
                                        <?php 
                                          $sqls = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Cabang")->result_array();
                                          foreach ($sqls as $key => $values) {
                                         ?>
                                           <option value="<?=$values['id_cabang']?>"><?=$values['nama_cabang']?></option> 
                                         <?php
                                          }
                                         ?>
                                    </select>
                                </div>
                                </div>
                            </div>
                        </div>
                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. PK :</label>
                                    <input type="text" class="form-control" id="pk" name="pk" value="<?= $val['pk'] ?>"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. Rekening :</label>
                                    <input type="text" class="form-control" id="norek" name="norek" readonly="" value="<?= $val['norek'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama :</label>
                                    <input type="text" class="form-control" id="nama" name="nama" value="<?= $val['nama'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tanggal Lahir :</label>
                                    <input class="form-control" type="date" value="<?=date("Y-m-d", strtotime($val['lahir']));?>" id="lahir" name="lahir">
                            </div>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tanggal Pembukaan :</label>
                                    <input class="form-control" type="date" value="<?=date("Y-m-d", strtotime($val['buka']));?>" id="buka" name="buka">
                            </div>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Jatuh Tempo (dalam bulan) :</label>
                                    <input type="text" class="form-control" id="tempo" name="tempo" value="<?= $val['tempo'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Plan Kredit :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="plankredit" name="plankredit">
                                        <?php
                                        $a = $val['plankredit'];
                                        $sqll = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.MasterPlan where kode_plan = '$a'")->result_array();
                                        $b = $sqll[0]['nama_plan'];
                                        ?>
                                        <option value="<?php echo $val['plankredit'];?>"><?php echo $b?></option>
                                        <?php 
                                          $sqls = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.MasterPlan")->result_array();
                                          foreach ($sqls as $key => $values) {
                                         ?>
                                           <option value="<?=$values['kode_plan']?>"><?=$values['nama_plan']?></option> 
                                         <?php
                                          }
                                         ?>
                                    </select>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pekerjaan :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="id" name="id">
                                        <?php
                                        $a = $val['id'];
                                        $sqll = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.MasterDebitur where id_pekerjaan = '$a'")->result_array();
                                        $b = $sqll[0]['nama_pekerjaan'];
                                        ?>
                                        <option value="<?php echo $val['id'];?>"><?php echo $b?></option>
                                        <?php 
                                          $sqls = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.MasterDebitur")->result_array();
                                          foreach ($sqls as $key => $values) {
                                         ?>
                                           <option value="<?=$values['id_pekerjaan']?>"><?=$values['nama_pekerjaan']?></option> 
                                         <?php
                                          }
                                         ?>
                                    </select>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Amount :</label>
                                    <input type="text" class="form-control" id="amount" name="amount" value="<?= number_format($val['amount'], 0); ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. KTP :</label>
                                    <input type="text" class="form-control" id="ktp" name="ktp" value="<?= $val['ktp'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Rate (dari Bank):</label>
                                    <input type="text" class="form-control" id="rate" name="rate" value="<?= $val['rate'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Jenis Kelamin :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="sex" name="sex">
                                        <?php
                                        if ($val['sex'] == 'P'){
                                        ?>
                                        <?php
                                        echo $ab = $val['sex'] = 'Perempuan';
                                        } else if ($val['sex'] == 'L') {
                                        ?>
                                        <?php
                                        echo $ab = $val['sex'] = 'Laki-Laki';
                                        } else{
                                            echo $ab = '';
                                        }
                                        ?>
                                        <option value="<?php echo $val['sex'];?>"><?php echo $ab;?></option>
                                        <?php
                                            if ($val['sex'] == 'Perempuan') {
                                        ?>
                                           <option value="Laki-Laki">Laki-Laki</option>
                                       <?php
                                        } elseif ($val['sex'] == 'Laki-Laki') {
                                       ?>
                                       <option value="Perempuan">Perempuan</option>
                                       <?php
                                        } else{
                                       ?>
                                        <option value="Laki-Laki">Laki-Laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Rate Asuransi (Per Mil):</label>
                                    <input type="text" class="form-control" id="rate" name="rate" value="<?= $val['rate_asuransi'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Asuransi :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="asuransi" name="asuransi">
                                        <?php
                                        $a = $val['asuransi'];
                                        $sqll = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Asuransi where id_asuransi = '$a'")->result_array();
                                        $b = $sqll[0]['nama_asuransi'];
                                        ?>
                                        <option value="<?php echo $val['asuransi'];?>"><?php echo $b?></option>
                                        <?php 
                                          $sqls = $this->db->query("SELECT * FROM PJM_SAUDARA.dbo.Asuransi")->result_array();
                                          foreach ($sqls as $key => $values) {
                                         ?>
                                           <option value="<?=$values['id_asuransi']?>"><?=$values['nama_asuransi']?></option> 
                                         <?php
                                          }
                                         ?>
                                    </select>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. NPWP :</label>
                                    <input type="text" class="form-control" id="npwp" name="npwp" value="<?= $val['npwp'] ?>"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. PK Lama :</label>
                                    <input type="text" class="form-control" id="old_pk" name="old_pk" value="<?= $val['old_pk'] ?>"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-sm btn-primary">Save changes</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

            <?php } ?>
            </div>
            </div>      
        </section>

      </div>
      <script src="<?=base_url()?>assets/main/js/jquery-1.10.0.min.js"></script>
                    <script>
                        $(document).ready(function(){
                            var base_url = '<?php echo base_url();?>';
                        $('#cabang').change(function(){
                            var id = $('#cabang').val();
                            $.ajax({
                            type      : "POST",
                            url       : base_url + 'dokumentidaklengkap/get_capem',
                            data      : {id : id},
                            // async     : false,
                            dataType  : 'json',
                            success   : function(data) {
                                // alert(data);
                                var html = '';
                                var i;
                                for(i=0; i<data.length; i++){
                                    html += '<option value='+data[i].id_cabang+'>'+data[i].nama_cabang+'</option>';
                                }
                                $("#capem").html(html);
                        }
                        });
                        });
                    });
                </script>
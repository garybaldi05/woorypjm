<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuanklaim extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->library('Excel');
        $this->load->model('Pengajuanklaim_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index($num='')
    {

        $data['title']      = 'Pengajuan Klaim';
        $data['sub_menu']   = 22;
        $data['page_id']    = 10;

        // $data['datana']         = $this->Pengajuanklaim_m->dokumen();

        $data['status'] = 1;

        if (empty($this->uri->segment(3))) {
          $perpage = 0;
        } else{
        $perpage = $this->uri->segment(3);
        }
          $offset = 10;
         $data['datana'] = $this->Pengajuanklaim_m->getDataPagination($perpage, $offset);

         $config['base_url'] = site_url('pengajuanklaim/index');
         $config['total_rows'] = $this->Pengajuanklaim_m->dokumen()->num_rows();
         $config['per_page'] = $offset;

         $config['next_link'] = 'Next';
          $config['prev_link'] = 'Previous';
          $config['first_link'] = 'First';
          $config['last_link'] = 'Last';
          $config['full_tag_open'] = '<ul class="pagination">';
          $config['full_tag_close'] = '</ul>';
          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
          $config['cur_tag_open'] = '<li class="active"><a href="#">';
          $config['cur_tag_close'] = '</a></li>';
          $config['prev_tag_open'] = '<li>';
          $config['prev_tag_close'] = '</li>';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';

         $this->pagination->initialize($config);


         $this->template->load('template','pengajuanklaim', $data);
    }

    function search()
    {

        extract($_POST);

        // var_dump($_POST);
        // die();

        $data['title']      = 'Pengajuan Klaim';
        $data['sub_menu']   = 22;
        $data['page_id']    = 10;

        $data['search']         = $this->Pengajuanklaim_m->search();

        $data['status'] = 1;

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun) || !empty($statusklaims)){
            $data['datana']         = $this->Pengajuanklaim_m->search();
        } else{
            $data['datana']         = $this->Pengajuanklaim_m->dokumen();
        }
        $this->template->load('template','pengajuanklaim',$data);
    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Pengajuanklaim_m->get_capem($id);
        echo json_encode($data);
    }

    function get_jenisklaim()
    {
        $id=$this->input->post('id');
        $data=$this->Pengajuanklaim_m->get_jenisklaim($id);
        echo json_encode($data);
    }

public function excel()
{

$file = $_FILES['file']['name'];
$fileName = str_replace(' ', '_', $file);
  
$config['upload_path'] = './upload/dokklaim/';
$config['file_name'] = $fileName;
$config['allowed_types'] = 'xls|xlsx|csv';
$config['max_size'] = 10000;

$this->load->library('upload');
$this->upload->initialize($config);
  
if(! $this->upload->do_upload('file') ){
   $this->session->set_flashdata('error', 'Please check your file (.xls / .xlsx)');
    redirect('pengajuanklaim');
}
      
        $inputFileName = './upload/dokklaim/'.$fileName;
        try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch(Exception $e) {
    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
}

//  Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(); 
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();

//  Loop through each row of the worksheet in turn
for ($rows = 2; $rows <= $highestRow; $rows++){
    //  Read a row of data into an array
    $rowData = $sheet->toArray(NULL,
                                    NULL,
                                    TRUE,
                                    TRUE);
    //  Insert row data array into your database of choice here
}

        $data = array();

        $numrow = 1;
        foreach($rowData as $row){
          
          if($numrow > 1){
            array_push($data, array(
              'cif'=>$row['B'],
              'kodecabang'=>$row['C'],
              'wilayah'=>$row['D'],
              'namacabang'=>$row['E'],
              'ktp'=>$row['F'],
              'nama'=>$row['G'],
              'tgllahir'=>$row['H'],
              'loan'=>$row['I'],
              'fasilitas'=>$row['J'],
              'jkmulai'=>$row['K'],
              'jkakhir'=>$row['L'],
              'tenor'=>$row['M'],
              'plafond'=>$row['N'],
              'premi'=>$row['O'],
              'polis'=>$row['P'],
              'asuransi'=>$row['Q'],
              'porsi'=>$row['R'],
              'tagihan'=>$row['S'],
              'jenisklaim'=>$row['T'],
              'tglkematian'=>$row['U'],
              'tgllapor'=>$row['V'],
              'tglsubmit'=>$row['W'],
              'tglmodify'=>$row['X'],
              'modifyby'=>$row['Y'],
              'outstandingpokok'=>$row['Z'],
              'outstandingbunga'=>$row['AA'],
              'outstandingtotalklaim'=>$row['AB'],
              'statusklaim'=>$row['AC'],
              'tglkirim'=>$row['AD'],
              'jasaekspedisi'=>$row['AE'],
              'noresi'=>$row['AF'],
              'remarkcabang'=>$row['AG'],
              'remarkasuransi'=>$row['AH'],
            ));
          }
          
          $numrow++; 
        }


        $i = 0;

        foreach($data as $key=>$val)
        {
                $cif = $data[$key]['cif'];
                $kodecabang = $data[$key]['kodecabang'];
                $wilayah = $data[$key]['wilayah'];
                $namacabang = $data[$key]['namacabang'];
                $ktp = $data[$key]['ktp'];
                $nama = $data[$key]['nama'];
                $tgllahir = $data[$key]['tgllahir'];
                $loan = $data[$key]['loan'];
                $fasilitas = $data[$key]['fasilitas'];
                $jkmulai = $data[$key]['jkmulai'];
                $jkakhir = $data[$key]['jkakhir'];
                $tenor = $data[$key]['tenor'];
                $plafond = $data[$key]['plafond'];
                $premi = $data[$key]['premi'];
                $polis = $data[$key]['polis'];
                $asuransi = $data[$key]['asuransi'];
                $tagihan = $data[$key]['tagihan'];
                $porsi = $data[$key]['porsi'];
                $jenisklaim = $data[$key]['jenisklaim'];
                $tglkematian = $data[$key]['tglkematian'];
                $tgllapor = $data[$key]['tgllapor'];
                $tglsubmit = $data[$key]['tglsubmit'];
                $tglmodify = $data[$key]['tglmodify'];
                $modifyby = $data[$key]['modifyby'];
                $outstandingpokok = $data[$key]['outstandingpokok'];
                $outstandingbunga = $data[$key]['outstandingbunga'];
                $outstandingtotalklaim = $data[$key]['outstandingtotalklaim'];
                $statusklaim = $data[$key]['statusklaim'];
                $tglkirim = $data[$key]['tglkirim'];
                $jasaekspedisi = $data[$key]['jasaekspedisi'];
                $noresi = $data[$key]['noresi'];
                $remarkcabang = $data[$key]['remarkcabang'];
                $remarkasuransi = $data[$key]['remarkasuransi'];

                var_dump($outstandingtotalklaim);
                die();


                $cifloan = $this->db->query("select * from PJM_SAUDARA.dbo.Klaim where cif = '$cif' and loan = '$loan'")->result_array();


                 $kodek = $this->db->query("select kodeklaim FROM PJM_SAUDARA.dbo.Klaim order by kodeklaim DESC");
              if ($kodek->num_rows() <> 0) {
                        $datas = $kodek->row();
                        $kode = intval($datas->kodeklaim) + 1;

                    } else {
                        $kode = 1;
                    }

                    $kodemax = str_pad($kode++, 4, "0", STR_PAD_LEFT);

              if ($kodek->num_rows() <> 0) {
                    for($x = 1; $x < count($data); $x++) {
                        $jour = substr($datas->kodeklaim,0,6);
                        $head = $this->db->query("select top 1 kodeklaim, RIGHT(kodeklaim,4) as voucher from PJM_SAUDARA.dbo.Klaim where LEFT(kodeklaim, 6) = '$jour' ORDER BY kodeklaim desc");
                      $da = $head->row();
                      $kodes = intval($da->voucher) + 1;
                      $kodemaxs = str_pad($kodes++, 4, "0", STR_PAD_LEFT);
                      $kodekla = substr($datas->kodeklaim,0,6).$kodemaxs;
                    }
              } else{
                  $kodekla = date('Ym').$kodemax;
              }

              if ($jenisklaim == 'MENINGGAL DUNIA') {
                $kode_jenisklaim = 'MD001';
              } elseif ($jenisklaim == 'PHK') {
                $kode_jenisklaim = 'PHK001';
              } elseif ($jenisklaim == 'KREDIT MACET') {
                $kode_jenisklaim = 'KM001';
              }
               
              $masa = 'PRA BROKER';
              $status_step = '3';

              if ($cif != '' && $loan != '') {
                $cifloan = $this->db->query("select * from PJM_SAUDARA.dbo.Klaim where cif = '$cif' and loan = '$loan'")->result_array();
                $insert = $this->db->query("insert into PJM_SAUDARA.dbo.Klaim (
                kodeklaim,
                cif,
                kodecabang,
                wilayah,
                namacabang,
                ktp,
                nama,
                tgllahir,
                loan,
                fasilitas,
                jkmulai,
                jkakhir,
                tenor,
                plafond,
                premi,
                polis,
                asuransi,
                tagihan,
                porsi,
                kode_jenisklaim,
                tglkematian,
                tgllapor,
                tglsubmit,
                tglmodify,
                modifyby,
                outstandingpokok,
                outstandingbunga,
                outstandingtotalklaim,
                statusklaim,
                tglkirim,
                jasaekspedisi,
                noresi,
                remarkcabang,
                remarkasuransi,
                masa_asuransi,
                status_step
                )
                values (
                '".$kodekla."',
                '".$cif."',
                '".$kodecabang."',
                '".$wilayah."',
                '".$namacabang."',
                '".$ktp."',
                '".str_replace("'", "", $nama)."',
                '".$tgllahir."',
                '".$loan."',
                '".$fasilitas."',
                '".$jkmulai."',
                '".$jkakhir."',
                '".$tenor."',
                '".$plafond."',
                '".$premi."',
                '".$polis."',
                '".$asuransi."',
                '".$tagihan."',
                '".$porsi."',
                '".$kode_jenisklaim."',
                '".$tglkematian."',
                '".$tgllapor."',
                '".$tglsubmit."',
                '".$tglmodify."',
                '".$modifyby."',
                '".$outstandingpokok."',
                '".$outstandingbunga."',
                '".$outstandingtotalklaim."',
                '".$statusklaim."',
                '".$tglkirim."',
                '".$jasaekspedisi."',
                '".$noresi."',
                '".$remarkcabang."',
                '".$remarkasuransi."',
                '".$masa."',
                '".$status_step."'
                )");
            // }

                $med = '0';
                $res = '0';
                $status = '1';
                 $staklaim = '2';

                $insertdok = $this->db->query("insert into PJM_SAUDARA.dbo.DataPenutupan (
                cab,
                namacab,
                pk,
                norek,
                nama,
                lahir,
                buka,
                tempo,
                amount,
                ktp,
                asuransi,
                status_medical,
                status_klaim,
                status_restitusi,
                status,
                plankredit
                ) 
                values (
                '".$kodecabang."',
                '".$namacabang."',
                '".$loan."',
                '".$cif."',
                '".str_replace("'", "", $nama)."',
                '".$tgllahir."',
                '".$jkmulai."',
                '".$tenor."',
                '".$plafond."',
                '".$ktp."',
                '".$asuransi."',
                '".$med."',
                '".$staklaim."',
                '".$res."',
                '".$status."',
                '".$fasilitas."'
                )");
              }

                
        }

        $this->session->set_flashdata('success', 'Upload Data Klaim Success');
         redirect('pengajuanklaim');
    }

    function save() //step1
    {
        extract($_POST);

        // var_dump($_POST);
        // die();

        $dok = $this->db->query("select kodeklaim from PJM_SAUDARA.dbo.Klaim where kodeklaim = '$noklaim'")->result_array();

        if (!empty($_POST)) {
            if (empty($dok)) {
                $cab = $this->db->query("select nama_cabang from PJM_SAUDARA.dbo.Cabang where id_cabang = '$capems'")->result_array();
                $cabang = $cab[0]['nama_cabang'];
                $asu = $this->db->query("select nama_asuransi from PJM_SAUDARA.dbo.Asuransi where kode_asuransi = '$asuransi'")->result_array();
                $asura = $asu[0]['nama_asuransi'];
                $return = $this->db->query("insert into PJM_SAUDARA.dbo.Klaim
                    (kodeklaim,
                     tgllapor,
                      loan,
                       cif,
                        nama,
                         tgllahir,
                          polis,
                           namacabang,
                            asuransi,
                             tglkematian,
                              plafond,
                              fasilitas,
                               outstandingpokok,
                                outstandingbunga,
                                 date_created,
                                  kodecabang,
                                   masa_asuransi,
                                    kode_jenisklaim,
                                     status_step,
                                      statusklaim)
                    values 
                    ('$noklaim',
                     '$tglsuratbank',
                      '$noloan',
                       '$nocif',
                        '$namadebitur',
                         '$tgllahir',
                          '$nosertifikatpolis',
                           '$cabang',
                            '$asura',
                             '$tglkejadian',
                              '$nilaiplafon',
                              '$tipemanfaat',
                               '$nilaipengajuanklaim',
                                '$nilaibungapengajuanklaim',
                                 GETDATE(),
                                  '$capems',
                                   'PRA BROKER',
                                    '$jenisklaim',
                                     '1',
                                      'PROSES CABANG')");

                $return = $this->db->query("insert into PJM_SAUDARA.dbo.DataPenutupan
                    (pk, norek, nama, lahir, namacab, cab, asuransi, plankredit, amount, date_created, createdby)
                    values ('$noloan', '$nocif', '$namadebitur', '$tgllahir', '$cabang', '$capems', '$asura', '$tipemanfaat', '$nilaiplafon', GETDATE(), '".$this->session->userdata('NamaUser')."')");

                // echo $return;
                if ($return) {
                $kla = $this->db->query("select id_kla from PJM_SAUDARA.dbo.Klaim where kodeklaim = '$noklaim' ")->row();
                $id = $kla->id_kla;
                echo $id;
                // redirect('editklaim/'.$id);
                }
                }
        }
    }

    function klaimna($kodeklaim)
    {
        $data['title']      = 'Pengajuan Klaim';
        $data['sub_menu']   = 22;
        $data['page_id']    = 10;

        $data['klaim'] = $this->db->query("select * from PJM_SAUDARA.dbo.Klaim where kodeklaim = $kodeklaim");
        $this->template->load('template','pengajuanklaim',$data);
    }

    function getstatus()
    {
        extract($_POST);
        $datasta = '';
        $datastatus = $this->Pengajuanklaim_m->getstatus($noklaim);
        foreach ($datastatus as $key => $value) {
            $datasta = $value['status_step'];
        }
        echo $datasta;
    }

    function updatestepdua()
    {
        extract($_POST);

        $file_name = $noklaim;
        $config['upload_path']          = FCPATH.'/upload/dokbukti/';
        $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|zip';
        $config['file_name']            = $file_name;
        $config['overwrite']            = true;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('buktijasa')) {
            $uploaded_data = $this->upload->data();

            $new_data = [
                'id_data' => $id_data,
                'buktijasa' => 'buktibatal'.$uploaded_data['file_name'],
            ];
            
            $new = $new_data['buktipembatalan'];

            $return = $this->db->query("update PJM_SAUDARA.dbo.Klaim set jasaekspedisi = '$ekspedisi', buktijasa = '$new', noresi = '$noresi', tglkirim = '$tglekspedisi', date_created = GETDATE(), status_step = '2' where kodeklaim = '$noklaim'");
            echo $return;

            $this->session->set_flashdata('success', 'Update Data Success');
            redirect('dokumen');
        }

        
    }

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datakerjatertanggung extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']          = "Data Kerja Tertanggung";
        $data['sub_menu']       = 0;
        $data['page_id']        = 1;
        
        $this->template->load('template','datakerjatertanggung',$data);
    }

    // function update_password()
    // {
    //     extract($_POST);
    //     $data = array(
    //             'password'      => md5($password)
    //     );

    //     $this->db->where('id_user',$id_user);
    //     $this->db->update('tm_user',$data);
    //     redirect('login/logout');
    // }


}
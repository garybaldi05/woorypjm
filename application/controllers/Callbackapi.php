<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//contoh callback yang bisa dipanggil 
class Callbackapi extends CI_Controller {

    function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        // $a = $this->input->raw_input_stream;
        // var_dump($a);
        // die();
        $this->getdatariau();
    }

    // nantinya server bank akan manggil function ini dengan nembak url :
    // IPServer/namaaplikasi/callbackapi/paid
    // contoh: http://203.210.84.220/aplikasiPAN/callbackapi/getdatariau

    public function getdatariau(){
        // $data = file_get_contents('php://input'); //data yang dikirim dari bank berbentuk json
        $data = $this->input->raw_input_stream;
        // var_dump($data);
        // die();
        $data_json = json_decode($data, true); //parse json menjadi array

        // var_dump($data_json);
        // die();

        //selanjutnya lakukan proses data sesuai sesuai kebutuhan
        $success = true;
        if(!empty($data_json)){

            date_default_timezone_set('Asia/Jakarta');

            $arr_data = array(
            'lahir1' => $data_json['lahir'],
            'amount1' => $data_json['amount'],
            'id1' => $data_json['id'],
            'tempo1' => $data_json['tempo'],
            'rate_asuransi1' => $data_json['rate_asuransi'],
            'plan1' => $data_json['plan'],
            'old_pk1' => $data_json['old_pk'],
        );

            $amount = $arr_data['amount1'];
            $ab = $arr_data['lahir1'];
            $id = $arr_data['id1'];
            $oldpk1 = $arr_data['old_pk1'];
            $tanggal = new DateTime($ab);
            $today = new DateTime('today');
            $y = $today->diff($tanggal)->y;
            $m = $today->diff($tanggal)->m;
            $d = $today->diff($tanggal)->d;
            if ($m >= 6 && $d > 0) {
                $year = $y + 1;
            } else{
                $year = $y;
            }

            $i = $arr_data['tempo1'];
            if (!empty($i)) {
                $yearss = $i/12;
                $pembulatan = number_format((float)$yearss, 1);
                $yea = substr(strrchr($pembulatan, "."), 1);
                    if ($yea > 6) {
                        $years = floor($pembulatan) + 1;
                    } else{
                        $years = floor($pembulatan);
                    }
            } else{
                $years = '0';
            }

            $medcheck = $this->db->query("select a.id_pekerjaan, c.id_type, a.kode_jenisdeb, b.status, b.usia_max FROM PAN_BRK.dbo.MasterDebitur a
            left join PAN_BRK.dbo.MedicalCheckup b on b.kode_jenisdeb = a.kode_jenisdeb
            left join PAN_BRK.dbo.TypeManfaat c on c.kode_jenisdeb = a.kode_jenisdeb
            where a.id_pekerjaan = '$id' and
            '$amount' between b.plafon_min and b.plafon_max
            and '$year' between b.usia_min and b.usia_max
            group by a.kode_jenisdeb, b.status, a.id_pekerjaan, c.id_type, b.usia_max")->result_array();

            $plans = $arr_data['plan1'];
            if ($plans == '326' || $plans == '358') {
                $kode = 'INTERNALBANK';
            } elseif ($id == '1' && $oldpk1 != '') {
                $kode = 'UMUM01TOPUP';
            } else{
                if (!empty($medcheck[0]['kode_jenisdeb'])) {
                    $kode = $medcheck[0]['kode_jenisdeb'];
                } else{
                    $kode = '';
                }
                
            }

             if ($kode == 'PENSIUNAN' || $year >= 55) {
                $idtypes = '13';
            } elseif ($kode == 'UMUM01') {
                $idtypes = '1';
            } elseif ($kode == 'KHUSUS01') {
                $idtypes = '2';
            } elseif ($kode == 'INTERNALBANK') {
                $idtypes = '6';
                $id = '43';
            } elseif ($kode == 'DEWAN') {
                $idtypes = '4';
            } elseif ($kode == 'UMUM01TOPUP') {
                $idtypes = '5';
                $id = '42';
            } else{
                if (!empty($medcheck[0]['id_type'])) {
                    $idtypes = $medcheck[0]['id_type'];
                } else{
                    $idtypes = '';
                }
                
            }

            $type = $this->db->query("select * from PAN_BRK.dbo.Rate a
            join PAN_BRK.dbo.TypeManfaat b on a.id_type = b.id_type
            join PAN_BRK.dbo.MasterDebitur c on c.kode_jenisdeb = b.kode_jenisdeb
            join PAN_BRK.dbo.MedicalCheckup d on d.kode_jenisdeb = c.kode_jenisdeb
            where c.kode_jenisdeb = '$kode' and a.jangka_waktu = '$years' and a.id_type = '$idtypes' and c.id_pekerjaan = '$id' and '$amount' between d.plafon_min and d.plafon_max and '$year' between d.usia_min and d.usia_max")->result_array();

            if (empty($type) || $type == '' || $type == null) {
                $med = 'CBC';
                $rate = '0'; 
            } else{
                $med = $type[0]['status'];
                $rate = $type[0]['rate'];
            }

            $a = 0;

        $querys = $this->db->query("select * from PAN_BRK.dbo.DataRiau where 
                pk = '$oldpk1'")->result_array();

            if (!empty($querys)) {
                $r = 2;
            }else{
                $r = 0;
            }

            $sta = 1;

            // foreach($data_json as $idx => $dt){
                $arr_insert = array(
                    'kodeh2h' => $data_json['kodeh2h'],
                    'username' => $data_json['username'],
                    'password' => $data_json['password'],
                    'cab' => $data_json['cab'],
                    'pk' => $data_json['pk'],
                    'norek' => $data_json['norek'],
                    'nama' => $data_json['nama'],
                    'lahir' => $data_json['lahir'],
                    'buka' => $data_json['buka'],
                    'tempo' => $data_json['tempo'],
                    'plankredit' => $data_json['plan'],
                    'amount' => $data_json['amount'],
                    'id' => $data_json['id'],
                    'ktp' => $data_json['ktp'],
                    'rate' => $data_json['rate'],
                    'sex' => $data_json['sex'],
                    'rate_asuransi' => $rate,
                    'asuransi' => $data_json['asuransi'],
                    'npwp' => $data_json['npwp'],
                    'old_pk' => $data_json['old_pk'],
                    'status_medical' => $med,
                    'date_created' => date("Y-m-d H:i:s"),
                    'status_klaim' => $a,
                    'status_rekon' => $a,
                    'status_restitusi' => $r,
                    'id_type' => $idtypes,
                    'status' => $sta,
                );
                // var_dump($arr_insert);
                // die();
                $insert = $this->db->query("insert into PAN_BRK.dbo.DataRiau (
                kodeh2h,
                userid,
                password,
                cab,
                pk,
                norek,
                nama,
                lahir,
                buka,
                tempo,
                plankredit,
                amount,
                id,
                ktp,
                rate,
                sex,
                rate_asuransi,
                asuransi,
                npwp,
                old_pk,
                status_medical,
                date_created,
                status_klaim,
                status_rekon,
                status_restitusi,
                id_type,
                status
                ) 
                values (
                '".$arr_insert['kodeh2h']."',
                '".$arr_insert['username']."',
                '".$arr_insert['password']."',
                '".$arr_insert['cab']."',
                '".$arr_insert['pk']."',
                '".$arr_insert['norek']."',
                '".$arr_insert['nama']."',
                '".$arr_insert['lahir']."',
                '".$arr_insert['buka']."',
                '".$arr_insert['tempo']."',
                '".$arr_insert['plankredit']."',
                '".$arr_insert['amount']."',
                '".$arr_insert['id']."',
                '".$arr_insert['ktp']."',
                '".$arr_insert['rate']."',
                '".$arr_insert['sex']."',
                '".$arr_insert['rate_asuransi']."',
                '".$arr_insert['asuransi']."',
                '".$arr_insert['npwp']."',
                '".$arr_insert['old_pk']."',
                '".$arr_insert['status_medical']."',
                '".$arr_insert['date_created']."',
                '".$arr_insert['status_klaim']."',
                '".$arr_insert['status_rekon']."',
                '".$arr_insert['status_restitusi']."',
                '".$arr_insert['id_type']."',
                '".$arr_insert['status']."'
                )");
                // $insert = $this->db->insert('DataRiau', $arr_insert);

                if($insert === false) $success = false;
            // }
        }

        if($success){
            $return = array(
                'status'    => '200',
                'message' => 'Data sudah di input'
            );
            echo json_encode($return);
            die;
        }else{
            $return = array(
                'status'    => '404',
                'message' => 'You dont have permission to access this service'
            );
            echo json_encode($return);
            die;
        }
    }

}

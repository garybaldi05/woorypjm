<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Content_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index($num='')
    {
        $data['title']      = 'Dasboard';
        $data['sub_menu']   = 21;
        $data['page_id']    = 6;

        // $data['datana']         = $this->Dokumen_m->dokumen();

        if (empty($this->uri->segment(3))) {
          $perpage = 0;
        } else{
        $perpage = $this->uri->segment(3);
        }
          $offset = 10;
         $data['datana'] = $this->Dokumen_m->getDataPagination($perpage, $offset);

         $config['base_url'] = site_url('dokumen/index');
         $config['total_rows'] = $this->Dokumen_m->dokumen()->num_rows();
         $config['per_page'] = $offset;

         $config['next_link'] = 'Next';
          $config['prev_link'] = 'Previous';
          $config['first_link'] = 'First';
          $config['last_link'] = 'Last';
          $config['full_tag_open'] = '<ul class="pagination">';
          $config['full_tag_close'] = '</ul>';
          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
          $config['cur_tag_open'] = '<li class="active"><a href="#">';
          $config['cur_tag_close'] = '</a></li>';
          $config['prev_tag_open'] = '<li>';
          $config['prev_tag_close'] = '</li>';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';

         $this->pagination->initialize($config);

        $this->template->load('template','dokumen',$data);
    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Dokumen_m->get_capem($id);
        echo json_encode($data);
    }

    function search()
    {

        extract($_POST);

        $data['title']      = 'Penutupan Asuransi';
        $data['sub_menu']   = 21;
        $data['page_id']    = 6;

        $data['search']         = $this->Dokumen_m->search();

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun) || !empty($rekon) || !empty($medical) || !empty($klaim) || !empty($restitusi)){
            $data['datana']         = $this->Dokumen_m->search();
        } else{
            $data['datana']         = $this->Dokumen_m->dokumen();
        }
        $this->template->load('template','dokumen',$data);
    }

    function update($id_data)
    {
        extract($_POST);
        $date = date("Y-m-d H:i:s");
        $lahir1 = date("Ymd", strtotime($lahir));
        $buka1 = date("Ymd", strtotime($buka));
        $tglasuransi = date("Ymd", strtotime($tglpengajuanasuransi));
        $amount1 = str_replace(",", "", $amount);
        if (!empty($sex)) {
        if ($sex === 'Laki-Laki') {
            $sex = 'L';
        }else if ($sex === 'Perempuan') {
            $sex = 'P';
        } else{
            $sex = '';
        }
         }

         if ($tglasuransi == 19700101) {
            $this->db->query("update PJM_SAUDARA.dbo.DataPenutupan set cab = '$cab', pk = '$pk', norek = '$norek', nama = '$nama', lahir = '$lahir1', buka = '$buka1', tempo = '$tempo', plankredit = '$plankredit', id = '$id', amount = '$amount1', ktp = '$ktp', rate = '$rate', sex = '$sex', asuransi = '$asuransi', npwp = '$npwp', old_pk = '$old_pk', rate_asuransi = '$rate_asuransi', status_medical = '$status_medical', modifiedby = '".$this->session->userdata('NamaUser')."', date_modified = GETDATE() where id_data = '$id_data'");
            $this->session->set_flashdata('success', 'Update Data Success');
            redirect('dokumen');
         } else{
             $this->db->query("update PJM_SAUDARA.dbo.DataPenutupan set cab = '$cab', pk = '$pk', norek = '$norek', nama = '$nama', lahir = '$lahir1', buka = '$buka1', tempo = '$tempo', plankredit = '$plankredit', id = '$id', amount = '$amount1', ktp = '$ktp', rate = '$rate', sex = '$sex', asuransi = '$asuransi', npwp = '$npwp', old_pk = '$old_pk', rate_asuransi = '$rate_asuransi', status_medical = '$status_medical', tglpengajuanasuransi = '$tglasuransi', modifiedby = '".$this->session->userdata('NamaUser')."', date_modified = GETDATE() where id_data = '$id_data'");
            $this->session->set_flashdata('success', 'Update Data Success');
            redirect('dokumen');
         }

         // var_dump($tglpengajuanasu);
         // die();
         
        $this->db->query("update PJM_SAUDARA.dbo.DataPenutupan set cab = '$cab', pk = '$pk', norek = '$norek', nama = '$nama', lahir = '$lahir1', buka = '$buka1', tempo = '$tempo', plankredit = '$plankredit', id = '$id', amount = '$amount1', ktp = '$ktp', rate = '$rate', sex = '$sex', asuransi = '$asuransi', npwp = '$npwp', old_pk = '$old_pk', rate_asuransi = '$rate_asuransi', status_medical = '$status_medical', tglpengajuanasuransi = '$tglpengajuanasu', modifiedby = '".$this->session->userdata('NamaUser')."', date_modified = GETDATE() where id_data = '$id_data'");
        $this->session->set_flashdata('success', 'Update Data Success');
        redirect('dokumen');
    }

    function delete($id_data)
    {
            $this->db->query("update PJM_SAUDARA.dbo.DataPenutupan set status = '0'
            where id_data = '$id_data'");
            $this->session->set_flashdata('success', 'Delete Data Successfully');
            redirect('dokumen');
    }


    public function download($id_data){

        $dok = $this->db->query("select buktiaktifbatal from PJM_SAUDARA.dbo.DataPenutupan
            where id_data = '$id_data' order by date_created ASC")->row();

        // $res = preg_replace('/\D/', '', $dok[0]['buktipembatalan']);
        $res = $dok->buktiaktifbatal;

        $file_name= 'upload/dokbukti/'.$res;

        $this->load->helper('download');
        $data = file_get_contents($file_name);
        $name = 'BuktiAktifBatal'.$res; // custom file name for your download

        force_download($name, $data);
    }

    public function upload_avatar()
{
    extract($_POST);

    // var_dump($_POST);
    // die();

    if ($this->input->method() === 'post') {

        $file_name = $id_data;
        $config['upload_path']          = FCPATH.'/upload/dokbukti/';
        $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|zip';
        $config['file_name']            = $file_name;
        $config['overwrite']            = true;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('buktipembatalan')) {
            $uploaded_data = $this->upload->data();

            $new_data = [
                'id_data' => $id_data,
                'buktipembatalan' => 'buktibatal'.$uploaded_data['file_name'],
            ];
            
            $new = $new_data['buktipembatalan'];

            $this->db->query("update PJM_SAUDARA.dbo.DataPenutupan set status = '$aktif', buktipembatalan = '$new' where id_data = '$id_data'");

            $this->session->set_flashdata('success', 'Update Data Success');
            redirect('dokumen');
        } else{
            $this->session->set_flashdata('error', 'Update Data Failed');
            redirect('dokumen');
        }
    }
}


}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editklaim extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->library('Excel');
        $this->load->model('Editklaim_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index($id_kla)
    {

        extract($_POST);

        // var_dump($_POST['id_data']);
        // die();

        // $id_kla = $_POST['id_kla'];

        $data['title']      = 'Edit Klaim';
        $data['sub_menu']   = 22;
        $data['page_id']    = 31;

        $data['datana'] = $this->Editklaim_m->dokumen($id_kla);

        $this->template->load('template','editklaim',$data);
    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Pengajuanklaim_m->get_capem($id);
        echo json_encode($data);
    }

    function get_jenisklaim()
    {
        $id=$this->input->post('id');
        $data=$this->Pengajuanklaim_m->get_jenisklaim($id);
        echo json_encode($data);
    }

        public function excel()
        {

        $file = $_FILES['file']['name'];
        $fileName = str_replace(' ', '_', $file);
          
        $config['upload_path'] = './upload/dokklaim/'; //path upload
        $config['file_name'] = $fileName;  // nama file
        $config['allowed_types'] = 'xls|xlsx|csv'; //tipe file yang diperbolehkan
        $config['max_size'] = 10000; // maksimal sizze
 
        $this->load->library('upload'); //meload librari upload
        $this->upload->initialize($config);
          
        if(! $this->upload->do_upload('file') ){
           $this->session->set_flashdata('error', 'Please check your file (.xls / .xlsx)');
            redirect('pengajuanklaim');
        }
              
                $inputFileName = './upload/dokklaim/'.$fileName;
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);

                $sheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true ,true);
                $data = array();
    
                $numrow = 1;
                foreach($sheet as $row){
                  // Cek $numrow apakah lebih dari 1
                  // Artinya karena baris pertama adalah nama-nama kolom
                  // Jadi dilewat saja, tidak usah diimport
                  if($numrow > 1){
                    // Kita push (add) array data ke variabel data
                    array_push($data, array(
                      // 'kodeklaim'=>$row['A'],
                      'cif'=>$row['B'],
                      'kodecabang'=>$row['C'],
                      'wilayah'=>$row['D'],
                      'namacabang'=>$row['E'],
                      'ktp'=>$row['F'],
                      'nama'=>$row['G'],
                      'tgllahir'=>$row['H'],
                      'loan'=>$row['I'],
                      'fasilitas'=>$row['J'],
                      'jkmulai'=>$row['K'],
                      'jkakhir'=>$row['L'],
                      'tenor'=>$row['M'],
                      'plafond'=>$row['N'],
                      'premi'=>$row['O'],
                      'polis'=>$row['P'],
                      'asuransi'=>$row['Q'],
                      'tagihan'=>$row['R'],
                      'tglkematian'=>$row['S'],
                      'tgllapor'=>$row['T'],
                      'tglsubmit'=>$row['U'],
                      'tglmodify'=>$row['V'],
                      'modifyby'=>$row['W'],
                      'outstandingpokok'=>$row['X'],
                      'outstandingbunga'=>$row['Y'],
                      'outstandingtotalklaim'=>$row['Z'],
                      'statusklaim'=>$row['AA'],
                      'tglkirim'=>$row['AB'],
                      'jasaekspedisi'=>$row['AC'],
                      'noresi'=>$row['AD'],
                      'remarkcabang'=>$row['AE'],
                      'remarkasuransi'=>$row['AF'],
                    ));
                  }
                  
                  $numrow++; // Tambah 1 setiap kali looping
                }

                $i = 0;

                foreach($data as $key=>$val)
                {
                        // $kodeklaim = $data[$key]['kodeklaim'];
                        $cif = $data[$key]['cif'];
                        $kodecabang = $data[$key]['kodecabang'];
                        $wilayah = $data[$key]['wilayah'];
                        $namacabang = $data[$key]['namacabang'];
                        $ktp = $data[$key]['ktp'];
                        $nama = $data[$key]['nama'];
                        $tgllahir = $data[$key]['tgllahir'];
                        $loan = $data[$key]['loan'];
                        $fasilitas = $data[$key]['fasilitas'];
                        $jkmulai = $data[$key]['jkmulai'];
                        $jkakhir = $data[$key]['jkakhir'];
                        $tenor = $data[$key]['tenor'];
                        $plafond = $data[$key]['plafond'];
                        $premi = $data[$key]['premi'];
                        $polis = $data[$key]['polis'];
                        $asuransi = $data[$key]['asuransi'];
                        $tagihan = $data[$key]['tagihan'];
                        $tglkematian = $data[$key]['tglkematian'];
                        $tgllapor = $data[$key]['tgllapor'];
                        $tglsubmit = $data[$key]['tglsubmit'];
                        $tglmodify = $data[$key]['tglmodify'];
                        $modifyby = $data[$key]['modifyby'];
                        $outstandingpokok = $data[$key]['outstandingpokok'];
                        $outstandingbunga = $data[$key]['outstandingbunga'];
                        $outstandingtotalklaim = $data[$key]['outstandingtotalklaim'];
                        $statusklaim = $data[$key]['statusklaim'];
                        $tglkirim = $data[$key]['tglkirim'];
                        $jasaekspedisi = $data[$key]['jasaekspedisi'];
                        $noresi = $data[$key]['noresi'];
                        $remarkcabang = $data[$key]['remarkcabang'];
                        $remarkasuransi = $data[$key]['remarkasuransi'];

                         $kodek = $this->db->query("select kodeklaim FROM PJM_SAUDARA.dbo.Klaim order by kodeklaim DESC");
                      if ($kodek->num_rows() <> 0) {
                                $datas = $kodek->row();
                                $kode = intval($datas->kodeklaim) + 1;

                            } else {
                                $kode = 1;
                            }

                            $kodemax = str_pad($kode++, 4, "0", STR_PAD_LEFT);

                      if ($kodek->num_rows() <> 0) {
                            for($x = 1; $x < count($data); $x++) {
                                $jour = substr($datas->kodeklaim,0,6);
                                $head = $this->db->query("select top 1 kodeklaim, RIGHT(kodeklaim,4) as voucher from PJM_SAUDARA.dbo.Klaim where LEFT(kodeklaim, 6) = '$jour' ORDER BY kodeklaim desc");
                              $da = $head->row();
                              $kodes = intval($da->voucher) + 1;
                              $kodemaxs = str_pad($kodes++, 4, "0", STR_PAD_LEFT);
                              $kodekla = substr($datas->kodeklaim,0,6).$kodemaxs;
                            }
                      } else{
                          $kodekla = date('Ym').$kodemax;
                      }
                        $masa = 'PRA BROKER';
                        $insert = $this->db->query("insert into PJM_SAUDARA.dbo.Klaim (
                        kodeklaim,
                        cif,
                        kodecabang,
                        wilayah,
                        namacabang,
                        ktp,
                        nama,
                        tgllahir,
                        loan,
                        fasilitas,
                        jkmulai,
                        jkakhir,
                        tenor,
                        plafond,
                        premi,
                        polis,
                        asuransi,
                        tagihan,
                        tglkematian,
                        tgllapor,
                        tglsubmit,
                        tglmodify,
                        modifyby,
                        outstandingpokok,
                        outstandingbunga,
                        outstandingtotalklaim,
                        statusklaim,
                        tglkirim,
                        jasaekspedisi,
                        noresi,
                        remarkcabang,
                        remarkasuransi,
                        masa_asuransi
                        ) 
                        values (
                        '".$kodekla."',
                        '".$cif."',
                        '".$kodecabang."',
                        '".$wilayah."',
                        '".$namacabang."',
                        '".$ktp."',
                        '".str_replace("'", "", $nama)."',
                        '".$tgllahir."',
                        '".$loan."',
                        '".$fasilitas."',
                        '".$jkmulai."',
                        '".$jkakhir."',
                        '".$tenor."',
                        '".$plafond."',
                        '".$premi."',
                        '".$polis."',
                        '".$asuransi."',
                        '".$tagihan."',
                        '".$tglkematian."',
                        '".$tgllapor."',
                        '".$tglsubmit."',
                        '".$tglmodify."',
                        '".$modifyby."',
                        '".$outstandingpokok."',
                        '".$outstandingbunga."',
                        '".$outstandingtotalklaim."',
                        '".$statusklaim."',
                        '".$tglkirim."',
                        '".$jasaekspedisi."',
                        '".$noresi."',
                        '".$remarkcabang."',
                        '".$remarkasuransi."',
                        '".$masa."'
                        )");
                    // }

                        $med = '0';
                        $res = '0';
                        $status = '0';
                        // if (!empty($kodeklaim)) {
                            $staklaim = '2';
                        // }else{
                        //     $staklaim = '0';
                        // }
                        $insertdok = $this->db->query("insert into PJM_SAUDARA.dbo.DataPenutupan (
                        cab,
                        namacab,
                        pk,
                        norek,
                        nama,
                        lahir,
                        buka,
                        tempo,
                        amount,
                        ktp,
                        asuransi,
                        status_medical,
                        status_klaim,
                        status_restitusi,
                        status
                        ) 
                        values (
                        '".$kodecabang."',
                        '".$namacabang."',
                        '".$loan."',
                        '".$cif."',
                        '".str_replace("'", "", $nama)."',
                        '".$tgllahir."',
                        '".$jkmulai."',
                        '".$tenor."',
                        '".$plafond."',
                        '".$ktp."',
                        '".$asuransi."',
                        '".$med."',
                        '".$staklaim."',
                        '".$res."',
                        '".$status."'
                        )");
                }

                $this->session->set_flashdata('success', 'Upload Data Klaim Success');
                 redirect('pengajuanklaim');
            }

     function save()
    {
        extract($_POST);

        // var_dump($_POST);
        // die();



        $dok = $this->db->query("select kodeklaim from PJM_SAUDARA.dbo.Klaim where kodeklaim = '$noklaim'")->result_array();

        // if (!empty($_POST)) {
        //     if (empty($dok)) {
                $cab = $this->db->query("select nama_cabang from PJM_SAUDARA.dbo.Cabang where id_cabang = '$cabangs'")->result_array();
                $caba = $cab[0]['nama_cabang'];
                $asu = $this->db->query("select nama_asuransi from PJM_SAUDARA.dbo.Asuransi where kode_asuransi = '$asuransi'")->result_array();
                $asura = $asu[0]['nama_asuransi'];  

                $return = $this->db->query("update PJM_SAUDARA.dbo.Klaim set tgllapor = '$tgllapor', loan = '$noloan', cif = '$nocif', nama = '$namadebitur', tgllahir = '$tgllahir', polis = '$nosertifikatpolis', namacabang = '$caba', asuransi = '$asuransi', tglkematian = '$tglkematian', plafond = '$nilaiplafon', outstandingpokok = '$nilaipengajuanklaim', outstandingbunga = '$nilaibungapengajuanklaim', kodecabang = '$cabangs', fasilitas = '$tipemanfaat' where kodeklaim = '$noklaim'");


                // $return = $this->db->query("insert into PJM_SAUDARA.dbo.Klaim
                //     (kodeklaim, tgllapor, loan, cif, nama, tgllahir, polis, namacabang, asuransi, tglkematian, plafond, outstandingtotalklaim, date_created, kodecabang, masa_asuransi, fasilitas)
                //     values ('$noklaim', '$tglsuratbank', '$noloan', '$nocif', '$namadebitur', '$tgllahir', '$nosertifikatpolis', '$cabang', '$asura', '$tglkejadian', '$nilaiplafon', '$nilaipengajuanklaim', GETDATE(), '$capems', 'PRA BROKER', '$tipemanfaat')");

                // $return = $this->db->query("insert into PJM_SAUDARA.dbo.DataPenutupan
                //     (pk, norek, nama, lahir, namacab, cab, asuransi, amount, date_created)
                //     values ('$noloan', '$nocif', '$namadebitur', '$tgllahir', '$cabang', '$capems', '$asuransi', '$nilaiplafon', GETDATE())");

                echo $return;
                // }
            //     else{
            //         $return = $this->db->query("update PJM_SAUDARA.dbo.Klaim set jasaekspedisi = '$ekspedisi', noresi = '$noresi', tglkirim = '$tglekspedisi', date_created = GETDATE() where kodeklaim = '$noklaim'");
            //     echo $return;
            // }
        // }
    }

    function updatestepdua()
    {
        extract($_POST);

        $config['upload_path']          = FCPATH.'/upload/dokbukti/';
        $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|zip';
        $config['overwrite']            = true;

        $this->load->library('upload', $config);

        if($_FILES['buktijasa']['name'])
    {
        if ($this->upload->do_upload('buktijasa'))
        {
          $data = array('upload_data' => $this->upload->data());

            $new_data = [
                'noklaim' => $noklaim,
                'buktijasa' => 'buktijasa_'.$data['upload_data']['file_name'],
            ];
            
            $new = $new_data['buktijasa'];

            $return = $this->db->query("update PJM_SAUDARA.dbo.Klaim set jasaekspedisi = '$ekspedisi', buktijasa = '$new', noresi = '$noresi', tglkirim = '$tglekspedisi', date_created = GETDATE(), status_step = '2' where kodeklaim = '$noklaim'");
            echo $return;
       }
   }
        
    }

    function updatesteptiga($idna)
    {
        extract($_POST);

        $upload1 = $_FILES['sp_kesehatan']['name'];
        $nmfile1 = time()."_".$upload1;

        if($upload1 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile1;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('sp_kesehatan');               
           
           $data1 = $this->upload->data();
        }

        $upload2 = $_FILES['fc_ktp']['name'];
        $nmfile2 = time()."_".$upload2;

        if($upload2 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile2;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('fc_ktp');               
           
           $data2 = $this->upload->data();
        }

        $upload3 = $_FILES['fc_perjanjiankredit']['name'];
        $nmfile3 = time()."_".$upload3;

        if($upload3 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile3;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('fc_perjanjiankredit');               
           
           $data3 = $this->upload->data();
        }

        $upload4 = $_FILES['form_klaim']['name'];
        $nmfile4 = time()."_".$upload4;

        if($upload4 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile4;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('form_klaim');               
           
           $data4 = $this->upload->data();
        }

        $upload5 = $_FILES['loan_quiry']['name'];
        $nmfile5 = time()."_".$upload5;

        if($upload5 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile5;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('loan_quiry');               
           
           $data5 = $this->upload->data();
        }

        $upload6 = $_FILES['bukti_pencariankredit']['name'];
        $nmfile6 = time()."_".$upload6;

        if($upload6 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile6;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('bukti_pencariankredit');               
           
           $data6 = $this->upload->data();
        }

        $upload7 = $_FILES['fc_kartutandapegawai']['name'];
        $nmfile7 = time()."_".$upload7;

        if($upload7 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile7;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('fc_kartutandapegawai');               
           
           $data7 = $this->upload->data();
        }

        $upload8 = $_FILES['fc_skterusan']['name'];
        $nmfile8 = time()."_".$upload8;

        if($upload8 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile8;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('fc_skterusan');               
           
           $data8 = $this->upload->data();
        }

        $upload9 = $_FILES['sk_polisi']['name'];
        $nmfile9 = time()."_".$upload9;

        if($upload9 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile9;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('sk_polisi');               
           
           $data9 = $this->upload->data();
        }

        $upload10 = $_FILES['skematian_dokter']['name'];
        $nmfile10 = time()."_".$upload10;

        if($upload10 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile10;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('skematian_dokter');               
           
           $data10 = $this->upload->data();
        }

        $upload11 = $_FILES['sk_phk']['name'];
        $nmfile11 = time()."_".$upload11;

        if($upload11 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile11;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('sk_phk');               
           
           $data11 = $this->upload->data();
        }

        $upload12 = $_FILES['s_peringatan']['name'];
        $nmfile12 = time()."_".$upload12;

        if($upload12 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile12;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('s_peringatan');               
           
           $data10 = $this->upload->data();
        }

        $upload13 = $_FILES['s_tuntutanklaim']['name'];
        $nmfile13 = time()."_".$upload13;

        if($upload13 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile13;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('s_tuntutanklaim');               
           
           $data13 = $this->upload->data();
        }

        $upload14 = $_FILES['sp_bank']['name'];
        $nmfile14 = time()."_".$upload14;

        if($upload14 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile14;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('sp_bank');               
           
           $data14 = $this->upload->data();
        }

        $upload15 = $_FILES['lap_kunjungandebitur']['name'];
        $nmfile15 = time()."_".$upload15;

        if($upload13 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile15;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('lap_kunjungandebitur');               
           
           $data15 = $this->upload->data();
        }

        $upload16 = $_FILES['kronologi_macet']['name'];
        $nmfile16 = time()."_".$upload16;

        if($upload16 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile16;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('kronologi_macet');               
           
           $data16 = $this->upload->data();
        }

        $upload17 = $_FILES['slik_ojk']['name'];
        $nmfile17 = time()."_".$upload17;

        if($upload17 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile17;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('slik_ojk');               
           
           $data17 = $this->upload->data();
        }

        $upload18 = $_FILES['s_tuntutanklaim']['name'];
        $nmfile18 = time()."_".$upload18;

        if($upload18 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile18;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('s_tuntutanklaim');               
           
           $data18 = $this->upload->data();
        }

        $upload19 = $_FILES['sk_paw']['name'];
        $nmfile19 = time()."_".$upload19;

        if($upload19 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile19;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('sk_paw');               
           
           $data19 = $this->upload->data();
        }

        $upload20 = $_FILES['s_peringatan']['name'];
        $nmfile20 = time()."_".$upload20;

        if($upload20 !== ""){
           $config['upload_path']          = './upload/dokumen/';
           $config['allowed_types']        = 'gif|jpg|png|jpeg';
           $config['max_size']             = 50000;
           $config['file_name']            = $nmfile20;
           
           $this->load->library('upload', $config);
           $this->upload->do_upload('s_peringatan');               
           
           $data20 = $this->upload->data();
        }

        // $data1['file_name'],

        // $result = $this->db->query("insert into Klaim  ")

        // echo json_decode($result);
        
    }

}
<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Import extends CI_Controller {

        public function index()
        {

            $data['title']      = 'Pengajuan Klaim';
            $data['sub_menu']   = 22;
            $data['page_id']    = 10;

            $this->template->load('template','pengajuanklaim',$data);

            // $data['title'] = 'Import Excel';
            // $data['mahasiswa'] = $this->db->get('mahasiswa')->result();
            // $this->load->view('import/index', $data);
        }

        public function create()
        {
            $data['title'] = "Upload File Excel";
            $this->load->view('import/create', $data);
        }

        public function excel()
        {
            if(isset($_FILES["file"]["name"])){
                  // upload
                $file_tmp = $_FILES['file']['tmp_name'];
                $file_name = $_FILES['file']['name'];
                $file_size =$_FILES['file']['size'];
                $file_type=$_FILES['file']['type'];
                // move_uploaded_file($file_tmp,"uploads/".$file_name); // simpan filenya di folder uploads

                $file_name = $id_data;
                $config['upload_path']          = FCPATH.'/upload/dokbukti/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|zip';
                $config['file_name']            = $file_name;
                $config['overwrite']            = true;

                // $this->load->library('upload', $config);
                
                $object = PHPExcel_IOFactory::load($config);
        
                foreach($object->getWorksheetIterator() as $worksheet){
        
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
        
                    for($row=4; $row<=$highestRow; $row++){
        
                        $kodeklaim = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                        $cif = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                        $kodecabang = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                        $wilayah = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                        $namacabang = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                        $ktp = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                        $nama = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                        $tgllahir = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                        $loan = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                        $fasilitas = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                        $jkmulai = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                        $jkakhir = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                        $tenor = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                        $plafond = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                        $premi = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
                        $polis = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                        $asuransi = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
                        $tagihan = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
                        $tglkematian = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                        $tgllapor = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
                        $tglsubmit = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
                        $tglmodify = $worksheet->getCellByColumnAndRow(21, $row)->getValue();
                        $modifyby = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
                        $outstandingpokok = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
                        $outstandingbunga = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
                        $statusklaim = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
                        $tglkirim = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
                        $jasaekspedisi = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
                        $noresi = $worksheet->getCellByColumnAndRow(28, $row)->getValue();
                        $remarkcabang = $worksheet->getCellByColumnAndRow(29, $row)->getValue();
                        $remarkasuransi = $worksheet->getCellByColumnAndRow(30, $row)->getValue();

                        $data[] = array(
                            'nim'          => $nim,
                            'nama'          =>$nama,
                            'angkatan'         =>$angkatan,
                        );
        
                    } 
        
                }
        
                $this->db->insert_batch('mahasiswa', $data);
        
                $message = array(
                    'message'=>'<div class="alert alert-success">Import file excel berhasil disimpan di database</div>',
                );
                
                $this->session->set_flashdata($message);
                redirect('import');
            }
            else
            {
                 $message = array(
                    'message'=>'<div class="alert alert-danger">Import file gagal, coba lagi</div>',
                );
                
                $this->session->set_flashdata($message);
                redirect('import');
            }
        }

    }
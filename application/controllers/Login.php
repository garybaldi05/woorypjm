<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('Login_m');
    }

    public function index()
    {
      if($this->Login_m->logged_id())
      {
          //jika memang session sudah terdaftar, maka redirect ke halaman dahsboard
          // $this->load->view('login');
        redirect('beranda');

      }else{
          //jika session belum terdaftar
          //set form validation
          extract($_POST);
          // var_dump($_POST);
          // die();

          $a = $this->form_validation->set_rules('KodeUser', 'KodeUser', 'required');
          $b = $this->form_validation->set_rules('Password', 'Password', 'required');

          //set message form validation
          $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
              <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');

          //cek validasi
          if ($this->form_validation->run() == TRUE) {

          $where = array(
            'KodeUser' => $KodeUser,
            'Password' => md5($Password)
            );

          $checking = $this->Login_m->cek_login($KodeUser,$Password)->num_rows();

          $nama = $this->db->query("select NamaUser from PJM_SAUDARA.dbo.DaftarUser where KodeUser = '$KodeUser'")->row();

          //jika ditemukan, maka create session
          if ($checking > 0) {
            
            $data_session = array(
            'KodeUser' => $KodeUser,
            'NamaUser' => $nama->NamaUser,
            );

          $this->session->set_userdata($data_session);

      redirect(base_url("beranda"));

          } else {
              $data['title'] = 'Login';

              $this->session->set_flashdata('message','<div class="alert alert-warning" role="alert"> User atau Password yang anda masukan salah! Silahkan Coba Lagi! </div>');

              $this->load->view('login', $data);
          }

      }else{
          $this->load->view('login');
      }

    }

  }

  function ubah_password($KodeUser)
  {
    extract($_POST);
    $this->db->query("update PJM_SAUDARA.dbo.DaftarUser set Password = '$Password' where KodeUser = '$KodeUser'");
      $this->session->unset_userdata('KodeUser');
    $this->session->unset_userdata('NamaUser');
    session_destroy();
    redirect('login');
  }

  public function logout() 
  {
    $this->session->unset_userdata('KodeUser');
    $this->session->unset_userdata('NamaUser');
    session_destroy();
    redirect('login');
  }
}
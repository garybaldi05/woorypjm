<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapklaimperiode extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Rekapklaimperiode_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Rekap Klaim Periode';
        $data['sub_menu']   = 22;
        $data['page_id']    = 11;

        $data['datana']         = $this->Rekapklaimperiode_m->dokumen();

            $this->template->load('template','rekapklaimperiode',$data);
    }

}
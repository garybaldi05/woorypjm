<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumentidaklengkap extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Dokumentidaklengkap_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Dokumen Tidak Lengkap';
        $data['sub_menu']   = 21;
        $data['page_id']    = 19;

        $data['datana']         = $this->Dokumentidaklengkap_m->dokumen();

            $this->template->load('template','dokumentidaklengkap',$data);
    }

    function update($id_data)
    {
        extract($_POST);
        $date = date("Y-m-d H:i:s");
        $lahir1 = date("Ymd", strtotime($lahir));
        $buka1 = date("Ymd", strtotime($buka));
        $amount1 = str_replace(",", "", $amount);
        if (!empty($sex)) {
        if ($sex === 'Laki-Laki') {
            $sex = 'L';
        }else if ($sex === 'Perempuan') {
            $sex = 'P';
        } else{
            $sex = '';
        }
         }
         
        $this->db->query("update PJM_SAUDARA.dbo.DataPenutupan set cab = '$cab', pk = '$pk', norek = '$norek', nama = '$nama', lahir = '$lahir1', buka = '$buka1', tempo = '$tempo', plankredit = '$plankredit', id = '$id', amount = '$amount1', ktp = '$ktp', rate = '$rate', sex = '$sex', asuransi = '$asuransi', npwp = '$npwp', old_pk = '$old_pk',
            modifiedby = '".$this->session->userdata('NamaUser')."', date_modified = GETDATE() where id_data = '$id_data'");
        $this->session->set_flashdata('success', 'Update Dokumen Tidak Lengkap Success');
        redirect('dokumentidaklengkap');
    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Dokumentidaklengkap_m->get_capem($id);
        echo json_encode($data);
    }

    function search()
    {

        extract($_POST);

        $data['title']      = 'Dokumen Tidak Lengkap';
        $data['sub_menu']   = 21;
        $data['page_id']    = 19;

        $data['search']         = $this->Dokumentidaklengkap_m->search();

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun)){
            $data['datana']         = $this->Dokumentidaklengkap_m->search();
        } else{
            $data['datana']         = $this->Dokumentidaklengkap_m->dokumen();
        }
        $this->template->load('template','dokumentidaklengkap',$data);
    }


}
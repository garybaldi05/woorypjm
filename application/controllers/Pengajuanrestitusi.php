<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuanrestitusi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Pengajuanrestitusi_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Pengajuan Restitusi';
        $data['sub_menu']   = 23;
        $data['page_id']    = 14;

        $data['datana']         = $this->Pengajuanrestitusi_m->dokumen();

            $this->template->load('template','pengajuanrestitusi',$data);
    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Pengajuanrestitusi_m->get_capem($id);
        echo json_encode($data);
    }

    function search()
    {

        extract($_POST);

        $data['title']      = 'Pengajuan Restitusi';
        $data['sub_menu']   = 23;
        $data['page_id']    = 14;

        $data['search']         = $this->Pengajuanrestitusi_m->search();

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun)){
            $data['datana']         = $this->Pengajuanrestitusi_m->search();
        } else{
            $data['datana']         = $this->Pengajuanrestitusi_m->dokumen();
        }
        $this->template->load('template','pengajuanrestitusi',$data);
    }

}
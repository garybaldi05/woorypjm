<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Master_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

	function index()
	{
        $data['title']  		= "Menu";
        $data['sub_menu']       = 25;
        $data['page_id']        = 27;

        $data['datana']         = $this->Master_m->menu();
        
		$this->template->load('template','menu',$data);
	}

    function role()
    {
        $data['title']          = "Role Menu";
        $data['sub_menu']       = 25;
        $data['page_id']        = 28;

        $data['datana']         = $this->Master_m->Role();
        
        $this->template->load('template','role',$data);
    }

    function save()
    {
        extract($_POST);

        // $a = $this->Master_m->Menu();
        $a = $_POST['to'];

        $idmenu = [];
        foreach ($a as $row)
    {
        $idmenu = $row;
    }


        $menu = $this->db->query("select * from PJM_SAUDARA.dbo.RoleMenu where id_menu = '$idmenu' and KodeUser = '$KodeUser'")->result_array();

        // var_dump($menu);
        // die();

        if (!empty($menu)){
            $this->session->set_flashdata('error', 'Menu already exist..');
            redirect('menu/role');
        }

        for ($i=0; $i < sizeof($to) ; $i++) { 
            $this->db->query("insert into PJM_SAUDARA.dbo.RoleMenu (KodeUser, id_menu) Values('$KodeUser', '$to[$i]')");
        }

        $this->session->set_flashdata('success', 'Adding Menu successfully');
        redirect('menu/role');
    }

    function edit($id_user)
    {
        $data['title']          = "Menu";
        $data['sub_menu']       = 25;
        $data['page_id']        = 27;

        $data['datana']         = $this->Admin_m->menu_user($id_user);
        
        $this->template->load('template','admin/menu/edit',$data);
    }

    function update($id_user)
    {
        extract($_POST);

        for ($i=0; $i < sizeof($to) ; $i++) { 

            $data = array(
                'id_user'       => $id_user,
                'id_menu'       => $to[$i],
            );

            $this->db->where('id_user',$id_user);
            $this->db->update('tm_role_menu',$data); 
            
        }

        redirect('menu/role');
    }

    function delete($id_role)
    {
        $this->db->query("delete from PJM_SAUDARA.dbo.RoleMenu where id_role = '$id_role'");
        redirect('menu/role');
    }

}
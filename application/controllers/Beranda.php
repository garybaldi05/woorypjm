<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Content_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']          = "Dashboard";
        $data['sub_menu']       = 0;
        $data['page_id']        = 1;

        $data['datana'] = $this->Content_m->dokumen();
        $data['status'] = $this;

        
        $this->template->load('template','content',$data);
    }

    public function status($asuransi, $statusklaim){

        $query = $this->Content_m->getdataprosescabang($asuransi, $statusklaim);
        
        return $query;

    }

    // function update_password()
    // {
    //     extract($_POST);
    //     $data = array(
    //             'password'      => md5($password)
    //     );

    //     $this->db->where('id_user',$id_user);
    //     $this->db->update('tm_user',$data);
    //     redirect('login/logout');
    // }


}
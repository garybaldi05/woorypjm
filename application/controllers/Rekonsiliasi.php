<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekonsiliasi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Rekonsiliasi_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Rekonsiliasi';
        $data['sub_menu']   = 21;
        $data['page_id']    = 18;

        $data['datana']         = $this->Rekonsiliasi_m->dokumen();

            $this->template->load('template','rekonsiliasi',$data);
    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Rekonsiliasi_m->get_capem($id);
        echo json_encode($data);
    }

    function search()
    {

        extract($_POST);

        $data['title']      = 'Rekonsiliasi';
        $data['sub_menu']   = 21;
        $data['page_id']    = 18;

        $data['search']         = $this->Rekonsiliasi_m->search();

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun)){
            $data['datana']         = $this->Rekonsiliasi_m->search();
        } else{
            $data['datana']         = $this->Rekonsiliasi_m->dokumen();
        }
        $this->template->load('template','rekonsiliasi',$data);
    }

    function update($id_data)
    {
        extract($_POST);

        // var_dump($_POST);
        // die();
        
        $date = date("Y-m-d H:i:s");
        $tglbayarrk = date("Y-m-d", strtotime($tglbayarrk));
         
        $this->db->query("update PJM_SAUDARA.dbo.DataPenutupan set tglbayarrk = '$tglbayarrk', jumlah_bayar = '$jumlahbayar', status_rekon = '1',
            createdby_rekon = '".$this->session->userdata('NamaUser')."', createddate_rekon = GETDATE() where id_data = '$id_data'");

        $this->db->query("insert into PJM_SAUDARA.dbo.Histori_Rekon 
                (id_data, nama, norek, pk, jenis_pembayaran, amount, rate_asuransi, jumlahpembayaran, tglpembayaran, createdby, createddate) 
                values ('$id_data', '$nama', '$norek', '$pk', 'Pembayaran Bank', '$amount', '$rate_asuransi', '$jumlahbayar', '$tglbayarrk', '".$this->session->userdata('NamaUser')."', GETDATE())");

        $this->session->set_flashdata('success', 'Rekonsiliasi Data Sukses');
        redirect('rekonsiliasi');
    }

}
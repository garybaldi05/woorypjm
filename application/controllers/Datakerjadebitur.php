<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datakerjadebitur extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Datakerjadebitur_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Data Kerja Debitur';
        $data['sub_menu']   = 24;
        $data['page_id']    = 3;

        $data['datana']         = $this->Datakerjadebitur_m->dokumens();

            $this->template->load('template','datakerjadebitur',$data);
    }

}
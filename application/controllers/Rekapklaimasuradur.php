<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapklaimasuradur extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Rekapklaimasuradur_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Rekap Klaim Periode';
        $data['sub_menu']   = 22;
        $data['page_id']    = 12;

        $data['datana']         = $this->Rekapklaimasuradur_m->dokumen();

            $this->template->load('template','rekapklaimasuradur',$data);
    }

}
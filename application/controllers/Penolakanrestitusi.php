<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penolakanrestitusi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Penolakanrestitusi_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Daftar Penolakan Restitusi';
        $data['sub_menu']   = 23;
        $data['page_id']    = 16;

        $data['datana']         = $this->Penolakanrestitusi_m->dokumen();

            $this->template->load('template','penolakanrestitusi',$data);
    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Penolakanrestitusi_m->get_capem($id);
        echo json_encode($data);
    }

    function search()
    {

        extract($_POST);

        $data['title']      = 'Daftar Penolakan Restitusi';
        $data['sub_menu']   = 23;
        $data['page_id']    = 16;

        $data['search']         = $this->Penolakanrestitusi_m->search();

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun)){
            $data['datana']         = $this->Penolakanrestitusi_m->search();
        } else{
            $data['datana']         = $this->Penolakanrestitusi_m->dokumen();
        }
        $this->template->load('template','penolakanrestitusi',$data);
    }

}
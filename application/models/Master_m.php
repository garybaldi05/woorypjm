<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_m extends CI_Model
{

   function Role(){
        $query = $this->db->query("select c.KodeUser, a.nama_menu, c.NamaUser, b.id_role
            from PJM_SAUDARA.dbo.Menu a 
            join PJM_SAUDARA.dbo.RoleMenu b on a.id_menu = b.id_menu
            join PJM_SAUDARA.dbo.DaftarUser c on b.KodeUser = c.KodeUser
            where a.status = 1
            group by c.KodeUser, c.NamaUser, a.nama_menu, b.id_role");
        return $query->result_array();
    }

    function user_not_exist(){
        $query = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser a where not exists(select a.KodeUser from PJM_SAUDARA.dbo.RoleMenu b where a.KodeUser = b.KodeUser) and KodeUser != 'admin'");
        return $query->result_array();
    }

    function Menu(){
        $query = $this->db->query("select * from PJM_SAUDARA.dbo.Menu order by kat_menu asc");
        return $query->result_array();
    }

}
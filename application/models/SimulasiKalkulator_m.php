<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SimulasiKalkulator_m extends CI_Model
{

    function get_tipe($id)
    {
        $query = $this->db->query("select a.id_type, a.nama_type, a.kode_jenisdeb, b.id_pekerjaan from PJM_SAUDARA.dbo.TypeManfaat a
                join PJM_SAUDARA.dbo.MasterDebitur b on a.kode_jenisdeb = b.kode_jenisdeb
                where b.id_pekerjaan = '$id'
                group by a.id_type, a.nama_type, a.kode_jenisdeb, b.id_pekerjaan
                order by a.nama_type DESC")->result();
        return $query;
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaranrestitusi_m extends CI_Model
{

    public function dokumen()
	{
		$query = $this->db->query("select * from PJM_SAUDARA.dbo.Restitusi where status = 2");
        return $query->result_array();
	}

    function get_capem($id)
    {
        $query = $this->db->query("select * from PJM_SAUDARA.dbo.Cabang
		where id_induk = '$id'
		order by id_induk ASC")->result();
        return $query;
    }

    public function search()
    {
    	extract($_POST);

    	$periode = $periodetahun.$periodebulan;
        $q = "select * from PJM_SAUDARA.dbo.Restitusi where status = 2 ";

        if (!empty($capem)){
            $q .= " and cab='$capem' ";
        } else{
        	$q .= " and cab != '' ";
        }

        if (!empty($periodebulan) and !empty($periodetahun)){
            $q .= " and LEFT(tgl_akadmulai, 6) = '$periode' ";
        } else{
        	$q .= " and tgl_akadmulai != '' ";
        }

        if (!empty($periodebulan)){
            $q .= " and SUBSTRING(tgl_akadmulai,5,2) = '$periodebulan' ";
        } else{
        	$q .= " and tgl_akadmulai != '' ";
        }

        if (!empty($periodetahun)){
            $q .= " and LEFT(tgl_akadmulai, 4) = '$periodetahun' ";
        } else{
        	$q .= " and tgl_akadmulai != '' ";
        }

        $q .= "order by cab ASC";

        $result = $this->db->query($q);
        return $result->result_array();
    }
}
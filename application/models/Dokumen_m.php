<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen_m extends CI_Model
{

    public function dokumen()
    {

        $dok = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
            where KodeUser = '".$this->session->userdata('KodeUser')."' ")->result_array();

        if (!empty($dok)) {
            $dokumen = $dok[0]['NamaUser'];
        } else{
            $dokumen = '';
        }

        // var_dump($dokumen);
        // die();

        $q = "select * from PJM_SAUDARA.dbo.DataPenutupan";

        // if (strpos($dokumen, 'Cabang') !== false){
        //         $q .= " and b.id_induk = '".$dok[0]['id_cabang']."' ";
        // } elseif (strpos($dokumen, 'Capem') !== false) {
        //     $q .= " and b.id_cabang = '".$dok[0]['id_cabang']."' ";
        // } elseif (strpos($dokumen, 'Kedai') !== false) {
        //     $q .= " and b.id_cabang = '".$dok[0]['id_cabang']."' ";
        // }

        // $q .= "order by date_created DESC";

        $result = $this->db->query($q);
        return $result;
    }

    public function getDataPagination($limit, $offset)
    {
         $query = $this->db->query("select * from PJM_SAUDARA.dbo.DataPenutupan order by date_created OFFSET $limit ROWS FETCH NEXT $offset ROWS ONLY");
        return $query->result_array();
    }

    function get_capem($id)
    {
        $query = $this->db->query("select * from PJM_SAUDARA.dbo.Cabang
		where id_induk = '$id'
		order by id_induk ASC")->result();
        return $query;
    }

    public function search()
    {
    	extract($_POST);

    	$periode = $periodetahun.$periodebulan;
        $q = "select * from PJM_SAUDARA.dbo.DataPenutupan where 
				status = 1 
				and pk != ''
				and norek != ''
				and nama != ''
				and lahir != ''
				and tempo != ''
				and plankredit != ''
				and id != ''
				and ktp != ''
				and rate != ''
				and sex != ''
				and npwp != ''";

        if (!empty($capem)){
            $q .= " and cab='$capem' ";
        } else{
        	$q .= " and cab != '' ";
        }

        if (!empty($periodebulan) and !empty($periodetahun)){
            $q .= " and LEFT(buka, 6) = '$periode' ";
        } else{
        	$q .= " and buka != '' ";
        }

        if (!empty($periodebulan)){
            $q .= " and SUBSTRING(buka,5,2) = '$periodebulan' ";
        } else{
        	$q .= " and buka != '' ";
        }

        if (!empty($periodetahun)){
            $q .= " and LEFT(buka, 4) = '$periodetahun' ";
        } else{
        	$q .= " and buka != '' ";
        }

        if (!empty($rekon)){
            $q .= " and status_rekon = '$rekon' ";
        }

        if (!empty($medical)){
            $q .= " and status_medical LIKE '%$medical%' ";
        }

        if (!empty($klaim)){
            $q .= " and status_klaim = '$klaim' ";
        }

        if (!empty($restitusi)){
            $q .= " and status_restitusi = '$restitusi' ";
        }

        $q .= "order by date_created DESC";

        $result = $this->db->query($q);
        return $result->result_array();
    }

    function count_filtered()
    {
        $this->dokumen();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {

        $dok = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
            where KodeUser = '".$this->session->userdata('KodeUser')."' ")->result_array();

        if (!empty($dok)) {
            $dokumen = $dok[0]['NamaUser'];
        } else{
            $dokumen = '';
        }

        // var_dump($dokumen);
        // die();

        $q = "select * from PJM_SAUDARA.dbo.DataPenutupan a 
            join PJM_SAUDARA.dbo.Cabang b on a.cab = b.id_cabang
            where 
                a.status = 1
                and cab != ''
                and pk != ''
                and norek != ''
                and nama != ''
                and lahir != ''
                and buka != ''
                and tempo != ''
                and plankredit != ''
                and id != ''
                and ktp != ''
                and sex != ''
                and npwp != '' ";

        if (strpos($dokumen, 'Cabang') !== false){
                $q .= " and b.id_induk = '".$dok[0]['id_cabang']."' ";
        } elseif (strpos($dokumen, 'Capem') !== false) {
            $q .= " and b.id_cabang = '".$dok[0]['id_cabang']."' ";
        } elseif (strpos($dokumen, 'Kedai') !== false) {
            $q .= " and b.id_cabang = '".$dok[0]['id_cabang']."' ";
        }

        $q .= "order by date_created DESC";

        $result = $this->db->query($q);
        return $result->count_all_results($q);
    }
}
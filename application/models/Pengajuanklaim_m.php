<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuanklaim_m extends CI_Model
{

	public function dokumen()
	{
		$query = $this->db->query("select * from PJM_SAUDARA.dbo.Klaim");
        return $query;
	}

    public function getDataPagination($limit, $offset)
    {
         $query = $this->db->query("select * from PJM_SAUDARA.dbo.Klaim order by kodeklaim OFFSET $limit ROWS FETCH NEXT $offset ROWS ONLY");
        return $query->result_array();
    }

    function get_jenisklaim($id)
    {
        $query = $this->db->query("select * from PJM_SAUDARA.dbo.JenisKlaim
		where kode_asuransi = '$id'
		order by kode_asuransi ASC")->result();
        return $query;
    }

    function getstatus($noklaim)
    {
        $query = $this->db->query("select status_step from PJM_SAUDARA.dbo.Klaim
        where kodeklaim = '$noklaim'")->result_array();
        return $query;
    }

    function get_capem($id)
    {
        $query = $this->db->query("select * from PJM_SAUDARA.dbo.Cabang
		where id_induk = '$id'
		order by id_induk ASC")->result();
        return $query;
    }

    public function search()
    {
    	extract($_POST);

    	$periode = $periodetahun.$periodebulan;
        $q = "select * from PJM_SAUDARA.dbo.Klaim where cif != '' ";

        if (!empty($capem)){
            $q .= " and kodecabang='$capem' ";
        }

        if (!empty($periodebulan) and !empty($periodetahun)){
            $q .= " and SUBSTRING(tglsubmit, 3, 6) = '9/2021' ";
        }

        if (!empty($periodebulan) && empty($periodetahun)){
            $q .= " and SUBSTRING(tglsubmit,5,2) = '$periodebulan' ";
        }

        if (!empty($periodetahun) && empty($periodebulan)){
            $q .= " and LEFT(tglsubmit, 4) = '$periodetahun' ";
        }

        if (!empty($statusklaims)){
            $q .= " and statusklaim = '$statusklaims' ";
        }

        $q .= "order by kodecabang ASC";

        $result = $this->db->query($q);
        return $result->result_array();
    }
}
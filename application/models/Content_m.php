<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content_m extends CI_Model
{

     public function dokumen()
    {

        $dok = $this->db->query("select 
        SUM(CAST(REPLACE(outstandingtotalklaim, ',', '') AS float)) as total_outstandingklaim, 
        COUNT(nama) as totalberkas,
        asuransi
        FROM PJM_SAUDARA.dbo.Klaim GROUP BY asuransi ORDER BY asuransi ASC");

        return $dok->result_array();

    }

    public function getdataprosescabang($asuransi, $statusklaim){
    	$dok = $this->db->query("select
        SUM(CAST(REPLACE(outstandingtotalklaim, ',', '') AS float)) as total_outstandingklaim, 
        COUNT(nama) as totalberkas,
        asuransi
        FROM PJM_SAUDARA.dbo.Klaim where asuransi = '$asuransi' and statusklaim = '$statusklaim' GROUP BY asuransi ORDER BY asuransi ASC");

        return $dok->row();
    }

}
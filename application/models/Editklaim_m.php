<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editklaim_m extends CI_Model
{

    public function dokumen($id_kla)
    {

        $query = $this->db->query("select * from PJM_SAUDARA.dbo.Klaim
            where 
                id_kla = '$id_kla'");
            return $query->result_array();
    }

    function get_jenisklaim($id)
    {
        $query = $this->db->query("select * from PJM_SAUDARA.dbo.JenisKlaim
		where kode_asuransi = '$id'
		order by kode_asuransi ASC")->result();
        return $query;
    }

    function get_capem($id)
    {
        $query = $this->db->query("select * from PJM_SAUDARA.dbo.Cabang
		where id_induk = '$id'
		order by id_induk ASC")->result();
        return $query;
    }

    public function search()
    {
    	extract($_POST);

    	$periode = $periodetahun.$periodebulan;
        $q = "select * from PJM_SAUDARA.dbo.Klaim where cif != '' ";

        if (!empty($capem)){
            $q .= " and kodecabang='$capem' ";
        }

        if (!empty($periodebulan) and !empty($periodetahun)){
            $q .= " and SUBSTRING(tglsubmit, 3, 6) = '9/2021' ";
        }

        if (!empty($periodebulan) && empty($periodetahun)){
            $q .= " and SUBSTRING(tglsubmit,5,2) = '$periodebulan' ";
        }

        if (!empty($periodetahun) && empty($periodebulan)){
            $q .= " and LEFT(tglsubmit, 4) = '$periodetahun' ";
        }

        if (!empty($statusklaims)){
            $q .= " and statusklaim = '$statusklaims' ";
        }

        $q .= "order by kodecabang ASC";

        $result = $this->db->query($q);
        return $result->result_array();
    }
}
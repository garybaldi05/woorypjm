<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekappenutupanasuransi_m extends CI_Model
{

    public function dokumen()
    {

        $dok = $this->db->query("select * from PJM_SAUDARA.dbo.DaftarUser
            where KodeUser = '".$this->session->userdata('KodeUser')."' ")->result_array();

        if (!empty($dok)) {
            $dokumen = $dok[0]['NamaUser'];
        } else{
            $dokumen = '';
        }

        // var_dump($dokumen);
        // die();

        $q = "select * from PJM_SAUDARA.dbo.DataPenutupan a 
            join PJM_SAUDARA.dbo.Cabang b on a.cab = b.id_cabang
             ";

        if (strpos($dokumen, 'Cabang') !== false){
                $q .= " where b.id_induk = '".$dok[0]['id_cabang']."' ";
        } elseif (strpos($dokumen, 'Capem') !== false) {
            $q .= " where b.id_cabang = '".$dok[0]['id_cabang']."' ";
        } elseif (strpos($dokumen, 'Kedai') !== false) {
            $q .= " where b.id_cabang = '".$dok[0]['id_cabang']."' ";
        }

        $q .= "order by date_created DESC";

        $result = $this->db->query($q);
        return $result->result_array();
    }

    function get_capem($id)
    {
        $query = $this->db->query("select * from PJM_SAUDARA.dbo.Cabang
		where id_induk = '$id'
		order by id_induk ASC")->result();
        return $query;
    }

    public function search()
    {
    	extract($_POST);
        // var_dump($_POST);
        // die();
        $q = "select * from PJM_SAUDARA.dbo.DataPenutupan where norek != '' ";

        if (!empty($capem)){
            $q .= " and cab='$capem' ";
        } else{
        	$q .= " and cab != '' ";
        }

        if (!empty($periodebulan) and !empty($periodetahun)){
            $periode = $periodetahun.$periodebulan;
            $q .= " and LEFT(buka, 6) = '$periode' ";
        } else{
        	$q .= " and buka != '' ";
        }

        if (!empty($periodebulan)){
            $q .= " and SUBSTRING(buka,5,2) = '$periodebulan' ";
        } else{
        	$q .= " and buka != '' ";
        }

        if (!empty($periodetahun)){
            $q .= " and LEFT(buka, 4) = '$periodetahun' ";
        } else{
        	$q .= " and buka != '' ";
        }

        $q .= "order by cab ASC";

        $result = $this->db->query($q);
        return $result->result_array();
    }
}